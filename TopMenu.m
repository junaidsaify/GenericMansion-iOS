//
//  TopMenu.m
//  GenericMansion
//
//  Created by Junaid Saify on 08/11/15.
//
//

#import "TopMenu.h"

@implementation TopMenu

@synthesize topMenuName,topMenuImagePath,topMenuSequence,topMenuOnClick,topMenuLink,topMenuBrowser,topMenuExtra;

-(instancetype)init{
    if(!self){
        self = [super init];
    }
    
    return  self;
}


-(void)dealloc{

    topMenuName=nil;
    topMenuImagePath=nil;
    topMenuSequence=nil;
    topMenuOnClick=nil;
    topMenuLink=nil;
    topMenuBrowser=nil;
    topMenuExtra=nil;
}



@end
