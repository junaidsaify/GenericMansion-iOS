//
//  SubStopsViewController.h
//  Mansion
//
//  Created by Falguni Shah on 11/28/14.
//
//

#import <UIKit/UIKit.h>
#import "SubStopsTableViewCell.h"
#import "AudioPlayer.h"
#import <Social/Social.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "IDZAQAudioPlayer.h"
#import "GAIDictionaryBuilder.h"
#import "MaximizedViewController.h"
#import "DBManager.h"
#import "CustomerDetails.h"

#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
@class SubStopsViewController;
@protocol SubStopProtocol <NSObject>

-(void)didClickOnRemoveSelf:(SubStopsViewController*)viewController;

@end


@interface SubStopsViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UIScrollViewDelegate,IDZAudioPlayerDelegate>
{
    AudioPlayer *player;
    UIButton *currentPlayingButton;
    UISlider *currentPlayingSlider;
    IBOutlet UIButton *headerButton;
    NSTimer *audioTimer;
    SubStopsTableViewCell *currentCell;
    MFMailComposeViewController* controller;
    NSInteger currentIndex;
    UIImage *imageToBeMaximized;
    MaximizedViewController *maxVC;
}

@property(nonatomic,assign)id<SubStopProtocol>delegate;

@property(nonatomic,retain)DBManager *dbManager;
@property(nonatomic,retain)CustomerDetails *customerDetails;
@property(nonatomic,retain)NSArray *subStopsArray,*subStopsNamesArray,*subStopsImagePathArray;
@property(nonatomic,retain)NSString *directoryPath;
@property(nonatomic,retain)IBOutlet UITableView *tableView;
@property(nonatomic,retain)IBOutlet UIView *transcriptView,*socialNetworkView,*transcriptInnerView,*maximizedView,*maximizedBaseView;
@property(nonatomic,retain)IBOutlet UIImageView* maximizedImageView,*semiCircleImageView;
@property(nonatomic,retain)IBOutlet UIScrollView *maximizedImageScrollView;
@property(nonatomic,retain)IBOutlet UIButton *closeTranscriptButton,*maximizedCloseButton,*maximizedDoneButton;
@property(nonatomic,retain)IBOutlet UITextView *transcriptTextView;
@property(nonatomic,retain)NSString *floorName,*presentRoom,*mansionName;
-(IBAction)removeView:(id)sender;
-(IBAction)playAudio:(UIButton*)sender;
-(IBAction)removeTranscriptView:(UIButton*)sender;

// Maximize Image
-(IBAction)maximizeTheImage:(UIButton*)sender;
-(void)stopAudioPlayer;
-(void)pauseThePlayer;
@end
