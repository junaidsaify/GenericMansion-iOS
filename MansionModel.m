//
//  MansionModel.m
//  GenericMansion
//
//  Created by Junaid Saify on 14/11/15.
//
//

#import "MansionModel.h"

@implementation MansionModel

@synthesize name,shortName,imagePath,interiorImagePath,exteriorImagePath,tourPath,aboutMansion,address,sequence,extra,fileSizeInMB,latitude,longitude,cultureCode,GUID;






-(void)dealloc
{
    name=nil;
    shortName=nil;
    imagePath=nil;
    interiorImagePath=nil;
    exteriorImagePath=nil;
    tourPath=nil;
    aboutMansion=nil;
    address=nil;
    sequence=nil;
    extra=nil;
    fileSizeInMB=nil;
    cultureCode=nil;
    GUID=nil;
    
    
}

@end
