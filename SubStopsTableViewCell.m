//
//  SubStopsTableViewCell.m
//  Mansion
//
//  Created by Falguni Shah on 11/28/14.
//
//

#import "SubStopsTableViewCell.h"

@implementation SubStopsTableViewCell
@synthesize audioPlayerSlider,moreButton,playButton;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)init{
    
    self=[super init];
    if(self){
        
    }
    
    return self;
}

-(IBAction)playTheAudio:(UIButton*)sender{
    //..if(sender==self.currentPlayingButton){
     //..   [self.delegate didClickOnPlayingButtonForCell:self withPlayButton:sender];
   //.. }
  //..  else{
     //..   [self.currentPlayingButton setSelected:NO];
    //..    self.currentPlayingButton=sender;
    [self.delegate didClickOnPlayButtonForCell:self withPlayButton:sender];
   //.. }
}

- (IBAction)slide {
//    [UIView animateWithDuration:1.0 animations:^{
   //     [self.delegate didDragAudioSlide:self.audioPlayerSlider];
        
        [self.delegate didSlideAudioSlider:self.audioPlayerSlider];
//}];
}

-(IBAction)currentTimeSliderTouchUpInside:(id)sender{
 //   [self.delegate didDragAudioSlide:self.audioPlayerSlider];
    [self.delegate didDragAudioSlide:self.audioPlayerSlider forCell:self];
}

-(IBAction)moreButtonClicked:(UIButton*)sender{
    [self.delegate didClickOnMoreButtonForCell:self withButton:self.moreButton];
}

-(IBAction)maximizedButtonClicked:(UIButton*)sender{
    [self.delegate didClickOnMaximizedButtonForCell:self withButton:sender];
}

@end
