//
//  CustomerDetails.h
//  GenericMansion
//
//  Created by Junaid Saify on 13/12/15.
//
//

#import <Foundation/Foundation.h>

@interface CustomerDetails : NSObject

@property(nonatomic,retain) NSString *customerName;
@property(nonatomic,retain) NSString *copywriteText;
@property(nonatomic,retain) NSString *colorCode;
@property(nonatomic,retain) NSString *secondaryColorCode;
@property(nonatomic,retain) NSString *sideMenuImagePath;
@property(nonatomic,retain) NSString *ipadGridImagePath,*iphoneGridImagePath;
@property(nonatomic,retain) NSString *website;
@property(nonatomic,retain) NSString *tagline;
@property(nonatomic,retain) NSString *sideMenuColor;
@property(nonatomic,retain) NSString *topMenuColor;
@property(nonatomic,retain) NSString *bottomMenuColor;
@property(nonatomic,retain) NSString *tourNextArrowPath;
@property(nonatomic,retain) NSString *tourPreviousArrowPath;
@property(nonatomic,retain) NSString *maximizeImagePath;
@property(nonatomic,retain) NSString *tourPlayButtonPath;
@property(nonatomic,retain) NSString *autoPlayButtonPath;
@property(nonatomic,retain) NSString *isAutoPlay;
@property(nonatomic,retain) NSString *tourPauseButtonPath;
@property(nonatomic,retain) NSString *popoverButtonPath;
@property(nonatomic,retain) NSString *substopText;
@property(nonatomic,retain) NSString *substopShowImagePath;
@property(nonatomic,retain) NSString *substopRemoveImagePath;
@property(nonatomic,retain) NSString *closeButtonImagePath;
@property(nonatomic,retain) NSString *deleteButtonImagePath;
@property(nonatomic,retain) NSString *actionShowImagePath;
@property(nonatomic,retain) NSString *listViewSelectedImagePath;
@property(nonatomic,retain) NSString *listViewDefaultImagePath;
@property(nonatomic,retain) NSString *shareEnabled;
@property(nonatomic,retain) NSString *mansionLeftArrowPath,*mansionRightArrowPath;
@property(nonatomic,retain) NSString *donateLink;
@property(nonatomic,retain) NSString *substopBackgroundColor;
@property(nonatomic,retain) NSString *segmentViewName;
@property(nonatomic,retain) NSString *extra;


@end
