//
//  CustomerDetails.m
//  GenericMansion
//
//  Created by Junaid Saify on 13/12/15.
//
//

#import "CustomerDetails.h"

@implementation CustomerDetails

@synthesize customerName,copywriteText,colorCode,secondaryColorCode,website,sideMenuImagePath,ipadGridImagePath,iphoneGridImagePath,extra,tagline,sideMenuColor,topMenuColor,bottomMenuColor,tourNextArrowPath,tourPreviousArrowPath,maximizeImagePath,tourPlayButtonPath,autoPlayButtonPath,isAutoPlay,tourPauseButtonPath,popoverButtonPath,substopText,substopShowImagePath,substopRemoveImagePath,closeButtonImagePath,deleteButtonImagePath,actionShowImagePath,listViewDefaultImagePath,listViewSelectedImagePath,shareEnabled,mansionLeftArrowPath,mansionRightArrowPath,donateLink,substopBackgroundColor,segmentViewName;

-(void) dealloc{
    
    self.customerName=nil;
    self.copywriteText=nil;
    self.colorCode=nil;
    self.website=nil;
    self.secondaryColorCode=nil;
    self.sideMenuImagePath=nil;
    self.ipadGridImagePath=nil;
      self.iphoneGridImagePath=nil;
    self.extra=nil;
    self.tagline=nil;
    
    self.sideMenuColor=nil;
    self.topMenuColor=nil;
    self.bottomMenuColor=nil;
    self.tourNextArrowPath=nil;
    self.tourPreviousArrowPath=nil;
    self.maximizeImagePath=nil;
    self.tourPlayButtonPath=nil;
    self.autoPlayButtonPath=nil;
    self.isAutoPlay=nil;
    
     self.tourPauseButtonPath=nil;
     self.popoverButtonPath=nil;
     self.substopText=nil;
     self.substopShowImagePath=nil;
     self.substopRemoveImagePath=nil;
     self.closeButtonImagePath=nil;
     self.deleteButtonImagePath=nil;
    self.actionShowImagePath=nil;
    self.listViewDefaultImagePath=nil;
    self.listViewSelectedImagePath=nil;
    self.shareEnabled=nil;
    self.mansionLeftArrowPath=nil;
    self.mansionRightArrowPath=nil;
    self.donateLink=nil;
    self.substopBackgroundColor=nil;
    self.segmentViewName=nil;
}

@end
