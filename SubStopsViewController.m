//
//  SubStopsViewController.m
//  Mansion
//
//  Created by Falguni Shah on 11/28/14.
//
//

#import "SubStopsViewController.h"
#import "UIActionSheet+Blocks.h"

#import "IDZTrace.h"
#import "IDZOggVorbisFileDecoder.h"
@interface SubStopsViewController ()
@property (nonatomic, strong) id<IDZAudioPlayer> customPlayer;
@end

@implementation SubStopsViewController
@synthesize subStopsArray,subStopsNamesArray,subStopsImagePathArray;
@synthesize directoryPath;
@synthesize floorName,presentRoom,mansionName;
@synthesize dbManager,customerDetails;

-(void)sendDataForTrackingForAction:(NSString*)action label:(NSString*)lable{
    NSMutableDictionary *event =   [[GAIDictionaryBuilder createEventWithCategory:@"UI"
                                                                           action:action
                                                                            label:lable
                                                                            value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    [[GAI sharedInstance] dispatch];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //UINib *nib = [UINib nibWithNibName:@"CCell" bundle:nil];
    //[[self tableView] registerNib:nib forCellReuseIdentifier:@"CCell"];
    self.screenName=@"SubstopView";
    CGRect frame=self.view.frame;
    CGRect screenFrame= [[UIScreen mainScreen] bounds];
    frame.origin=screenFrame.origin;
    frame.size=screenFrame.size;
    self.view.frame=frame;
    
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [headerButton setTitle:self.presentRoom forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(audioPlayerFinishedPlaying:) name:@"AudioPlayerFinishedPlaying" object:nil];
    currentIndex=-1;
   //.. self.tableView.separatorColor=[UIColor colorWithString:@"#0F4D3A"];
    
    [self.tableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    
    [self.semiCircleImageView sd_setImageWithURL:[NSURL URLWithString:customerDetails.substopRemoveImagePath] placeholderImage:nil];
     [self.closeTranscriptButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.closeButtonImagePath] forState:UIControlStateNormal placeholderImage:nil];
    
    [self.view setBackgroundColor:[UIColor colorWithString:self.customerDetails.substopBackgroundColor]];
    [self.tableView setBackgroundColor:[UIColor colorWithString:self.customerDetails.substopBackgroundColor]];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)removeView:(id)sender{
    
  //..  [self stopAudioPlayer];
  //..  [self pauseThePlayer];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SubstopsRemoved" object:nil];
    
}




#pragma tableview datasource and delegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
  /*  SubStopsTableViewCell *customCell = (SubStopsTableViewCell*)cell;
    if(indexPath.row == currentIndex){
        NSLog(@" current index is %d",currentIndex);
        if(self.customPlayer.isPlaying){
        [customCell.audioPlayerSlider setValue:self.customPlayer.currentTime animated:YES];
            [customCell.playButton setSelected:YES];
        }
    }
    
    else{
        [customCell.audioPlayerSlider setValue:0.0 animated:YES];
        [customCell.playButton setSelected:NO];
    } */
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if(isIphone){
        return   160;
    }
        return 218;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.subStopsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CCell";
    
    SubStopsTableViewCell *cell = (SubStopsTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        //cell = [[SubStopsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSArray *nib ;
        
        if(isIphone){
            nib = [[NSBundle mainBundle] loadNibNamed:@"SubStopsTableViewCell-iPhone" owner:self options:nil];
             cell = (SubStopsTableViewCell*) [nib objectAtIndex:0];
            cell.stopImageView.frame=CGRectMake(10, 27, 128, 100);
            
            cell.playButton.center=CGPointMake(CGRectGetMaxX(cell.stopImageView.frame), CGRectGetMidY(cell.stopImageView.frame));
            cell.expandButton.frame=CGRectMake(CGRectGetMinX(cell.stopImageView.frame)+1, CGRectGetMaxY(cell.stopImageView.frame)-38, 40, 40);
        }
        else{
            nib = [[NSBundle mainBundle] loadNibNamed:@"SubStopsTableViewCell" owner:self options:nil];
             cell = (SubStopsTableViewCell*) [nib objectAtIndex:0];
            cell.stopImageView.frame=CGRectMake(25, 10, 281, 201);
            cell.playButton.center=CGPointMake(CGRectGetMaxX(cell.stopImageView.frame), CGRectGetMidY(cell.stopImageView.frame));
            cell.expandButton.frame=CGRectMake(30, CGRectGetMaxY(cell.stopImageView.frame)-40, 50 , 50);
        }
       cell.fakeButton.frame=cell.stopImageView.frame;
        [cell.contentView setBackgroundColor:[UIColor colorWithString:self.customerDetails.substopBackgroundColor]];
        
       
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        cell.stopImageView.layer.borderColor=[UIColor whiteColor].CGColor;
        cell.stopImageView.layer.borderWidth=1.0f;
      
        UIImage *minImage=[[UIImage imageNamed:@"GreenSlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        UIImage *maxImage=[[UIImage imageNamed:@"GreySlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        
        [cell.audioPlayerSlider setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [cell.audioPlayerSlider setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [cell.audioPlayerSlider setThumbImage:[UIImage imageNamed:@"TrackBall"] forState:UIControlStateNormal];
       
        [cell.playButton sd_setBackgroundImageWithURL:[NSURL URLWithString:self.customerDetails.tourPlayButtonPath] forState:UIControlStateNormal placeholderImage:nil];
        [cell.playButton sd_setBackgroundImageWithURL:[NSURL URLWithString:self.customerDetails.tourPauseButtonPath] forState:UIControlStateSelected placeholderImage:nil];
        [cell.expandButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.maximizeImagePath] forState:UIControlStateNormal placeholderImage:nil];
        [cell.expandButton setContentMode:UIViewContentModeScaleAspectFit];
        [cell.moreButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.popoverButtonPath] forState:UIControlStateNormal placeholderImage:nil];
    }
    
    
    
    cell.audioPlayerSlider.tag=indexPath.row;
    cell.playButton.tag=indexPath.row;
    cell.tag=indexPath.row;//..[[self.subStopsArray objectAtIndex:indexPath.row] integerValue];
   // cell.playButton.tag=indexPath.row;//..[[self.subStopsArray objectAtIndex:indexPath.row] integerValue];
   // cell.audioPlayerSlider.tag=indexPath.row;//[[self.subStopsArray objectAtIndex:indexPath.row] integerValue];
    
    
    
    cell.moreButton.tag=[[self.subStopsArray objectAtIndex:indexPath.row] integerValue];
    cell.stopName.text=nil;
    cell.stopNumber.text= [self.subStopsNamesArray objectAtIndex:indexPath.row];
    cell.stopImageView.image=[UIImage imageWithContentsOfFile:[self.subStopsImagePathArray objectAtIndex:indexPath.row]];
    [cell.playButton setSelected:NO];
    [cell.audioPlayerSlider setValue:0.0 animated:NO];
     NSLog(@"CPB from cell Tag %d",currentIndex);
    
    if(indexPath.row == currentIndex){
        NSLog(@"The current cell tag %d", currentIndex);
        if(self.customPlayer.isPlaying){
          cell.playButton.selected=YES;
        }
        
    [cell.audioPlayerSlider setValue:self.customPlayer.currentTime animated:NO];//..currentPlayingSlider.value;
     //   [currentPlayingSlider setValue:player.player.currentTime animated:NO];
    }
    
    else{
        cell.playButton.selected=NO;
        [cell.audioPlayerSlider setValue:0.0 animated:NO];
    }
    

  return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        
}

-(void)didClickOnPlayButtonForCell:(SubStopsTableViewCell*)cell withPlayButton:(UIButton*)button{
   //.. currentPlayingButton.tag=cell.tag;
    //cell.tag;
     NSString *room= [self.subStopsArray objectAtIndex:cell.tag];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"SubstopPlay"
     object:self];
    NSLog(@"CPB Tag %d",button.tag);
  //  if(button==currentPlayingButton)
  if(button.tag == currentIndex) // done after ogg
    {
        [self didClickOnPlayingButtonForCell:cell withPlayButton:button];
    }
    
    else{
        [UIView animateWithDuration:1.0 animations:^{
            [currentPlayingSlider setValue:0.0 animated:YES];
        }];
        [self.customPlayer stop]; // added later after custom player
    self.customPlayer=nil;
    
    if(!self.customPlayer){
        
   //.. NSString *room= [self.subStopsArray objectAtIndex:cell.tag];
        [currentPlayingButton setSelected:NO];
  //..      NSString *audioPath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@/SubStops/%d/Audio/eng_%d.mp3",self.mansionName,self.floorName,self.presentRoom,room,room]];
        
   /*     player=[[AudioPlayer alloc]init];
        [player createAudioPlayerWithPath:audioPath];
        [button setSelected:YES];
        currentPlayingButton=button;
        currentPlayingSlider=cell.audioPlayerSlider;
        currentPlayingSlider.maximumValue = [player.player duration];
        currentPlayingSlider.value = 0.0;
        audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES]; */
        
        NSError *error;
        NSString *audioPath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@/SubStops/%@/Audio/eng_%@.ogg",self.mansionName,self.floorName,self.presentRoom,room,room]];
        NSURL* oggUrl = [NSURL fileURLWithPath:audioPath]; //..[[NSBundle mainBundle] URLForResource:@"test" withExtension:@"ogg"];
        
        IDZOggVorbisFileDecoder* decoder = [[IDZOggVorbisFileDecoder alloc] initWithContentsOfURL:oggUrl error:&error];
        NSLog(@"Ogg Vorbis file duration is %g", decoder.duration);
        self.customPlayer = [[IDZAQAudioPlayer alloc] initWithDecoder:decoder error:nil];
        self.customPlayer.delegate=self;
        [self.customPlayer prepareToPlay];
        [self.customPlayer play];
        [button setSelected:YES];
        currentPlayingButton=button;
        currentPlayingSlider=cell.audioPlayerSlider;
        currentPlayingSlider.tag=button.tag;
        currentPlayingSlider.maximumValue=decoder.duration;
        currentPlayingSlider.value=0.0;
        audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:cell.audioPlayerSlider repeats:YES];
    }
    
    }
 currentIndex=button.tag;

}

-(void)didClickOnPlayingButtonForCell:(SubStopsTableViewCell*)cell withPlayButton:(UIButton*)button{
    currentPlayingButton.tag=cell.tag;
    currentPlayingButton=button;
     if(self.customPlayer.isPlaying){
        
        [button setSelected:NO];
      //  [player pauseAudioPlayer];
         [self.customPlayer pause];
    }
    else{
        [button setSelected:YES];
      //  [player.player play];
        [self.customPlayer play];
    }
    

}

-(void)pauseThePlayer{
    [currentPlayingButton setSelected:NO];
  //  [player.player pause];
    [self.customPlayer pause];
}


-(void)stopAudioPlayer{
    [currentPlayingButton setSelected:NO];
    [UIView animateWithDuration:1.0 animations:^{
        [currentPlayingSlider setValue:0 animated:YES];
    }];
   // [player stopAndDistroyPlayer];
  //  player=nil;
    [self.customPlayer stop];
    self.customPlayer=nil;
    currentPlayingButton=nil;
    currentIndex=-1;
    [audioTimer invalidate];
}


// Notification of Audio Player
-(void)audioPlayerFinishedPlaying:(NSNotification*)noti{
    [self stopAudioPlayer];
    
}
// Audio Slider update

-(void)didSlideAudioSlider:(UISlider*)audioSlider{
    
    [audioTimer invalidate];
    audioTimer=nil;
}

-(void)didDragAudioSlide:(UISlider*)audioSlider forCell:(SubStopsTableViewCell*)cell{

//-(void)didDragAudioSlide:(UISlider*)audioSlider{
  //..  if(audioSlider!=currentPlayingSlider)
    audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:cell.audioPlayerSlider repeats:YES];

    if(audioSlider.tag != currentIndex)
    {
        return;
    }
   self.customPlayer.currentTime = currentPlayingSlider.value;
    
    
}

- (void)updateTime:(NSTimer *)timer {
    //..  [audioProgressSlider setValue: _player.player.currentTime animated:YES];
   // [UIView animateWithDuration:1.0 animations:^{
    UISlider *slider =(UISlider*) timer.userInfo;
    if(slider.tag == currentIndex){
        [slider setValue:self.customPlayer.currentTime animated:YES];
    }
    else{
        [slider setValue:0.0 animated:NO];
    }
   // }];
   
}


#pragma mark - IDZAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(id<IDZAudioPlayer>)player successfully:(BOOL)flag
{
    NSLog(@"%s successfully=%@", __PRETTY_FUNCTION__, flag ? @"YES"  : @"NO");
    
//..    [self stopAudioPlayer];
    
    [currentPlayingButton setSelected:NO];
    [UIView animateWithDuration:1.0 animations:^{
        [currentPlayingSlider setValue:0 animated:YES];
    }];
    [self.customPlayer stop];
    currentPlayingButton=nil;
    currentIndex=-1;
    [audioTimer invalidate];
    
    
    //    audioProgressSlider.value=0.0;
    //[audioTimer invalidate];
   
     }

- (void)audioPlayerDecodeErrorDidOccur:(id<IDZAudioPlayer>)player error:(NSError *)error
{
    NSLog(@"%s error=%@", __PRETTY_FUNCTION__, error);
    
}






// More Button Methods
-(void)didClickOnMoreButtonForCell:(SubStopsTableViewCell*)cell withButton:(UIButton*)moreButton{
   //.. currentIndex=cell.tag;
    currentCell=cell;
    NSLog(@"Share Cell Tag %d", cell.tag);
    [UIActionSheet showFromRect:moreButton.frame inView:cell.contentView animated:YES withTitle:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@[@"Transcript",@"Share"]  tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 0:
                [self showTranscriptWithTag:(NSInteger)cell.tag];
                break;
            case 1:{
             //..   [self showShareScreenWithTag:(NSInteger)moreButton.tag];
          /*   UIImage *imageToShare = [self setShareImageForTour];
                [self shareText:Subject andImage:imageToShare andUrl:[NSURL URLWithString:NewportURL]]; */
                [self performSelector:@selector(shareWithDelay) withObject:nil afterDelay:0.5];
                
                break;
            }
            case 2:
               //.. [self markAsFavourite];
                break;
            default:
                break;
        }
        
    }];

}

-(void)didClickOnMaximizedButtonForCell:(SubStopsTableViewCell*)cell withButton:(UIButton*)maximizedButton{
    imageToBeMaximized= cell.stopImageView.image;
    [self maximizeTheImage:maximizedButton];
    
  /*  if(isIphone){
        maxVC=[[MaximizedViewController alloc]initWithNibName:@"MaximizedViewController" bundle:nil];
    }
    
    else{
        maxVC=[[MaximizedViewController alloc]initWithNibName:@"MaximizedViewController-iPad" bundle:nil];
    }
    
    maxVC.imageToMaximize = imageToBeMaximized;
    
    [self presentViewController:maxVC animated:YES completion:nil]; */
    
}



-(void)showTranscriptWithTag:(NSInteger)tag{
    NSString *room= [self.subStopsArray objectAtIndex:tag] ;
 //  NSMutableString *str=[NSMutableString stringWithString:[dbManager getTranscriptForRoom:room andTable:@"Substops"]];
 //  [str appendString:[NSString stringWithFormat:@"\n \nCopyright \u00A9 %@",customerDetails.copywriteText]];
 
    NSMutableString *str = [dbManager getTranscriptForRoom:room andTable:@"Substops"];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.transcriptTextView.attributedText = attributedString;
    
    
    NSArray *subViews=[self.transcriptView subviews];
    for(UIView *view in subViews){
        if(view.tag != 19){
            [view removeFromSuperview];
        }
    }
    
    
    self.transcriptView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    self.transcriptInnerView.layer.cornerRadius=10.0;
    self.transcriptInnerView.layer.borderWidth=5.0;
    self.transcriptInnerView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    //..[UIColor colorWithRed:120.0 green:181.0 blue:226.0 alpha:1].CGColor;
  //.. self.transcriptTextView.text=str;
    [self.transcriptView addSubview:self.transcriptInnerView];
   
    [self.transcriptView bringSubviewToFront:self.closeTranscriptButton];
     self.transcriptView.frame=self.view.frame;
     self.transcriptInnerView.center=self.view.center;
     self.closeTranscriptButton.center=CGPointMake(CGRectGetMaxX(self.transcriptInnerView.frame), CGRectGetMinY(self.transcriptInnerView.frame));
    [self.view addSubview:self.transcriptView];
    
    
   
   
}


-(void)showShareScreenWithTag:(NSInteger)tag{
    // UIView *nowShowingView=[self.transcriptView viewWithTag:<#(NSInteger)#>]
    NSArray *subViews=[self.transcriptView subviews];
    for(UIView *view in subViews){
        if(view.tag != 19){
            [view removeFromSuperview];
        }
    }
    
    
    self.transcriptView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    self.socialNetworkView.layer.cornerRadius=10.0;
    self.socialNetworkView.layer.borderWidth=5.0;
    self.socialNetworkView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    //..[UIColor colorWithRed:120.0 green:181.0 blue:226.0 alpha:1].CGColor;
    self.socialNetworkView.center=self.transcriptView.center;
    [self.transcriptView addSubview:self.socialNetworkView];
    
    self.transcriptView.frame=self.view.frame;
    self.socialNetworkView.center = self.transcriptView.center;
    
    self.closeTranscriptButton.center=CGPointMake(CGRectGetMaxX(self.socialNetworkView.frame), CGRectGetMinY(self.socialNetworkView.frame));
    [self.transcriptView bringSubviewToFront:self.closeTranscriptButton];
    [self.view addSubview:self.transcriptView];
    
}

-(UIImage*)setShareImageForTour {
    
    NSString *room= [self.subStopsArray objectAtIndex:currentCell.tag];
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:self.mansionName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorName];
    NSString *imagePath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@/SubStops/%@/Images",self.mansionName,self.floorName,self.presentRoom,room]];
    
    UIImage *imageToShare=[self returnImageForPath:imagePath];
    return imageToShare;
}


-(IBAction)shareLink:(UIButton*)sender{
    
    NSString *room= [self.subStopsArray objectAtIndex:currentCell.tag];
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:self.mansionName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorName];
    NSString *imagePath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@/SubStops/%@/Images",self.mansionName,self.floorName,self.presentRoom,room]];
    
      UIImage *imageToShare=[self returnImageForPath:imagePath];
    SLComposeViewController *socialPost;
    //.. if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
    
    if(sender.tag==3){
        /*    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
         controller.mailComposeDelegate = self;
         [controller setSubject:@"My Subject"];
         [controller setMessageBody:@"Hello there." isHTML:NO];
         
         if ([MFMailComposeViewController canSendMail]) {
         if (controller) [self presentViewController:controller animated:YES completion:nil];
         } else {
         
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport" message:@"Configure Your Mail" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
         [alert show];
         }
         */
        [self sendEmailWIthAttachments:imageToShare];
        
        
        return;
    }
    
    if(sender.tag==1){
        socialPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    }
    
    else if(sender.tag==2){
        socialPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    }

    [socialPost setInitialText:[NSString stringWithFormat:@"%@Newport Mansions",GreatTimeMessage]];
    [socialPost addURL:[NSURL URLWithString:@"www.NewportMansions.org"]];
    [socialPost addImage:imageToShare];
  
  //..  [socialPost setInitialText:Subject];
  //..  [socialPost addImage:imageToShare];
    
    [self presentViewController:socialPost animated:YES completion:nil];
    
    // set up a completion handler
    [socialPost setCompletionHandler:^(SLComposeViewControllerResult result) {
        switch (result) {
            case SLComposeViewControllerResultDone:
                // post was completed
                NSLog(@"Completed");
                break;
            case SLComposeViewControllerResultCancelled:
                // post was cancelled
                NSLog(@"Not Completed");
                break;
            default:
                break;
        }
    }];
    
    
    
    
}

-(UIImage*)returnImageForPath:(NSString*)path{
    NSFileManager *fileMngr=[NSFileManager defaultManager];
    UIImage *newImage;
    //..NSString *imagePath=[NSString stringWithFormat:@"%@/%@/Images",path,presentRoom];
    NSArray *imageArray=[fileMngr contentsOfDirectoryAtPath:path error:nil];
    if(imageArray>0){
        newImage= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",path,[imageArray objectAtIndex:0]]];
        if(newImage==nil){
            newImage=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",path,[imageArray objectAtIndex:1]]];
        }
        
    }
    else{
        newImage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"SubStops" ofType:@"png"]];
    }
    return newImage;
}


-(void)sendEmailWIthAttachments:(id)attachment{
    UIImage *imageToSend=(UIImage*)attachment;
    NSData *imageData=UIImagePNGRepresentation(imageToSend);
    controller = [[MFMailComposeViewController alloc] init];
    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"mansion.png"];
    controller.mailComposeDelegate = self;
    [controller setSubject:Subject];
    [controller setMessageBody:Subject isHTML:NO];
    
    if ([MFMailComposeViewController canSendMail]) {
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\nPlease Configure Your Mail" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)maximizeTheImage:(UIButton*)sender{
    self.maximizedView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
 //   self.maximizedBaseView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
   // self.maximizedBaseView.layer.borderWidth=5.0f;
    self.maximizedImageView.image=imageToBeMaximized;
   
    
    self.maximizedView.frame=self.view.frame;
    self.maximizedBaseView.center = self.maximizedView.center;
     self.maximizedCloseButton.center=CGPointMake(CGRectGetMaxX(self.maximizedBaseView.frame), CGRectGetMinY(self.maximizedBaseView.frame));
    // start of new fb approach
    
    self.maximizedCloseButton.hidden=YES; // added for done
    self.maximizedDoneButton.layer.borderWidth=2.0f;
    self.maximizedDoneButton.layer.cornerRadius=5.0;
    self.maximizedDoneButton.layer.borderColor=[UIColor whiteColor].CGColor;
    
    self.maximizedImageView.contentMode =UIViewContentModeScaleAspectFit;
    
  // end of new fb approach
   UITapGestureRecognizer *taptoRemove=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resetZoomScale:)];
    taptoRemove.numberOfTapsRequired=2;
    [self.maximizedView addGestureRecognizer:taptoRemove];

    [self.view addSubview:self.maximizedView];
    [self.view bringSubviewToFront:self.maximizedView];
    
/*
    [UIView animateWithDuration:0.5/1.5 animations:^{
        self.maximizedBaseView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5/2 animations:^{
            self.maximizedBaseView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5/2 animations:^{
                self.maximizedBaseView.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
    
    */

    
    
    
}

-(void)resetZoomScale:(UIGestureRecognizer*)gesture{
    if(self.maximizedImageScrollView.zoomScale>1 ){
        [self.maximizedImageScrollView setZoomScale:1.0];
    }
    else{
        [self.maximizedImageScrollView setZoomScale:4.0];
    }
}

-(void)shareWithDelay{
    
     UIImage *imageToShare = [self setShareImageForTour];
    [self shareText:customerDetails.tagline andImage:imageToShare andUrl:[NSURL URLWithString:customerDetails.website]];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    
    //...[self sendDataForTrackingForAction:@"SubstopShare" label:[self.subStopsArray objectAtIndex:currentCell.tag]];
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [activityController setValue:customerDetails.tagline forKey:@"subject"];
    if(!isIphone) {
        //    activityController.popoverPresentationController.sourceRect = self.exteriorImage.frame;
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityController];
    
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }
    else{
        [self presentViewController:activityController animated:YES completion:nil];
    }

}



-(IBAction)removeMaximizedView:(id)sender{
    [self.maximizedImageScrollView setZoomScale:1];
    [self.maximizedView removeFromSuperview];
}



-(IBAction)removeTranscriptView:(UIButton*)sender{
    [self.transcriptView removeFromSuperview];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.maximizedImageView;
}



- (void)centerScrollViewContents {
    CGSize boundsSize = self.maximizedImageScrollView.bounds.size;
     CGRect contentsFrame = self.maximizedImageView.frame;
             if (contentsFrame.size.width < boundsSize.width) {
                contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
            } else {
                    contentsFrame.origin.x = 0.0f;
                }
    
        
        if (contentsFrame.size.height < boundsSize.height) {
                contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
            } else {
                    contentsFrame.origin.y = 0.0f;
                }
        
        self.maximizedImageView.frame = contentsFrame;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    if(scrollView.tag==50){
        if(scrollView.zoomScale==1){
            
            self.maximizedDoneButton.hidden=NO;
        }
        else{
            self.maximizedDoneButton.hidden=YES;
            
        }
    }
    [self centerScrollViewContents];
    
}



@end
