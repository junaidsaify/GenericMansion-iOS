//
//  SegmentViewDetails.m
//  GenericMansion
//
//  Created by Junaid Saify on 24/01/16.
//
//

#import "SegmentViewDetails.h"

@implementation SegmentViewDetails
@synthesize name,onClick,colorCode,browser,link;


-(void)dealloc {
    self.name=nil;
    self.onClick=nil;
    self.colorCode=nil;
    self.browser=nil;
    self.link=nil;
}


@end
