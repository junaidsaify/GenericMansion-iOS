//
//  SideMenu.h
//  GenericMansion
//
//  Created by Junaid Saify on 07/11/15.
//
//

#import <Foundation/Foundation.h>

@interface SideMenu : NSObject


@property(nonatomic,retain) NSString *menuName;
@property(nonatomic,retain) NSString *menuImagePath;
@property(nonatomic) NSUInteger menuSequence;
@property(nonatomic,retain) NSString *menuOnClick;
@property(nonatomic,retain)NSString *menuLink;
@property(nonatomic,retain)NSString *menuBrowser;
@property(nonatomic,retain)NSString *menuExtra;
@end
