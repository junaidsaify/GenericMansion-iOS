//
//  CustomerManager.h
//  GenericMansion
//
//  Created by Junaid Saify on 13/12/15.
//
//

#import <Foundation/Foundation.h>
#import "FMDBManager.h"
@interface CustomerManager : NSObject
-(NSMutableArray*)fetchCustomerDetails;
@end
