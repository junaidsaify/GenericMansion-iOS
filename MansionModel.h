//
//  MansionModel.h
//  GenericMansion
//
//  Created by Junaid Saify on 14/11/15.
//
//

#import <Foundation/Foundation.h>

@interface MansionModel : NSObject

@property(nonatomic,retain) NSString *name,*shortName;
@property(nonatomic,retain) NSString *imagePath,*interiorImagePath,*exteriorImagePath;
@property(nonatomic,retain) NSString *tourPath;
@property(nonatomic,retain) NSString *aboutMansion;
@property(nonatomic,retain) NSString *address;
@property(nonatomic,retain) NSString *extra;
@property(nonatomic,retain) NSString *fileSizeInMB;
@property(nonatomic,retain) NSString *cultureCode;
@property(nonatomic,retain) NSString *GUID;

@property(nonatomic) NSInteger *sequence;
@property(nonatomic) double latitude;
@property(nonatomic) double longitude;






@end
