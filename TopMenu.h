//
//  TopMenu.h
//  GenericMansion
//
//  Created by Junaid Saify on 08/11/15.
//
//

#import <Foundation/Foundation.h>

@interface TopMenu : NSObject

@property(nonatomic,retain) NSString *topMenuName;
@property(nonatomic,retain) NSString *topMenuImagePath;
@property(nonatomic) NSUInteger topMenuSequence;
@property(nonatomic,retain) NSString *topMenuOnClick;
@property(nonatomic,retain)NSString *topMenuLink;
@property(nonatomic,retain)NSString *topMenuBrowser;
@property(nonatomic,retain)NSString *topMenuExtra;

@end
