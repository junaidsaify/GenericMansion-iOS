//
//  SegmentViewDetails.h
//  GenericMansion
//
//  Created by Junaid Saify on 24/01/16.
//
//

#import <Foundation/Foundation.h>

@interface SegmentViewDetails : NSObject

@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *onClick;
@property(nonatomic,retain)NSString *colorCode;
@property(nonatomic,retain)NSString *browser;
@property(nonatomic,retain)NSString *link;
@end
