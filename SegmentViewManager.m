//
//  SegmentViewManager.m
//  GenericMansion
//
//  Created by Junaid Saify on 24/01/16.
//
//

#import "SegmentViewManager.h"

@implementation SegmentViewManager

-(NSMutableArray*)fetchSegmentViewDetails{
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM SegmentView Order By Sequence ASC";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
    array=   [manager returnValueForQuery:query forTable:@"SegmentView"];
    
    return array;

}
@end
