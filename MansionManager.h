//
//  MansionManager.h
//  GenericMansion
//
//  Created by Junaid Saify on 14/11/15.
//
//

#import <Foundation/Foundation.h>
#import "FMDBManager.h"
@interface MansionManager : NSObject

-(NSMutableArray*)fetchAllMansions;


@end
