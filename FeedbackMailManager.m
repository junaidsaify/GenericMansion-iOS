//
//  FeedbackMailManager.m
//  GenericMansion
//
//  Created by Junaid Saify on 16/12/15.
//
//

#import "FeedbackMailManager.h"

@implementation FeedbackMailManager



-(NSMutableArray*)fetchFeedbackMailDetails{
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM FeedbackDetails";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
    array=   [manager returnValueForQuery:query forTable:@"FeedbackDetails"];
    
    return array;
}
@end
