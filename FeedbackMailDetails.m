//
//  FeedbackMailDetails.m
//  GenericMansion
//
//  Created by Junaid Saify on 16/12/15.
//
//

#import "FeedbackMailDetails.h"

@implementation FeedbackMailDetails
@synthesize recipient,subject,body,extra;


-(void)dealloc{
    self.recipient=nil;
    self.body=nil;
    self.subject=nil;
    self.extra=nil;
}

@end
