//
//  SubStopsTableViewCell.h
//  Mansion
//
//  Created by Falguni Shah on 11/28/14.
//
//

#import <UIKit/UIKit.h>
@class SubStopsTableViewCell;
@protocol MansionSubStopPlayerDelegate<NSObject>
@optional

-(void)didClickOnPlayButtonForCell:(SubStopsTableViewCell*)cell withPlayButton:(UIButton*)button;
-(void)didClickOnPlayingButtonForCell:(SubStopsTableViewCell*)cell withPlayButton:(UIButton*)button;
-(void)didDragAudioSlide:(UISlider*)audioSlider forCell:(SubStopsTableViewCell*)cell;
-(void)didClickOnMoreButtonForCell:(SubStopsTableViewCell*)cell withButton:(UIButton*)moreButton;
-(void)didClickOnMaximizedButtonForCell:(SubStopsTableViewCell*)cell withButton:(UIButton*)maximizedButton;
-(void)didSlideAudioSlider:(UISlider*)audioSlider; // used only to invalidate the timer for custom ogg player



@end
@interface SubStopsTableViewCell : UITableViewCell
@property(nonatomic,retain)UIButton *currentPlayingButton;
@property(nonatomic,retain)IBOutlet UIImageView *stopImageView;
@property(nonatomic,retain)IBOutlet UILabel *stopNumber,*stopName;
@property(nonatomic,retain)IBOutlet UIButton *playButton,*moreButton,*expandButton,*fakeButton;
@property(nonatomic,assign)id <MansionSubStopPlayerDelegate>delegate;
@property(nonatomic,retain)IBOutlet UISlider *audioPlayerSlider;
-(IBAction)playTheAudio:(UIButton*)sender;
-(IBAction)moreButtonClicked:(UIButton*)sender;
@end
