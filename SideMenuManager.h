//
//  SideMenuManager.h
//  GenericMansion
//
//  Created by Junaid Saify on 07/11/15.
//
//

#import <Foundation/Foundation.h>
#import "SideMenu.h"


#import "FMDBManager.h"
@interface SideMenuManager : NSObject


-(NSMutableArray*)fetchSideMenu;
-(NSMutableArray*)fetchTopMenu;
-(NSMutableArray*)fetchBottomMenu;
@end
