//
//  SegmentViewManager.h
//  GenericMansion
//
//  Created by Junaid Saify on 24/01/16.
//
//

#import <Foundation/Foundation.h>
#import "FMDBManager.h"
@interface SegmentViewManager : NSObject

-(NSMutableArray*)fetchSegmentViewDetails;
@end
