//
//  BottomMenu.m
//  GenericMansion
//
//  Created by Junaid Saify on 20/12/15.
//
//

#import "BottomMenu.h"

@implementation BottomMenu

@synthesize menuName,menuImagePath,menuSequence,menuOnClick,menuLink,menuBrowser,menuExtra;

-(instancetype)init{
    if(!self){
        self = [super init];
    }
    
    return  self;
}


-(void)dealloc{
    menuName=nil;
    menuImagePath=nil;
    menuSequence=nil;
    menuOnClick=nil;
    menuLink=nil;
    menuBrowser=nil;
    menuExtra=nil;
}


@end
