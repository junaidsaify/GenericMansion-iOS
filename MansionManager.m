//
//  MansionManager.m
//  GenericMansion
//
//  Created by Junaid Saify on 14/11/15.
//
//

#import "MansionManager.h"

@implementation MansionManager


-(NSMutableArray*)fetchAllMansions{
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM MansionDetails Order By Sequence ASC";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
    array=   [manager returnValueForQuery:query forTable:@"MansionDetails"];
    
    return array;
    
}


@end
