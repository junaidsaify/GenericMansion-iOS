//
//  SideMenuManager.m
//  GenericMansion
//
//  Created by Junaid Saify on 07/11/15.
//
//

#import "SideMenuManager.h"

@implementation SideMenuManager


-(NSMutableArray*)fetchSideMenu {
    
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM SideMenu Order By Sequence ASC";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
 array=   [manager returnValueForQuery:query forTable:@"SideMenu"];
    
    return array;
    
}


-(NSMutableArray*)fetchTopMenu{
    
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM TopMenu Order By Sequence ASC";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
    array=   [manager returnValueForQuery:query forTable:@"TopMenu"];
    
    return array;
}

-(NSMutableArray*)fetchBottomMenu{
    
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM BottomMenu Order By Sequence ASC";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
    array=   [manager returnValueForQuery:query forTable:@"BottomMenu"];
    
    return array;
    
}





@end
