//
//  FeedbackMailManager.h
//  GenericMansion
//
//  Created by Junaid Saify on 16/12/15.
//
//

#import <Foundation/Foundation.h>
#import "FMDBManager.h"
@interface FeedbackMailManager : NSObject

-(NSMutableArray*)fetchFeedbackMailDetails;
@end
