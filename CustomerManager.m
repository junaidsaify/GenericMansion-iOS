//
//  CustomerManager.m
//  GenericMansion
//
//  Created by Junaid Saify on 13/12/15.
//
//

#import "CustomerManager.h"

@implementation CustomerManager


-(NSMutableArray*)fetchCustomerDetails{
    NSMutableArray *array;
    NSString *query = @"SELECT * FROM Customer";
    FMDBManager *manager = [[FMDBManager alloc] init];
    
    array=   [manager returnValueForQuery:query forTable:@"Customer"];
    
    return array;
    
}

@end
