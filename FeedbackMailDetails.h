//
//  FeedbackMailDetails.h
//  GenericMansion
//
//  Created by Junaid Saify on 16/12/15.
//
//

#import <Foundation/Foundation.h>

@interface FeedbackMailDetails : NSObject
@property(nonatomic,retain)NSString *recipient;
@property(nonatomic,retain)NSString *subject;
@property(nonatomic,retain)NSString *body;
@property(nonatomic,retain)NSString *extra;
@end
