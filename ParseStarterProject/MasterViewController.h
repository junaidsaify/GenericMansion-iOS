//
//  MasterViewController.h
//  Mansion
//
//  Created by Falguni Shah on 10/18/14.
//
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"
#import "Mansion.h"
#import "MansionDownloadManager.h"
#import "AudioPlayer.h"
#import "MansionAudioPlayer.h"
#import "SubStopsViewController.h"
#import "GettingHereViewController.h"
#import "LearnViewController.h"
#import "AboutUsViewController.h"
#import "DirectionsViewController.h"
//..#import <FacebookSDK/FacebookSDK.h>
#import <Social/Social.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DBManager.h"
#import "WebViewController.h"
#import "EventsViewController.h"
#import "JoinViewController.h"
#import "ExploreViewController.h"
#import "ListViewTableViewCell-iPad.h"
#import "StoresViewController.h"
#import "NewDirectionViewController.h"
#import "IDZAQAudioPlayer.h"
#import "GAIDictionaryBuilder.h"
#import "MaximizedViewController.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"

#import "SideMenuManager.h"
#import "SideMenu.h"
#import "BottomMenu.h"
#import "MansionManager.h"
#import "CustomerManager.h"
#include "MansionModel.h"
#include "CustomerDetails.h"
#include "FeedbackMailManager.h"
#import "FeedbackMailDetails.h"
#import "SegmentViewManager.h"
#import "SegmentViewDetails.h"
#import "ManageMyDownloadViewController.h"
#import "MansionButton.h"
#import "GPSViewController.h"

@interface MasterViewController : GAITrackedViewController<UIScrollViewDelegate,MansionDownloadManagerDelegate,IDZAudioPlayerDelegate,MapProtocol>{
    NSMutableDictionary *audioDetailDict;
    BOOL isExpandTouched;
    int indexOfExpandCell;
    NSString *presentRoom;
    MansionAudioPlayer *audioPlayer;
    NSArray *subStops;
    NSMutableArray *subStopsName,*subStopsImagePath;
    SubStopsViewController *subStopVC;
    GettingHereViewController *gettingHereVC;
    LearnViewController *learnVC;
    AboutUsViewController *aboutUsVC;
    DirectionsViewController *directionVC;
    MansionDownloadManager *downloadManager;
    DBManager *dbManager;
    WebViewController *webViewCtrl;
    EventsViewController *eventsVC;
    JoinViewController *joinVC;
    ExploreViewController *exploreVC;
    StoresViewController *storesVC;
    NewDirectionViewController *newDirVC;
    MaximizedViewController *maxVC;
    GPSViewController *gpsVC;
    
    NSArray *mansionLearnArray,*mansionAboutUsArray,*allFloors;
    NSDictionary *aboutMansionDict,*storesDictionary;
   NSMutableArray *cellCollectionArray;
    IBOutlet UIImageView *leftArrow,*rightArrow,*tourImageView;
    NSTimer *audioTimer;
    IBOutlet UISlider *audioProgressSlider;
    UISwipeGestureRecognizer *swipeLeftRight;
    UIButton *dummy;
    UIImage *imageToBeMaximized,*imageToShare;
    NSString *currentMansionAddress;
    MansionModel *currentMansion;
    CustomerDetails *customerDetails;
    FeedbackMailDetails *feedbackMailDetails;
    SegmentViewDetails *segmentViewDetails;
    NSArray *segmentViewArray;
    UITapGestureRecognizer *tap,*tapOnView;
    
    BOOL pageSwipe,playerStopped;
    
    id<GAITracker> tracker;
    
     ManageMyDownloadViewController *manageDownload;
}
@property(nonatomic,retain)IBOutlet UIButton *segmentViewButton;
@property(nonatomic,retain)IBOutlet UIImageView *patchImageView,*customerLogo;
@property(nonatomic,retain) Mansion *mansion;
@property(nonatomic,strong)IBOutlet UITableView *sideTableView;
@property(nonatomic,weak)IBOutlet UIButton *downloadMediaButton,*sideMenuPopUpButton,*searchButton,*maximizedCloseButton,*baseFrameButton,*maximizedDoneButton,*roomNameFakeBotton,*actionShowLogoButton,*maximizeImageButton;
@property(nonatomic,strong)IBOutlet UIView *mainView,*sideView,*topView,*maximizedView,*maximizedBaseView;
@property(nonatomic,retain)IBOutlet UIScrollView *maximizedImageScrollView,*homePageScrollView,*mapScrollView;
@property(nonatomic,strong)IBOutlet UIView *mansionDetailsView,*tourDetailView,*downloadOptionView,*downloadOptionBaseView;
@property(nonatomic,retain) NSMutableArray *mansionArray,*sideMansionArray,*sideTableItems,*topMenuItems,*bottomMenuItems;
@property(nonatomic,retain)IBOutlet UIImageView *homeImage,*semiUp,*exteriorImage,*interiorImage,*maximizedImageView;
// properties of Mansion Home
@property(nonatomic,retain)IBOutlet UIButton *aboutButton,*tourButton,*directionButton,*ticketButton,*downloadButton,*languageButton,*deleteContentButton,*downloadMansionDataButton,*storesButton,*mansionNameButon,*greenPlayButton,*appFeedbackButton,*previousMansionButton,*nextMansionButton;
@property (strong, nonatomic) IBOutlet UIImageView *appFeedbackArrowImg;

@property(nonatomic,retain)IBOutlet UIView *bottomView,*downloadMansionView;
@property(nonatomic,retain)IBOutlet UIProgressView *downloadProgressSlider;
@property(nonatomic,retain)IBOutlet UILabel *progressLabel;
@property(nonatomic,retain)IBOutlet UITextView *aboutMansionTextView;
// properties of tourDetailView
@property(nonatomic,retain)IBOutlet UILabel*floorLabel,*roomLable;
@property(nonatomic,retain)IBOutlet UIButton *substopButton,*nextRoom,*previousRoom,*playPauseButton,*closeTranscriptButton,*actionSheetButton,*webActionSheetButton;
@property(nonatomic,copy) NSArray *floorRoomsArray;
@property(nonatomic,retain)NSMutableArray *allFloorRoomArray;
@property(nonatomic,retain)AudioPlayer *player;


@property(nonatomic,retain)IBOutlet UIView *transcriptView,*transcriptInnerView,*socialNetworkView;
@property(nonatomic,retain)IBOutlet UITextView *transcriptTextView;
-(IBAction)loadRooms:(UIButton*)sender;
-(IBAction)playTheAudio:(UIButton*)sender;
-(IBAction)showSubStopScreen:(UIButton*)sender;
-(IBAction)removeCurrentView:(id)sender;
-(IBAction)showActionSheet:(id)sender;
-(IBAction)showSegmentedView:(UIButton*)sender;
-(IBAction)removeSegmentedView:(id)sender;
-(IBAction)removeView:(UIButton*)sender;
- (IBAction)slide;
-(IBAction)openActionShowSite:(UIButton*)sender;

-(IBAction)loadStores:(UIButton*)sender;
// end
- (IBAction)showAppFeedback:(id)sender;

// Properties of Segmented View
@property(nonatomic,retain)IBOutlet UIView *segmentView,*floorMapView,*floorListView,*segmentTopView;
@property(nonatomic,retain)IBOutlet UITableView *floorListTableView;
@property(nonatomic,retain)IBOutlet UIImageView *floorMapImageView;
@property(nonatomic,retain)IBOutlet UISegmentedControl *segmentControl;
// End


//next and prev mansion
- (IBAction)showPrevious:(id)sender;
- (IBAction)showNext:(id)sender;
@property(nonatomic)NSInteger currentPage;


// Donate Method

-(IBAction)donateToday:(UIButton*)sender;

-(IBAction)showLeftMenu:(UIButton*)sender;
-(IBAction)loadScreenForDetail:(UIButton*)sender;

-(IBAction)downloadMansionData:(UIButton*)sender;
-(IBAction)startDownload:(id)sender;
-(IBAction)deleteMansionContent:(UIButton*)sender;

// Maximize Image
-(IBAction)maximizeTheImage:(UIButton*)sender;

// Mail
-(void)sendEmailWIthAttachments:(id)attachment withMessage:(NSString*)messageString;
@end
