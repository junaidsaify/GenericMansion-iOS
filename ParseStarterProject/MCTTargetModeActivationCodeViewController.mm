//
//  MCTTargetModeActivationCodeViewController.m
//  MCT
//
//  Created by Erwan Aguillon on 26/01/2015.
//  Copyright (c) 2015 Synchronoss Software Ltd. All rights reserved.
//

#include <sys/types.h>
#include <sys/sysctl.h>

#import <AVFoundation/AVFoundation.h>
#import "MCTTargetModeActivationCodeViewController.h"
#import "Decoder.h"
#import "NSString+HTML.h"
#import "resultParsers/ResultParser.h"
#import "parsedResults/ParsedResult.h"
#import "actions/ResultAction.h"
#import "TwoDDecoderResult.h"
#import "SDSecondaryButton.h"
#import "MCTStrings.h"
#import "SDCustomUI.h"
#import "ConfigurationManager.h"
#import "P2PConstants.h"
#import "GenericStrings.h"
#import "P2PUtils.h"
#import "SDBarButtonItem.h"
#import "CustomIOS7AlertView.h"
#import "UIColor+HexString.h"
#import "MCTPairingTools.h"
#import "SDPrimaryButton.h"
#import "MCTTargetModeActivationCongratulationViewController.h"
#import "SDAnalyticsWrapper.h"
#import "P2PConstants.h"
#import "AnalysisStrings.h"
#import "ScoreManager.h"

#define CAMERA_SCALAR       1.12412 // scalar = (480 / (2048 / 480))
#define FIRST_TAKE_DELAY    1.0
#define ONE_D_BAND_HEIGHT   10.0

#define kSDKConnectionFailureAlertViewTag 100000001

@interface OverlayView ()
-(instancetype)initWithFrame:(CGRect)theFrame cancelEnabled:(BOOL)cancelEnabled oneDMode:(BOOL)isOneDModeEnabled;
@end

@interface MCTTargetModeActivationCodeViewController () {
    UILabel *scanYourQRCodeLabel;
}

@property (nonatomic, retain) MBProgressHUD *configHUD;

- (void)initCapture;
- (void)stopCapture;
- (void)displayCongratulationScreen;
- (BOOL)validateSizeAndFormat:(NSString*)text;

@end

@implementation MCTTargetModeActivationCodeViewController

#if HAS_AVFF
@synthesize captureSession;
@synthesize prevLayer;
#endif
@synthesize result, delegate, soundToPlay;
@synthesize overlayView;
@synthesize readers;
@synthesize isDisplayedAsOption;
@synthesize textField;
@synthesize waitingView;
@synthesize activityIndicatorView;
@synthesize voucherCodeView;

- (id)initWithDelegate:(id<ActivationCodeDelegate>)scanDelegate  {
    if ((self = [super init])) {
        beepSound = -1;
        decoding = NO;
        
        [self setDelegate:scanDelegate];
    }
    
    return self;
}

- (void)dealloc {
    if (beepSound != (SystemSoundID)-1) {
        AudioServicesDisposeSystemSoundID(beepSound);
    }
    
    [self stopCapture];
    [result release];
    [soundToPlay release];
    [readers release];
    [textField release];
    [overlayView release];
    [voucherCodeView release];
    [super dealloc];
}

#pragma mark View Lifecycle Methods
- (void)viewDidLoad {
    
    OverlayView *theOverLayView = nil;
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    self.title = MCT_ACTIVATION_CODE_TITLE;
    
    id activateButton = [[[SDBarButtonItem alloc] initWithTitle:MCT_ACTIVATION_ACTIVATE_BUTTON_LABEL style:UIBarButtonItemStyleDone target:self action:@selector(activateButtonWasTapped:)] autorelease];
    [self.navigationItem setRightBarButtonItem:activateButton];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    self.view.backgroundColor = [SDCustomUI customBackgroundColor1];
    
    //OverlayView has a default way to display a message but we can not set the text color (displayed with drawInRect)
    UILabel *headerLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width - 20.0, 40.0)] autorelease];
    headerLabel.backgroundColor = [SDCustomUI customBackgroundColor1];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont systemFontOfSize:16.0];
    headerLabel.numberOfLines = 2.0;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.text = MCT_ACTIVATION_CODE_INPUT_LABEL;
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 40.0, self.view.frame.size.width - 20.0, 30.0)];
    textField.backgroundColor = [SDCustomUI customBackgroundColor1];
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont systemFontOfSize:16.0];
    textField.delegate = self;
    textField.borderStyle = UITextBorderStyleLine;
    
    waitingView = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
    waitingView.backgroundColor = [UIColor grayColor];
    waitingView.alpha = 0.5;
    activityIndicatorView = [[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(138, 200, 40, 40)] autorelease];
    activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [waitingView addSubview:activityIndicatorView];
    

    if([[[ConfigurationManager sharedInstance] objectForP2P:@"P2PVoucherScanQrCodeEnabled"] boolValue])
    {

        theOverLayView = [[OverlayView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-(self.navigationController.navigationBar.frame.size.height+[[UIApplication sharedApplication] statusBarFrame].size.height)) cancelEnabled:NO oneDMode:NO];
        
        CGRect cropRectFrame = theOverLayView.cropRect;
        cropRectFrame.origin.y = theOverLayView.cropRect.origin.y + 50;
        cropRectFrame.size.height = theOverLayView.cropRect.size.height - 40;
        theOverLayView.cropRect = cropRectFrame;
        
        theOverLayView.displayedMessage = @" ";
        self.overlayView = theOverLayView;
        [theOverLayView release];
        

        [theOverLayView addSubview:headerLabel];

        [theOverLayView addSubview:textField];
        
        float yForLabel = self.overlayView.cropRect.origin.y - 40;
        scanYourQRCodeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0, yForLabel, self.view.frame.size.width - 20.0, 45.0)] autorelease];
        scanYourQRCodeLabel.backgroundColor = [UIColor clearColor];
        scanYourQRCodeLabel.textColor = [UIColor blackColor];
        scanYourQRCodeLabel.font = [UIFont systemFontOfSize:14.0];
        scanYourQRCodeLabel.numberOfLines = 2.0;
        scanYourQRCodeLabel.textAlignment = NSTextAlignmentCenter;
        scanYourQRCodeLabel.text = MCT_ACTIVATION_CODE_SCAN_CODE_LABEL;
        [theOverLayView addSubview:scanYourQRCodeLabel];
        
        SDPrimaryButton* infoButton = [[[SDPrimaryButton alloc] init] autorelease];
        [infoButton addTarget:self action:@selector(infoButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
        int heightInfoButton = 30.0;
        infoButton.frame = CGRectMake(90.0, theOverLayView.frame.size.height - (10.0 + heightInfoButton), 140.0, heightInfoButton);
        [infoButton setTitle:MCT_ACTIVATION_CODE_HELP_BUTTON forState:UIControlStateNormal];
        [theOverLayView addSubview:infoButton];
        [theOverLayView bringSubviewToFront:infoButton];
        

        [theOverLayView addSubview:waitingView];
        [theOverLayView bringSubviewToFront:waitingView];
        
    }
    else
    {
        voucherCodeView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-(self.navigationController.navigationBar.frame.size.height+[[UIApplication sharedApplication] statusBarFrame].size.height))];

        [voucherCodeView addSubview:headerLabel];

        [voucherCodeView addSubview:textField];
  
        [voucherCodeView addSubview:waitingView];
        [voucherCodeView bringSubviewToFront:waitingView];
    }
    
    waitingView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationItem] setHidesBackButton:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self stopCapture];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];

    decoding = YES;
    
    if([[[ConfigurationManager sharedInstance] objectForP2P:@"P2PVoucherScanQrCodeEnabled"] boolValue])
    {
        [self initCapture];
        [self.view addSubview:overlayView];
        [overlayView setPoints:nil];
    }
    else
    {
        [self.view addSubview:voucherCodeView];
    }
    
    wasCancelled = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.overlayView removeFromSuperview];
    [self stopCapture];
}

-(void)resignResponders
{
    [textField resignFirstResponder];
}

#pragma
#pragma UITextFieldDelegate methods
#pragma
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.location > 0 || (range.location == 0 && [string length] > 0) )
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.textField resignFirstResponder];
    return YES;
}

#pragma mark User Interaction
-(void)activateButtonWasTapped:(id)sender
{
    decoding = NO;
    
    if([self validateSizeAndFormat:self.textField.text])
    {
        waitingView.hidden = NO;
        [activityIndicatorView startAnimating];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.navigationItem.backBarButtonItem.enabled = NO;
        
        [[PCVoucherManager sharedInstance] validateVoucher:self.textField.text withDelegate:self];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                message:MCT_ACTIVATION_CODE_ERROR_FORMAT
                                                               delegate:self
                                                      cancelButtonTitle:GENERIC_BUTTON_OK
                                                      otherButtonTitles:nil,nil];
            [alertView show];
        });
    }
}

-(void)infoButtonWasTapped:(id)sender
{
    decoding = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                 message:MCT_ACTIVATION_CODE_HELP
                                                                delegate:nil
                                                       cancelButtonTitle:GENERIC_BUTTON_OK
                                                       otherButtonTitles:nil, nil] autorelease];
        alertView.delegate = self;
        [alertView show];
    });
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    decoding = YES;
}

#pragma mark Rotation Methods (Don't allow landscape in the scanner)

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}



- (CGImageRef)CGImageRotated90:(CGImageRef)imgRef {
    CGFloat angleInRadians = -90 * (M_PI / 180);
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGRect imgRect = CGRectMake(0, 0, width, height);
    CGAffineTransform transform = CGAffineTransformMakeRotation(angleInRadians);
    CGRect rotatedRect = CGRectApplyAffineTransform(imgRect, transform);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bmContext = CGBitmapContextCreate(NULL,
                                                   rotatedRect.size.width,
                                                   rotatedRect.size.height,
                                                   8,
                                                   0,
                                                   colorSpace,
                                                   kCGImageAlphaPremultipliedFirst);
    CGContextSetAllowsAntialiasing(bmContext, FALSE);
    CGContextSetInterpolationQuality(bmContext, kCGInterpolationNone);
    CGColorSpaceRelease(colorSpace);
    //      CGContextTranslateCTM(bmContext,
    //                                                +(rotatedRect.size.width/2),
    //                                                +(rotatedRect.size.height/2));
    CGContextScaleCTM(bmContext, rotatedRect.size.width/rotatedRect.size.height, 1.0);
    CGContextTranslateCTM(bmContext, 0.0, rotatedRect.size.height);
    CGContextRotateCTM(bmContext, angleInRadians);
    //      CGContextTranslateCTM(bmContext,
    //                                                -(rotatedRect.size.width/2),
    //                                                -(rotatedRect.size.height/2));
    CGContextDrawImage(bmContext, CGRectMake(0, 0,
                                             rotatedRect.size.width,
                                             rotatedRect.size.height),
                       imgRef);
    
    CGImageRef rotatedImage = CGBitmapContextCreateImage(bmContext);
    CFRelease(bmContext);
    [(id)rotatedImage autorelease];
    
    return rotatedImage;
}

- (CGImageRef)CGImageRotated180:(CGImageRef)imgRef {
    CGFloat angleInRadians = M_PI;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bmContext = CGBitmapContextCreate(NULL,
                                                   width,
                                                   height,
                                                   8,
                                                   0,
                                                   colorSpace,
                                                   kCGImageAlphaPremultipliedFirst);
    CGContextSetAllowsAntialiasing(bmContext, FALSE);
    CGContextSetInterpolationQuality(bmContext, kCGInterpolationNone);
    CGColorSpaceRelease(colorSpace);
    CGContextTranslateCTM(bmContext,
                          +(width/2),
                          +(height/2));
    CGContextRotateCTM(bmContext, angleInRadians);
    CGContextTranslateCTM(bmContext,
                          -(width/2),
                          -(height/2));
    CGContextDrawImage(bmContext, CGRectMake(0, 0, width, height), imgRef);
    
    CGImageRef rotatedImage = CGBitmapContextCreateImage(bmContext);
    CFRelease(bmContext);
    [(id)rotatedImage autorelease];
    
    return rotatedImage;
}

#pragma mark Decoder Methods

- (void)decoder:(Decoder *)decoder willDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset{
}

- (void)decoder:(Decoder *)decoder decodingImage:(UIImage *)image usingSubset:(UIImage *)subset {
}

- (void)presentResultForString:(NSString *)resultString {
    self.result = [ResultParser parsedResultForString:resultString];
}

- (void)presentResultPoints:(NSArray *)resultPoints forImage:(UIImage *)image usingSubset:(UIImage *)subset {
    // simply add the points to the image view
    //NSMutableArray *mutableArray = [[NSMutableArray alloc] initWithArray:resultPoints];
    //[overlayView setPoints:mutableArray];
    //[mutableArray release];
}

- (void)decoder:(Decoder *)decoder didDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset withResult:(TwoDDecoderResult *)twoDResult {
    
    decoding = NO;
    [self setVoucherCode:[twoDResult text]];
       if([MCTPairingTools isPairingQRCode:[twoDResult text]])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                message:MCT_ACTIVATION_CODE_IS_FOR_PAIRING
                                                               delegate:self
                                                      cancelButtonTitle:GENERIC_BUTTON_OK
                                                      otherButtonTitles:nil,nil];
            [alertView show];
        });
    }
    else if ([self validateSizeAndFormat:[twoDResult text]])
    {
        [self presentResultForString:[twoDResult text]];
        [self presentResultPoints:[twoDResult points] forImage:image usingSubset:subset];
        
        if([[PCVoucherManager sharedInstance] isEnabled])
        {
            waitingView.hidden = NO;
            [activityIndicatorView startAnimating];
            self.navigationItem.rightBarButtonItem.enabled = NO;
            self.navigationItem.backBarButtonItem.enabled = NO;
            
            [[PCVoucherManager sharedInstance] validateVoucher:[twoDResult text] withDelegate:self];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                             message:MCT_ACTIVATION_CODE_INVALID_BARCODE
                                                            delegate:self
                                                             cancelButtonTitle:GENERIC_BUTTON_OK
                                                             otherButtonTitles:nil,nil];
            [alertView show];
            });
    }
    
}

- (BOOL)validateSizeAndFormat:(NSString*)text {

    if([text length] >= 1)
    {
        return YES;
    }
    
    return NO;
}

- (void)displayCongratulationScreen {
    [overlayView removeFromSuperview];
    [self stopCapture];
    
    MCTTargetModeActivationCongratulationViewController *activationCongratulationVC = [[UIStoryboard storyboardWithName:@"P2PTargetModeIPhone" bundle:[P2PUtils uiBundle]] instantiateViewControllerWithIdentifier:@"TargetModeActivationCongratulationViewControllerID"];
    activationCongratulationVC.delegate = self.delegate;
    [self.navigationController pushViewController:activationCongratulationVC animated:YES];
}

- (void)decoder:(Decoder *)decoder failedToDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset reason:(NSString *)reason {
    decoder.delegate = nil;
    [overlayView setPoints:nil];
}

- (void)decoder:(Decoder *)decoder foundPossibleResultPoint:(CGPoint)point {
    [overlayView setPoint:point];
}


#pragma mark -
#pragma mark AVFoundation

#include <sys/types.h>
#include <sys/sysctl.h>

- (void)initCapture {
#if HAS_AVFF
    AVCaptureDevice* inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *captureInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:nil];
    
    if (!captureInput) {
        return;
    }
    
    AVCaptureVideoDataOutput *captureOutput = [[AVCaptureVideoDataOutput alloc] init];
    captureOutput.alwaysDiscardsLateVideoFrames = YES;
    
    if ([P2PUtils SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO:@"8.0"]) {
        [captureOutput setSampleBufferDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    } else {
        [captureOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    }

    NSString* key = (NSString*)kCVPixelBufferPixelFormatTypeKey;
    NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
    NSDictionary* videoSettings = [NSDictionary dictionaryWithObject:value forKey:key];
    [captureOutput setVideoSettings:videoSettings];
    self.captureSession = [[[AVCaptureSession alloc] init] autorelease];
    
    NSString* preset = 0;
    
    if (!preset) {
        preset = AVCaptureSessionPresetMedium;
    }
    self.captureSession.sessionPreset = preset;
    
    [self.captureSession addInput:captureInput];
    [self.captureSession addOutput:captureOutput];
    
    [captureOutput release];
    
    if (!self.prevLayer) {
        self.prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    }
    
    self.prevLayer.frame = CGRectMake(self.overlayView.cropRect.origin.x, self.overlayView.cropRect.origin.y, self.overlayView.cropRect.size.width, self.overlayView.cropRect.size.height);
    self.prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer: self.prevLayer];

    if ([P2PUtils SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO:@"8.0"]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self.captureSession startRunning];
        });
    } else {
        [self.captureSession startRunning];
    }
    
#endif
}

#if HAS_AVFF
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    if (!decoding) {
        return;
    }
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    /*Lock the image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    /*Get information about the image*/
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // DLog(@"wxh: %lu x %lu", width, height);
    
    uint8_t* baseAddress = (uint8_t *) CVPixelBufferGetBaseAddress(imageBuffer);
    void* free_me = 0;
    if (true) { // iOS bug?
        uint8_t* tmp = baseAddress;
        size_t bytes = bytesPerRow*height;
        free_me = baseAddress = (uint8_t*)malloc(bytes);
        baseAddress[0] = 0xdb;
        memcpy(baseAddress,tmp,bytes);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef newContext =
    CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace,
                          kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst);
    
    CGImageRef capture = CGBitmapContextCreateImage(newContext);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    free(free_me);
    
    CGContextRelease(newContext);
    CGColorSpaceRelease(colorSpace);
    
    if (false) {
        CGRect cropRect = [overlayView cropRect];
        if (oneDMode) {
            // let's just give the decoder a vertical band right above the red line
            cropRect.origin.x = cropRect.origin.x + (cropRect.size.width / 2) - (ONE_D_BAND_HEIGHT + 1);
            cropRect.size.width = ONE_D_BAND_HEIGHT;
            // do a rotate
            CGImageRef croppedImg = CGImageCreateWithImageInRect(capture, cropRect);
            CGImageRelease(capture);
            capture = [self CGImageRotated90:croppedImg];
            capture = [self CGImageRotated180:capture];
            //              UIImageWriteToSavedPhotosAlbum([UIImage imageWithCGImage:capture], nil, nil, nil);
            CGImageRelease(croppedImg);
            CGImageRetain(capture);
            cropRect.origin.x = 0.0;
            cropRect.origin.y = 0.0;
            cropRect.size.width = CGImageGetWidth(capture);
            cropRect.size.height = CGImageGetHeight(capture);
        }
        
        // N.B.
        // - Won't work if the overlay becomes uncentered ...
        // - iOS always takes videos in landscape
        // - images are always 4x3; device is not
        // - iOS uses virtual pixels for non-image stuff
        
        {
            float height = CGImageGetHeight(capture);
            float width = CGImageGetWidth(capture);
            
            DLog(@"%f %f", width, height);
            
            CGRect screen = UIScreen.mainScreen.bounds;
            float tmp = screen.size.width;
            screen.size.width = screen.size.height;;
            screen.size.height = tmp;
            
            cropRect.origin.x = (width-cropRect.size.width)/2;
            cropRect.origin.y = (height-cropRect.size.height)/2;
        }
        
        DLog(@"sb %@", NSStringFromCGRect(UIScreen.mainScreen.bounds));
        DLog(@"cr %@", NSStringFromCGRect(cropRect));
        
        CGImageRef newImage = CGImageCreateWithImageInRect(capture, cropRect);
        CGImageRelease(capture);
        capture = newImage;
    }
    
    UIImage* scrn = [[[UIImage alloc] initWithCGImage:capture] autorelease];
    
    CGImageRelease(capture);
    
    Decoder* d = [[Decoder alloc] init];
    d.readers = readers;
    d.delegate = self;
    
    decoding = [d decodeImage:scrn] == YES ? NO : YES;
    
    [d release];
    
    if (decoding) {
        
        d = [[Decoder alloc] init];
        d.readers = readers;
        d.delegate = self;
        
        scrn = [[[UIImage alloc] initWithCGImage:scrn.CGImage
                                           scale:1.0
                                     orientation:UIImageOrientationLeft] autorelease];
        
        // DLog(@"^ %@ %f", NSStringFromCGSize([scrn size]), scrn.scale);
        decoding = [d decodeImage:scrn] == YES ? NO : YES;
        
        [d release];
    }
    
}
#endif

- (void)stopCapture {
    decoding = NO;
#if HAS_AVFF
    [captureSession stopRunning];
    AVCaptureInput* input = [captureSession.inputs objectAtIndex:0];
    [captureSession removeInput:input];
    AVCaptureVideoDataOutput* output = (AVCaptureVideoDataOutput*)[captureSession.outputs objectAtIndex:0];
    [captureSession removeOutput:output];
    [self.prevLayer removeFromSuperlayer];
    
    self.prevLayer = nil;
    self.captureSession = nil;
#endif
}


- (NSString *)getPlatform {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = (char *)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    return platform;
}

- (BOOL)fixedFocus {
    NSString *platform = [self getPlatform];
    if ([platform isEqualToString:@"iPhone1,1"] ||
        [platform isEqualToString:@"iPhone1,2"]) return YES;
    return NO;
}

#pragma
#pragma PCVoucherManagerDelegate
#pragma

- (void)onResult:(PCVoucherCodeResult*)a_result
{
    [activityIndicatorView stopAnimating];
    waitingView.hidden = YES;
    if([textField.text length] > 0)
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    self.navigationItem.backBarButtonItem.enabled = YES;

    switch (a_result.status)
    {
        case VOUCHER_MNGR_RESULT_OK:
            [[SDAnalyticsWrapper analyticApi]customProperty:@"Success" forKey:MCT_SET_VOUCHERVALIDATION_STATUS];
            [self setVoucherCode:self.textField.text];
            [self performSelectorOnMainThread:@selector(displayCongratulationScreen) withObject:nil waitUntilDone:NO];
            break;
        
        case VOUCHER_MNGR_RESULT_INVALID_RESPONSE:
            [[SDAnalyticsWrapper analyticApi]customProperty:@"Failure" forKey:MCT_SET_VOUCHERVALIDATION_STATUS];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_INVALID_RESPONSE
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                [alertView show];
            });
            break;
        
        case VOUCHER_MNGR_RESULT_ERROR_NETWORK:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_ERROR_NETWORK
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                [alertView show];
            });
            break;
            
        case VOUCHER_MNGR_RESULT_CODE_EMPTY:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_BODY_EMPTY
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                [alertView show];
            });
            break;
            
        case VOUCHER_MNGR_RESULT_CODE_NOT_STARTED:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = nil;
                if(a_result.startDate != nil && a_result.endDate != nil)
                {
                    NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
                    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                    alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                           message:[NSString stringWithFormat:MCT_ACTIVATION_CODE_VALID_FROM_DATE_TO_DATE, [dateFormatter stringFromDate:a_result.startDate], [dateFormatter stringFromDate:a_result.endDate]]
                                                          delegate:self
                                                 cancelButtonTitle:GENERIC_BUTTON_OK
                                                 otherButtonTitles:nil,nil];
                }
                else
                {
                    alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_ERROR_NOT_STARTED
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                }
                [alertView show];
            });
            break;
            
        case VOUCHER_MNGR_RESULT_CODE_EXPIRED:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = nil;
                if(a_result.endDate != nil)
                {
                    NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
                    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
                    alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                           message:[NSString stringWithFormat:MCT_ACTIVATION_CODE_ERROR_EXPIRED_ON_DATE, [dateFormatter stringFromDate:a_result.endDate]]
                                                          delegate:self
                                                 cancelButtonTitle:GENERIC_BUTTON_OK
                                                 otherButtonTitles:nil,nil];
                }
                else
                {
                    alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                           message:MCT_ACTIVATION_CODE_ERROR_EXPIRED
                                                          delegate:self
                                                 cancelButtonTitle:GENERIC_BUTTON_OK
                                                 otherButtonTitles:nil,nil];
                }
                [alertView show];
            });
            break;
            
        case VOUCHER_MNGR_RESULT_CODE_MAX_REACHED:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_ERROR_MAX_REACHED
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                [alertView show];
            });
            break;
            
        case VOUCHER_MNGR_RESULT_CODE_REVOKED:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_ERROR_REVOKED
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                [alertView show];
            });
            break;
            
        case VOUCHER_MNGR_RESULT_CODE_NOT_FOUND:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:MCT_ACTIVATION_CODE_ALERT_TITLE
                                                                    message:MCT_ACTIVATION_CODE_ERROR_NOT_FOUND
                                                                   delegate:self
                                                          cancelButtonTitle:GENERIC_BUTTON_OK
                                                          otherButtonTitles:nil,nil];
                [alertView show];
            });
            break;
            
        default:
            break;
    }
}
-(void)setVoucherCode:(NSString *)voucherID
{
    NSString* txnId = [ScoreManager sharedInstance].txn_id;
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:txnId,@"txnID",voucherID,@"voucherID",nil];
    [[SDAnalyticsWrapper analyticApi] customProperty:dict forKey:@"setVoucherID"];
}

@end
