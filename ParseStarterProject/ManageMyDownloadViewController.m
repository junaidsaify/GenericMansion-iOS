//
//  ManageMyDownloadViewController.m
//  GenericMansion
//
//
//
//

#import "ManageMyDownloadViewController.h"

#define ElmsTourPath @"http://actiontour.blob.core.windows.net/newportmansions/junaid.zip"

@interface ManageMyDownloadViewController ()

@end

@implementation ManageMyDownloadViewController
@synthesize mansionArray,deleteButtonURL;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentCell.tag=-1;
    currentCell=nil;
    isDownloadInProgress = NO;
    [self prepareDownloadedMansionArray];
    
    self.tableBaseView.layer.borderWidth=5.0;
    self.tableBaseView.layer.borderColor = [UIColor colorWithString:@"#00563F"].CGColor;
    
    if(isIphone){
        self.mansionList.layer.borderWidth=3.0;
        self.mansionList.layer.borderColor = [UIColor colorWithString:@"#00563F"].CGColor;
    }

    
}

-(void) prepareDownloadedMansionArray{
    NSFileManager *manager = [NSFileManager defaultManager];
    if(downloadedMansionArray){
        [downloadedMansionArray removeAllObjects];
        downloadedMansionArray=nil;
    }
    downloadedMansionArray = [[NSMutableArray alloc] initWithCapacity:1];
    for(int i=0; i<self.mansionArray.count;i++){
        MansionModel *mansion = [self.mansionArray objectAtIndex:i];
        NSString *tourPath = [DocumentDirectory stringByAppendingPathComponent:mansion.shortName];
        if([manager fileExistsAtPath:tourPath isDirectory:NO]){
            [downloadedMansionArray addObject:mansion];
        }
        
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)removeView:(UIButton*)sender{
    [self.view removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma Tableview Delegate and Datasource


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isIphone){
        return 50;
    }
    else{
        return 80;
    }
    

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
 return  [mansionArray count];
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ManageDownloadTableViewCell *cell = (ManageDownloadTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSArray *nib;
        if(isIphone){
            nib = [[NSBundle mainBundle] loadNibNamed:@"ManageDownloadTableViewCell" owner:self options:nil];
        }
        else{
            nib = [[NSBundle mainBundle] loadNibNamed:@"ManageDownloadTableViewCell-iPad" owner:self options:nil];
        }
        
        cell = (ManageDownloadTableViewCell*) [nib objectAtIndex:0];
        cell.delegate=self;
        [cell.deleteNow sd_setBackgroundImageWithURL:[NSURL URLWithString:deleteButtonURL] forState:UIControlStateNormal placeholderImage:nil];
    }
    
  
    
    
    MansionModel *mansion = [mansionArray objectAtIndex:indexPath.row];
    cell.mansionName.text= mansion.name;
    cell.downloadNow.tag= indexPath.row;
    cell.tag=indexPath.row;
    cell.deleteNow.tag=indexPath.row;
    
    // set all properties
    if(currentCell.tag == cell.tag && currentCell!=nil){
        currentCell=cell;
    }

    
    
    if([downloadedMansionArray containsObject:mansion]){
    
        [cell.downloadNow setTitle:@"Downloaded" forState:UIControlStateNormal];
        [cell.deleteNow setHidden:NO];
        [cell.progressLable setHidden:YES];
        [cell.progress setHidden:YES];
    }
    
    else if(isDownloadInProgress && currentCell==cell){
        [cell.deleteNow setHidden:YES];
        cell.progress.hidden=NO;
        cell.progressLable.hidden=NO;
        cell.downloadNow.hidden=YES;
        
    }
    
    
    else {
        [cell.downloadNow setTitle:@"Download Now" forState:UIControlStateNormal];
        [cell.progressLable setText:@"0%% of 0 MB"];
        [cell.progress setProgress:0.0];
        [cell.downloadNow setHidden:NO];
        [cell.deleteNow setHidden:YES];
        [cell.progressLable setHidden:YES];
        [cell.progress setHidden:YES];
    }
    
    
    
    
    
    
    
    return cell;
    
    
}


#pragma Cell delegate methods

-(void)didClickOnDownloadButtonForCell:(ManageDownloadTableViewCell*)cell withDownloadButton:(UIButton*)button{
    
    if(![self connectedToInternet]){
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Please Connect to Internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if([downloadedMansionArray containsObject:[self.mansionArray objectAtIndex:cell.tag]]){
        return;
    }
    
    if(isDownloadInProgress){
        return;
    }
    
    
    currentCell=cell;
    MansionModel *mansion = [mansionArray objectAtIndex:cell.tag];
    NSString *tourPath = [mansion.tourPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"The tour Path is %@", tourPath);
    
   
    
   downloadManager=[[MansionDownloadManager alloc] init];
    downloadManager.delegate=self;
  
    [downloadManager downloadFileForAddress:tourPath forMansion:mansion.shortName];
    
    
    
}
-(void)didClickOnDeleteButtonForCell:(ManageDownloadTableViewCell*)cell withDeleteButton:(UIButton*)button{
    
     NSFileManager *manager = [NSFileManager defaultManager];
    MansionModel *mansion = [mansionArray objectAtIndex:cell.tag];
    NSString *deleteTour = mansion.shortName;
    NSLog(@"Tour to delete is %@", deleteTour);
    NSString *tourPath = [DocumentDirectory stringByAppendingPathComponent:deleteTour];

     if([manager fileExistsAtPath:tourPath isDirectory:NO]){
         
         [manager removeItemAtPath:tourPath error:nil];
         [downloadedMansionArray removeObject:mansion];
     }
    
    [self.mansionList reloadData];
    
    
    
}


#pragma Download Manager Delegate

-(void)downloadManagerDidRecieveResponseWithContentLength:(long)contentLength{
    if(contentLength<=0){
            [self stopTheDownload:nil];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Resource Missing At Path" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    

        return;
          }
    
    isDownloadInProgress=YES;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    NSLog(@"Download Began");
}
-(void)downloadManagerDidRecieveResponseWithPercent:(float)percent{
    NSLog(@"Percent began");
    int displayPercent = (percent/1)*100;
    currentCell.progress.hidden=NO;
    currentCell.progressLable.hidden=NO;
    currentCell.downloadNow.hidden=YES;
    [currentCell.progress setProgress:percent animated:YES];
//..    [currentCell.progressLable setText:[NSString stringWithFormat:@"%d%%",displayPercent]];
    
    [currentCell.progressLable setText:[NSString stringWithFormat:@"%d%% of %@ MB",displayPercent,[[mansionArray objectAtIndex:currentCell.tag] fileSizeInMB]]];
}


-(void)downloadManagerDidFinishLoading{
    
     isDownloadInProgress=NO;
    [self prepareDownloadedMansionArray];
    [self.mansionList reloadData];
    
/*    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    NSLog(@"Download Finished");
    //..  [self.downloadButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
    [self.downloadButton setTitle:@"Download Complete" forState:UIControlStateNormal];
    self.downloadButton.layer.cornerRadius=0.0;
    self.downloadButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
    [self.progressLabel setText:@"0%"];
    self.progressLabel.hidden=YES;
    [self.downloadButton setHidden:NO];
    [self.downloadProgressSlider setProgress:0.0 animated:YES];
    [self.downloadProgressSlider setHidden:YES];
    //..[self.downloadButton setTitle:@"" forState:UIControlStateNormal];
    [self.downloadButton setUserInteractionEnabled:NO];
    CGRect dlButtonRect =self.downloadButton.frame;
    //..  self.downloadButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 32, 25);
    self.deleteContentButton.hidden=NO;
    
    [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
    [self.downloadMansionDataButton setTitle:@"" forState:UIControlStateNormal];
    CGRect ButtonRect =self.downloadMansionDataButton.frame;
    self.downloadMansionDataButton.frame=CGRectMake(ButtonRect.origin.x, ButtonRect.origin.y, 52, 40);
    
    [self.mansionNameButon setUserInteractionEnabled:YES];
    
 
    
    //..   if([self.mansionArray indexOfObject:currentMansion]==0 && [self.mansionArray containsObject:currentMansion])
    if(downloadManager.shouldJump)
    {
        
        // remove view and show the tour
        downloadManager=nil;
        [self stopTheDownload:nil];// remove view
        UIButton *tButton =[UIButton buttonWithType:UIButtonTypeCustom];
        tButton.tag=2;
        [self loadScreenForDetail:tButton];
    }
    
    */
}
-(void)downloadManagerDidFailWithError:(NSError*)error{
    isDownloadInProgress=NO;
    [self.mansionList reloadData];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Sorry" message:error.description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
  /*  [self.progressLabel setText:@"0%"];
    [self.downloadButton setHidden:NO];
    [self.downloadButton setUserInteractionEnabled:YES];
    [self.mansionNameButon setUserInteractionEnabled:YES];
    [self.downloadProgressSlider setProgress:0.0 animated:YES];
    [self.downloadProgressSlider setHidden:YES];
    [self.progressLabel setHidden:YES];
    [self.deleteContentButton setHidden:YES];  */
}
-(IBAction)stopTheDownload:(UIButton*)sender{
    if(downloadManager){
        [downloadManager stopTheDownload];
        downloadManager=nil;
    }
    
   [self.view removeFromSuperview];
    
   
}





#pragma Network Check

- (BOOL) connectedToInternet
{
    NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/m"];
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    if (data)
        return true;
    else
        return false;
}






@end
