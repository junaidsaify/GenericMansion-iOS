//
//  JoinViewController.m
//  Mansion
//
//  Created by Falguni Shah on 3/5/15.
//
//

#import "JoinViewController.h"

@interface JoinViewController ()

@end

@implementation JoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName=@"Join&SupportScreen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)removeTheView:(id)sender{
    [self.view removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    NSLog(@"URL Hit");
  //..  NSString *urlString=[[NSURL URLWithString:DonateLink] absoluteString];
    //@"http://mobile.newportmansions.org/join-and-support"
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.thankyouforsupport.org/nwpmansions/join.html"]];
  //..  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
  /*  webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    webViewCtrl.urlString=urlString;
    //.. [self presentViewController:webViewCtrl animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil]; */
    return NO;
}


@end
