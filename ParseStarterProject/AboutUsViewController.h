//
//  AboutUsViewController.h
//  Mansion
//
//  Created by Falguni Shah on 12/19/14.
//
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
@interface AboutUsViewController : GAITrackedViewController<UITextViewDelegate>
{
    NSArray *mansionAboutUsArray;
    WebViewController *webViewCtrl;
}
@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;
@property(nonatomic,retain)IBOutlet UIView *view1,*view2,*view3;
@property(nonatomic,retain)IBOutlet UILabel *headerLabel;
@end
