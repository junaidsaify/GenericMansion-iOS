//
//  P2PTargetModeQRCodeScannerViewController.m
//  SyncDriveCore
//
//  Created by Alan Carney on 05/09/2013.
//  Copyright (c) 2013 Newbay Software Ltd. All rights reserved.
//

#include <sys/types.h>
#include <sys/sysctl.h>

#import <AVFoundation/AVFoundation.h>
#import "P2PTargetModeQRCodeScannerViewController.h"
#import "Decoder.h"
#import "NSString+HTML.h"
#import "resultParsers/ResultParser.h"
#import "parsedResults/ParsedResult.h"
#import "actions/ResultAction.h"
#import "TwoDDecoderResult.h"
#import "SDSecondaryButton.h"
#import "P2PStrings.h"
#import "SDCustomUI.h"
#import "ConfigurationManager.h"
#import "P2PConstants.h"
#import "SDAnalyticsWrapper.h"
#import "GenericStrings.h"
#import "P2PUtils.h"
#import "SDBarButtonItem.h"
#import "CustomIOS7AlertView.h"
#import "UIColor+HexString.h"
#import "SDDeviceUtils.h"
#import "MCTPairingTools.h"


#if SYNCDRIVECORE==0
    #import "CloudFallbackStrings.h"
    #import "MCTCloudFallbackSignInEmailViewController.h"
    #import "AppDelegate.h"
#endif

#define CAMERA_SCALAR       1.12412 // scalar = (480 / (2048 / 480))
#define FIRST_TAKE_DELAY    1.0
#define ONE_D_BAND_HEIGHT   10.0

#define kSDKConnectionFailureAlertViewTag 100000001


@interface OverlayView ()
-(instancetype)initWithFrame:(CGRect)theFrame cancelEnabled:(BOOL)cancelEnabled oneDMode:(BOOL)isOneDModeEnabled;
-(instancetype)initWithFrame:(CGRect)theFrame cancelEnabled:(BOOL)cancelEnabled oneDMode:(BOOL)isOneDModeEnabled showLicense:(BOOL)shouldShowLicense;
@end


@interface P2PTargetModeQRCodeScannerViewController () {
    UILabel *moreOptionLabel;
    UIButton *cloudFallbackButton;
}

@property (nonatomic, retain) MBProgressHUD *configHUD;

- (void)initCapture;
- (void)stopCapture;



@end

@implementation P2PTargetModeQRCodeScannerViewController

#if HAS_AVFF
@synthesize captureSession;
@synthesize prevLayer;
#endif
@synthesize result, delegate, soundToPlay;
@synthesize overlayView;
@synthesize readers;
@synthesize isDisplayedAsOption;

- (id)initWithDelegate:(id<ZXingDelegate>)scanDelegate  {
    if ((self = [super init])) {
        beepSound = -1;
        decoding = NO;
        
        [self setDelegate:scanDelegate];
    }
    
    return self;
}

#pragma mark View Lifecycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    OverlayView *theOverLayView = nil;
    
    CGFloat delta = 0.0;
    
    if ([[UIScreen mainScreen] bounds].size.height > 480.0f) { //iPhone 5
        delta = 0.0;
    } else {
        delta = -20.0;
    }

    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    self.title = P2P_SOURCE_QRCODE_TITLE;
    
    self.view.backgroundColor = [SDCustomUI customBackgroundColor1];
    
    float yOverlayView = self.navigationController.navigationBar.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height;

    CGRect aFrame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-yOverlayView + delta);

    theOverLayView = [[OverlayView alloc] initWithFrame:aFrame cancelEnabled:NO oneDMode:NO showLicense:NO];
    
    
    //theOverLayView = [[OverlayView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-yOverlayView + delta) oneDMode:NO];
    theOverLayView.displayedMessage = @" ";
    self.overlayView = theOverLayView;
    [theOverLayView release];
    
    //OverlayView has a default way to display a message but we can not set the text color (displayed with drawInRect)
    UILabel *headerLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width - 20.0, 105.0)] autorelease];
    headerLabel.backgroundColor = [SDCustomUI customBackgroundColor1];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont systemFontOfSize:16.0];
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2PMCTAmazon] == YES) {
        headerLabel.font = [UIFont systemFontOfSize:18];
    }
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2PMCTAmazon] == NO) {
        headerLabel.numberOfLines = 3.0;
    }else{
        headerLabel.numberOfLines = 4.0;
    }
    
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.text = P2P_TARGET_QRCODE_HEADER;
    [theOverLayView addSubview:headerLabel];
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionInfoButtonOnQRCode] == YES) {
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
        [infoButton addTarget:self action:@selector(infoButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
        if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2PMCTAmazon] == NO) {
            infoButton.frame = CGRectMake(250.0 + 20, 52.0 + 10, 22.0, 22.0);
        }else{
            infoButton.frame = CGRectMake(186 + 76 + 20, 59.0 + 20 + 12, 22.0, 22.0);
            [infoButton setImage:[UIImage imageNamed:@"tool_tip"] forState:UIControlStateNormal];
        }
        
        [theOverLayView addSubview:infoButton];
        [theOverLayView bringSubviewToFront:infoButton];
    }
    
    float yForLabel = self.overlayView.cropRect.origin.y + self.overlayView.cropRect.size.height;
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2PCloudFallbackEnabled] == NO)
    {
        moreOptionLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0, yForLabel, self.view.frame.size.width - 20.0, 45.0)] autorelease];
        moreOptionLabel.backgroundColor = [UIColor clearColor];
        moreOptionLabel.textColor = [UIColor blackColor];
        moreOptionLabel.font = [UIFont systemFontOfSize:12.0];
        moreOptionLabel.numberOfLines = 3.0;
        moreOptionLabel.textAlignment = NSTextAlignmentCenter;
        moreOptionLabel.text = P2P_QRCODE_FOOTER;
        [theOverLayView addSubview:moreOptionLabel];
    }
    else
    {
        moreOptionLabel.hidden = YES;
        
        cloudFallbackButton = [[[UIButton alloc] initWithFrame:CGRectMake(10.0, yForLabel, self.view.frame.size.width - 20.0, 40.0)] autorelease];
        [cloudFallbackButton setBackgroundColor:[UIColor clearColor]];
        cloudFallbackButton.titleLabel.textColor = [UIColor blackColor];
 //       cloudFallbackButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        cloudFallbackButton.titleLabel.numberOfLines = 1;
        cloudFallbackButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cloudFallbackButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [self updateCloudFallbackLabel:YES];
        [cloudFallbackButton addTarget:self action:@selector(cloudFallbackButtonWasTapped:) forControlEvents:UIControlEventTouchUpInside];
        [theOverLayView addSubview:cloudFallbackButton];
    }
    
    if ([self checkNumberOfOptions]) {
        SDSecondaryButton* optionButton = [[[SDSecondaryButton alloc] initWithFrame:CGRectMake(0.0,0.0,65.0,30.0)] autorelease];
        [optionButton setTitle:P2P_MORE_OPTION_BUTTON forState:UIControlStateNormal];
        [optionButton setEnabled:YES];
        [optionButton addTarget:self action:@selector(moreOptionsButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *optionBarButton = [[[UIBarButtonItem alloc] initWithCustomView:optionButton] autorelease];
        self.navigationItem.rightBarButtonItem = optionBarButton;
        
    }
    
    //Analytics
    [[SDAnalyticsWrapper analyticApi] customProperty:MCT_TARGET forKey:MCT_SET_ORIGIN];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationItem] setHidesBackButton:NO];
    
#if SYNCDRIVECORE==0
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveCloudSDKInitializationNotification:) name:SNCR_NOTIFICATION_CLOUD_SDK_INITIALIZED object:nil];
#endif
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self stopCapture];
    [[SDAnalyticsWrapper analyticApi] viewWillDisappear:MCT_PAGE_QRCODE_TARGET];
#if SYNCDRIVECORE==0
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SNCR_NOTIFICATION_CLOUD_SDK_INITIALIZED object:nil];
#endif
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];

    decoding = YES;
    
    [self initCapture];
    [self.view addSubview:overlayView];
    
    [overlayView setPoints:nil];
    wasCancelled = NO;
    
    [[SDAnalyticsWrapper analyticApi] viewDidAppear:MCT_PAGE_QRCODE_TARGET];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.overlayView removeFromSuperview];
    [self stopCapture];
}

-(void)updateCloudFallbackLabel:(BOOL)a_isNormalState
{
#if SYNCDRIVECORE==0
    UIColor* color;
    if(a_isNormalState)
    {
        color = [UIColor colorWithHexString:@"#0176c0"];
    }
    else
    {
        color = [UIColor colorWithHexString:@"#118ad6"];
    }
    
    UIFont* font = [UIFont systemFontOfSize:16.0];
    UIFont* boldFont = [UIFont boldSystemFontOfSize:16.0];
    NSString* firstPart = CLOUD_FALLBACK_QR_CODE_WITHOUT_WIFI_PART1;
    NSString* secondPart =  CLOUD_FALLBACK_QR_CODE_WITHOUT_WIFI_PART2;
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat: @"%@%@",firstPart,secondPart]];
    [str addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [firstPart length]-1)];
    [str addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange([firstPart length], [secondPart length])];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [firstPart length]-1)];
    [str addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange([firstPart length], [secondPart length])];
    [str addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange([firstPart length], [secondPart length])];
    if(a_isNormalState)
    {
        [cloudFallbackButton setAttributedTitle:str forState:UIControlStateNormal];
    }
    else
    {
        [cloudFallbackButton setAttributedTitle:str forState:UIControlStateHighlighted];
    }
    [str release];
#endif
}

#pragma mark User Interaction
-(void)infoButtonWasTapped:(id)sender {
    
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2PMCTAmazon] == NO) {
        
        [self.overlayView removeFromSuperview];
        [self stopCapture];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:P2P_CONTEXTUAL_HELP_TITLE
                                                                 message:P2P_TARGET_CONTEXTUAL_HELP_MSG
                                                                delegate:nil
                                                       cancelButtonTitle:GENERIC_BUTTON_OK
                                                       otherButtonTitles:nil, nil] autorelease];
            alertView.delegate = self;
            [alertView show];
        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            CustomIOS7AlertView *alertView = [[[CustomIOS7AlertView alloc] init] autorelease];
            UIView *leftJustifiedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 260, 20)];
            label.text = P2P_CONTEXTUAL_HELP_TITLE;
            label.font = [UIFont boldSystemFontOfSize:16];
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, 260, 360)];
            label2.text = P2P_TARGET_CONTEXTUAL_HELP_MSG;
            label2.numberOfLines = 20;
            [label2 setLineBreakMode:NSLineBreakByWordWrapping];
            [leftJustifiedView addSubview:label];
            [label release];
            [leftJustifiedView addSubview:label2];
            [label2 release];
            [alertView setContainerView:leftJustifiedView];
            [leftJustifiedView release];
            [alertView show];
        });
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([alertView tag] == kSDKConnectionFailureAlertViewTag) {
        [[self navigationController] popToRootViewControllerAnimated:YES];
    } else {
        decoding = YES;
        [self initCapture];
        [self.view addSubview:overlayView];
        
        [overlayView setPoints:nil];
        wasCancelled = NO;
    }
}

-(void)cloudFallbackButtonWasTapped:(id)sender
{
#if SYNCDRIVECORE==0
    AppDelegate *appDelegate = nil;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self stopCapture];
    
    if ([appDelegate cloudSDKInitialized]) {
        [self displayCloudFallbackSigninVC];
    } else {
        [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication] keyWindow] animated:NO];
        [self setConfigHUD:[MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES]];
        [[self configHUD] setDetailsLabelText:@"Please wait..."];
        
        [appDelegate initializeCloudSDK];
    }
#endif
}

#pragma mark otpions management
-(int)checkNumberOfOptions {
    int nbOptions = 0;
    
    if (self.isDisplayedAsOption == YES) {
        //the user tapped the option button to display this ViewController
        //so don't dislay again the option button
        return 0;
    }
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionScanningIsOption] == YES)
        nbOptions++;
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionManualPairingIsOption] == YES)
        nbOptions++;
    
    return nbOptions;
}

#pragma mark Button Touches

- (void)moreOptionsButtonTouched:(id)sender {
    [self stopCapture];
    [overlayView setPoints:nil];
    
    wasCancelled = YES;
    if (delegate != nil) {
        [delegate zxingControllerDidCancel:self withOption:@"MoreOption"];
    }
}

#pragma mark Rotation Methods (Don't allow landscape in the scanner)

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}



- (CGImageRef)CGImageRotated90:(CGImageRef)imgRef {
    CGFloat angleInRadians = -90 * (M_PI / 180);
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGRect imgRect = CGRectMake(0, 0, width, height);
    CGAffineTransform transform = CGAffineTransformMakeRotation(angleInRadians);
    CGRect rotatedRect = CGRectApplyAffineTransform(imgRect, transform);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bmContext = CGBitmapContextCreate(NULL,
                                                   rotatedRect.size.width,
                                                   rotatedRect.size.height,
                                                   8,
                                                   0,
                                                   colorSpace,
                                                   kCGImageAlphaPremultipliedFirst);
    CGContextSetAllowsAntialiasing(bmContext, FALSE);
    CGContextSetInterpolationQuality(bmContext, kCGInterpolationNone);
    CGColorSpaceRelease(colorSpace);
    //      CGContextTranslateCTM(bmContext,
    //                                                +(rotatedRect.size.width/2),
    //                                                +(rotatedRect.size.height/2));
    CGContextScaleCTM(bmContext, rotatedRect.size.width/rotatedRect.size.height, 1.0);
    CGContextTranslateCTM(bmContext, 0.0, rotatedRect.size.height);
    CGContextRotateCTM(bmContext, angleInRadians);
    //      CGContextTranslateCTM(bmContext,
    //                                                -(rotatedRect.size.width/2),
    //                                                -(rotatedRect.size.height/2));
    CGContextDrawImage(bmContext, CGRectMake(0, 0,
                                             rotatedRect.size.width,
                                             rotatedRect.size.height),
                       imgRef);
    
    CGImageRef rotatedImage = CGBitmapContextCreateImage(bmContext);
    CFRelease(bmContext);
    [(id)rotatedImage autorelease];
    
    return rotatedImage;
}

- (CGImageRef)CGImageRotated180:(CGImageRef)imgRef {
    CGFloat angleInRadians = M_PI;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bmContext = CGBitmapContextCreate(NULL,
                                                   width,
                                                   height,
                                                   8,
                                                   0,
                                                   colorSpace,
                                                   kCGImageAlphaPremultipliedFirst);
    CGContextSetAllowsAntialiasing(bmContext, FALSE);
    CGContextSetInterpolationQuality(bmContext, kCGInterpolationNone);
    CGColorSpaceRelease(colorSpace);
    CGContextTranslateCTM(bmContext,
                          +(width/2),
                          +(height/2));
    CGContextRotateCTM(bmContext, angleInRadians);
    CGContextTranslateCTM(bmContext,
                          -(width/2),
                          -(height/2));
    CGContextDrawImage(bmContext, CGRectMake(0, 0, width, height), imgRef);
    
    CGImageRef rotatedImage = CGBitmapContextCreateImage(bmContext);
    CFRelease(bmContext);
    [(id)rotatedImage autorelease];
    
    return rotatedImage;
}

#pragma mark Decoder Methods

- (void)decoder:(Decoder *)decoder willDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset{
}

- (void)decoder:(Decoder *)decoder decodingImage:(UIImage *)image usingSubset:(UIImage *)subset {
}

- (void)presentResultForString:(NSString *)resultString {
    self.result = [ResultParser parsedResultForString:resultString];
}

- (void)presentResultPoints:(NSArray *)resultPoints forImage:(UIImage *)image usingSubset:(UIImage *)subset {
    // simply add the points to the image view
    NSMutableArray *mutableArray = [[NSMutableArray alloc] initWithArray:resultPoints];
    [overlayView setPoints:mutableArray];
    [mutableArray release];
}

- (void)decoder:(Decoder *)decoder didDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset withResult:(TwoDDecoderResult *)twoDResult {
    
if ([MCTPairingTools isPairingQRCode:[twoDResult text]])  {
        [self presentResultForString:[twoDResult text]];
        [self presentResultPoints:[twoDResult points] forImage:image usingSubset:subset];
        
        // now, in a selector, call the delegate to give this overlay time to show the points
        [self stopCapture];
        
        [self performSelector:@selector(notifyDelegate:) withObject:[[twoDResult text] copy] afterDelay:0.0];
        //decoder.delegate = nil;
    }else{
        [self stopCapture];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:P2P_INVALID_BARCODE_TITLE
                                                                       message:P2P_INVALID_BARCODE_MESSAGE
                                                                      delegate:self
                                                             cancelButtonTitle:GENERIC_BUTTON_OK
                                                             otherButtonTitles:nil,nil];
            [alertView show];
            });
    }
    
}


- (void)notifyDelegate:(id)text {
    [overlayView setPoints:nil];
    [delegate zxingController:self didScanResult:text];
    [text release];
}

- (void)decoder:(Decoder *)decoder failedToDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset reason:(NSString *)reason {
    decoder.delegate = nil;
    [overlayView setPoints:nil];
}

- (void)decoder:(Decoder *)decoder foundPossibleResultPoint:(CGPoint)point {
    [overlayView setPoint:point];
}


#pragma mark -
#pragma mark AVFoundation

#include <sys/types.h>
#include <sys/sysctl.h>

- (void)initCapture {
#if HAS_AVFF
    AVCaptureDevice* inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *captureInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:nil];
    
    if (!captureInput) {
        return;
    }
    
    AVCaptureVideoDataOutput *captureOutput = [[AVCaptureVideoDataOutput alloc] init];
    captureOutput.alwaysDiscardsLateVideoFrames = YES;
    
    if ([P2PUtils SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO:@"8.0"]) {
        [captureOutput setSampleBufferDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    } else {
        [captureOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    }

    NSString* key = (NSString*)kCVPixelBufferPixelFormatTypeKey;
    NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
    NSDictionary* videoSettings = [NSDictionary dictionaryWithObject:value forKey:key];
    [captureOutput setVideoSettings:videoSettings];
    self.captureSession = [[[AVCaptureSession alloc] init] autorelease];
    
    NSString* preset = 0;
    
    if (!preset) {
        preset = AVCaptureSessionPresetMedium;
    }
    self.captureSession.sessionPreset = preset;
    
    [self.captureSession addInput:captureInput];
    [self.captureSession addOutput:captureOutput];
    
    [captureOutput release];
    
    if (!self.prevLayer) {
        self.prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
    }
    // DLog(@"prev %p %@", self.prevLayer, self.prevLayer);
    //self.prevLayer.frame = self.view.bounds;
    CGRect myFrame = self.overlayView.cropRect;
    myFrame.size.height -= 10.0;
    myFrame.size.width -= 10.0;
    myFrame.origin.x -= 5.0;
    myFrame.origin.y -= 5.0;
    self.prevLayer.frame = CGRectMake(self.overlayView.cropRect.origin.x, self.overlayView.cropRect.origin.y, self.overlayView.cropRect.size.width, self.overlayView.cropRect.size.height);
    self.prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer: self.prevLayer];

    if ([P2PUtils SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO:@"8.0"]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self.captureSession startRunning];
        });
    } else {
        [self.captureSession startRunning];
    }
    
#endif
}

#if HAS_AVFF
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    if (!decoding) {
        return;
    }
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    /*Lock the image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    /*Get information about the image*/
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // DLog(@"wxh: %lu x %lu", width, height);
    
    uint8_t* baseAddress = (uint8_t *) CVPixelBufferGetBaseAddress(imageBuffer);
    void* free_me = 0;
    if (true) { // iOS bug?
        uint8_t* tmp = baseAddress;
        unsigned long bytes = bytesPerRow*height;
        free_me = baseAddress = (uint8_t*)malloc(bytes);
        baseAddress[0] = 0xdb;
        memcpy(baseAddress,tmp,bytes);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef newContext =
    CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace,
                          kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst);
    
    CGImageRef capture = CGBitmapContextCreateImage(newContext);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    free(free_me);
    
    CGContextRelease(newContext);
    CGColorSpaceRelease(colorSpace);
    
    if (false) {
        CGRect cropRect = [overlayView cropRect];
        if (oneDMode) {
            // let's just give the decoder a vertical band right above the red line
            cropRect.origin.x = cropRect.origin.x + (cropRect.size.width / 2) - (ONE_D_BAND_HEIGHT + 1);
            cropRect.size.width = ONE_D_BAND_HEIGHT;
            // do a rotate
            CGImageRef croppedImg = CGImageCreateWithImageInRect(capture, cropRect);
            CGImageRelease(capture);
            capture = [self CGImageRotated90:croppedImg];
            capture = [self CGImageRotated180:capture];
            //              UIImageWriteToSavedPhotosAlbum([UIImage imageWithCGImage:capture], nil, nil, nil);
            CGImageRelease(croppedImg);
            CGImageRetain(capture);
            cropRect.origin.x = 0.0;
            cropRect.origin.y = 0.0;
            cropRect.size.width = CGImageGetWidth(capture);
            cropRect.size.height = CGImageGetHeight(capture);
        }
        
        // N.B.
        // - Won't work if the overlay becomes uncentered ...
        // - iOS always takes videos in landscape
        // - images are always 4x3; device is not
        // - iOS uses virtual pixels for non-image stuff
        
        {
            float height = CGImageGetHeight(capture);
            float width = CGImageGetWidth(capture);
            
            DLog(@"%f %f", width, height);
            
            CGRect screen = UIScreen.mainScreen.bounds;
            float tmp = screen.size.width;
            screen.size.width = screen.size.height;;
            screen.size.height = tmp;
            
            cropRect.origin.x = (width-cropRect.size.width)/2;
            cropRect.origin.y = (height-cropRect.size.height)/2;
        }
        
        DLog(@"sb %@", NSStringFromCGRect(UIScreen.mainScreen.bounds));
        DLog(@"cr %@", NSStringFromCGRect(cropRect));
        
        CGImageRef newImage = CGImageCreateWithImageInRect(capture, cropRect);
        CGImageRelease(capture);
        capture = newImage;
    }
    
    UIImage* scrn = [[[UIImage alloc] initWithCGImage:capture] autorelease];
    
    CGImageRelease(capture);
    
    Decoder* d = [[Decoder alloc] init];
    d.readers = readers;
    d.delegate = self;
    
    decoding = [d decodeImage:scrn] == YES ? NO : YES;
    
    [d release];
    
    if (decoding) {
        
        d = [[Decoder alloc] init];
        d.readers = readers;
        d.delegate = self;
        
        scrn = [[[UIImage alloc] initWithCGImage:scrn.CGImage
                                           scale:1.0
                                     orientation:UIImageOrientationLeft] autorelease];
        
        // DLog(@"^ %@ %f", NSStringFromCGSize([scrn size]), scrn.scale);
        decoding = [d decodeImage:scrn] == YES ? NO : YES;
        
        [d release];
    }
    
}
#endif

- (void)stopCapture {
    decoding = NO;
#if HAS_AVFF
    [captureSession stopRunning];
    AVCaptureInput* input = [captureSession.inputs objectAtIndex:0];
    [captureSession removeInput:input];
    AVCaptureVideoDataOutput* output = (AVCaptureVideoDataOutput*)[captureSession.outputs objectAtIndex:0];
    [captureSession removeOutput:output];
    [self.prevLayer removeFromSuperlayer];
    
    self.prevLayer = nil;
    self.captureSession = nil;
#endif
}


- (NSString *)getPlatform {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = (char *)malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    return platform;
}

- (BOOL)fixedFocus {
    NSString *platform = [self getPlatform];
    if ([platform isEqualToString:@"iPhone1,1"] ||
        [platform isEqualToString:@"iPhone1,2"]) return YES;
    return NO;
}


- (void)didReceiveCloudSDKInitializationNotification:(NSNotification *)notification {

#if SYNCDRIVECORE==0
    NSDictionary *userInfo = nil;
    NSError      *error = nil;
    UIAlertView  *alertView = nil;
    
    userInfo = [notification userInfo];
    error = [userInfo objectForKey:@"error"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SNCR_NOTIFICATION_CLOUD_SDK_INITIALIZED object:nil];
    
    [[self configHUD] hide:YES];
    [self setConfigHUD:nil];
    
    if (error == nil) {
        [self displayCloudFallbackSigninVC];
    } else {
        alertView = [[UIAlertView alloc] initWithTitle:GENERIC_ALERT_TITLE_ERROR
                                               message:GENERIC_LABEL_CONNECTION_FAILURE
                                              delegate:self
                                     cancelButtonTitle:GENERIC_BUTTON_OK
                                     otherButtonTitles:nil, nil];
        [alertView setTag:kSDKConnectionFailureAlertViewTag];
        [alertView show];
        [alertView release];
    }
#endif
}

- (void)displayCloudFallbackSigninVC {
#if SYNCDRIVECORE==0
    MCTCloudFallbackSignInEmailViewController *enterEmailVC = nil;
    
    [[SDAnalyticsWrapper analyticApi] logNetworkInfoWithLinkType:@"cloud"];
    
    enterEmailVC = [[UIStoryboard storyboardWithName:[SDDeviceUtils isIPad] ? @"MCTCommoniPad" : @"MCTCommoniPhone" bundle:[P2PUtils uiBundle]] instantiateViewControllerWithIdentifier:@"MCTCloudFallbackSignInEmailViewControllerID"];
    enterEmailVC.isModeSource = NO;
    [self.navigationController pushViewController:enterEmailVC animated:YES];
#endif
}


- (void)dealloc {
    if (beepSound != (SystemSoundID)-1) {
        AudioServicesDisposeSystemSoundID(beepSound);
    }
    
    [self stopCapture];
    [result release];
    [soundToPlay release];
    [readers release];
    [super dealloc];
}


@end
