//
//  EventsViewController.h
//  Mansion
//
//  Created by Falguni Shah on 3/5/15.
//
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
@interface EventsViewController : GAITrackedViewController
{
    WebViewController *webViewCtrl;
}

- (IBAction)openURL:(id)sender;
@end
