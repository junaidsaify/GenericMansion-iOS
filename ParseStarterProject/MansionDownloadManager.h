//
//  MansionDownloadManager.h
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//

#import <Foundation/Foundation.h>
#import "SSZipArchive.h"
@protocol MansionDownloadManagerDelegate <NSObject>

@optional

-(void)downloadManagerDidRecieveResponseWithContentLength:(long)contentLength;
-(void)downloadManagerDidRecieveResponseWithPercent:(float)percent;
-(void)downloadManagerDidFinishLoading;
-(void)downloadManagerDidFailWithError:(NSError*)error;


@end

@interface MansionDownloadManager : NSObject<NSURLConnectionDelegate,NSURLConnectionDataDelegate,NSURLConnectionDownloadDelegate>
{
    NSURLConnection *connection;
    NSMutableURLRequest *request;
    long contentLength;
    NSInteger bytesSum;
    float percent;
}

@property(nonatomic,retain)NSMutableData *mdata;
@property(nonatomic,retain)NSString *mansionName;
@property(nonatomic)BOOL shouldJump;
@property(nonatomic,assign)id <MansionDownloadManagerDelegate> delegate;

-(void)stopTheDownload;
-(void)downloadFileForAddress:(NSString*)urlString forMansion:(NSString*)mansionName;
@end
