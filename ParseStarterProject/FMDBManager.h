//
//  FMDBManager.h
//  GenericMansion
//
//  Created by Junaid Saify on 07/11/15.
//
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "SideMenu.h"
#import "TopMenu.h"
#import "BottomMenu.h"
#import "MansionModel.h"
#import "CustomerDetails.h"
#import "FeedbackMailDetails.h"
#import "SegmentViewDetails.h"
@interface FMDBManager : NSObject

@property(nonatomic,retain)NSMutableArray *dataArray;
@property(nonatomic,retain) FMDatabase *database;

-(NSMutableArray*)returnValueForQuery:(NSString*)queryString forTable:(NSString*)tableName;
@end
