//
//  StoresTableViewCell.m
//  Mansion
//
//  Created by Falguni Shah on 1/4/15.
//
//

#import "StoresTableViewCell.h"

@implementation StoresTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)addressButtonClicked:(UIButton*)sender{
    [self.delegate didClickOnAddressButtonForCell:self withButton:sender];
}
-(IBAction)phoneNumberButtonClicked:(UIButton*)sender{
    [self.delegate didClickOnPhoneNumberButtonForCell:self withButton:sender];
}


@end
