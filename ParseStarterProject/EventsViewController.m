//
//  EventsViewController.m
//  Mansion
//
//  Created by Falguni Shah on 3/5/15.
//
//

#import "EventsViewController.h"

@interface EventsViewController ()

@end

@implementation EventsViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName=@"EventsScreen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)removeTheView:(id)sender{
    [self.view removeFromSuperview];
}

- (IBAction)openURL:(id)sender
{
    NSString *urlString=[[NSURL URLWithString:@"http://mobile.newportmansions.org/events"] absoluteString];
    
    if(isIphone){
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    }
    else{
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    }

    webViewCtrl.urlString=urlString;
    //.. [self presentViewController:webViewCtrl animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    NSLog(@"URL Hit");
    NSString *urlString=[[NSURL URLWithString:@"http://mobile.newportmansions.org/events"] absoluteString];
  //..  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    if(isIphone){
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    }
    else{
       webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    }
    
     webViewCtrl.urlString=urlString;
     //.. [self presentViewController:webViewCtrl animated:YES completion:nil];
     [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil];
    return NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
