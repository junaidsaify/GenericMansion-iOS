//
//  ManageDownloadTableViewCell.m
//  GenericMansion
//
//  Created by Junaid Saify on 22/11/15.
//
//

#import "ManageDownloadTableViewCell.h"

@implementation ManageDownloadTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)downloadTourClicked:(UIButton*)sender{
    [self.delegate didClickOnDownloadButtonForCell:self withDownloadButton:self.downloadNow];
}
-(IBAction)deleteTourClicked:(UIButton*)sender{
    [self.delegate didClickOnDeleteButtonForCell:self withDeleteButton:self.deleteNow];
}



@end
