//
//  Mansion.h
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//

#import <Foundation/Foundation.h>

@interface Mansion : NSObject

@property(nonatomic,retain) NSString *mansionName;
@property(nonatomic,retain) NSString *aboutMansion;
@property(nonatomic,retain) UIImage *nameImage; // the Breakers,Elms images etc.
@property(nonatomic,retain) NSMutableArray *mansionImagesArray,*floorsArray,*dataArray;
@property(nonatomic,retain) NSMutableDictionary *floorDict;
@property(nonatomic)BOOL isGPSPackage;
-(Mansion*)fetchDataForMansionName:(NSString*)mansion;
@end
