//
//  Constant.h
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//

#ifndef Mansion_Constant_h
#define Mansion_Constant_h


#endif
#define APPDELEGATE   (AppDelegate*) [[UIApplication sharedApplication] delegate]
# define DocumentDirectory  [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
//..[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]
#define ELMSAddress @"367 Bellevue Avenue Newport, RI 02840"
#define TheBreakersAddress @"44 Ochre Point Avenue Newport, RI 02840"
#define MarbleHouseAddress @"596 Bellevue Avenue Newport, RI 02840"
#define RosecliffAddress @"548 Bellevue Avenue Newport, RI 02840"
#define DonateLink @"http://www.thankyouforsupport.org/nwpmansions/donate.html"
//"http://newportmansions.thankyou4caring.org/pages/af-donation-app"

//"http://mobile.newportmansions.org/join-and-support"
#define Subject @"Having a great time at Newport Mansions!"
#define SubjectFeedback @"Feedback on Newport Mansions App"
#define GreatTimeMessage @"Having a great time at "
#define AppFeedbackMessage @"Thank you for downloading the Newport Mansions app. Please share your thoughts on your Newport Mansions app experience. \n\n Feedback Questions:\n1. How was your overall experience of the mansion using the app?\n\n\n 2. What features did you use: Tour, Floor Map, Photo-Share, Directions, Donation etc. ? \n\n\n 3. What other features would you like to add to the app? \n\n\n 4. On a scale of 0 to 10, where 10 being Definitely Yes and 0 being Definitely No, would you recommend this app to your friend? \n\n\n More Feedback/Comments"
#define RecipientFeedback @"appfeedback@NewportMansions.org"
#define ActionShow @"http://actionshowapp.com"
#define NewportURL @"http://www.NewportMansions.org"
#define isIphone   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//..[[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone

#import "GAI.h"
#import "GAIFields.h"

#define ActionChartsGA @"UA-64574304-1"
#define ActionDataSystemsGA @"UA-64581918-1"
#define NWPMansionsGA @""

