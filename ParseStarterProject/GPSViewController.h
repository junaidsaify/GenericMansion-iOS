//
//  GPSViewController.h
//  GenericMansion
//
//  Created by Junaid Saify on 23/04/16.
//
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol MapProtocol <NSObject>

-(void)didEnterRegionWithName:(NSString*)name;
-(void)willShowGPSMap;

@end



@interface GPSViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate,MKOverlay,MKAnnotation,GMSMapViewDelegate>
{
    BOOL firstLocationUpdate_;
    CLRegion* currentMonitoredRegion;
   //.. NSMutableArray *gpsArray;
    NSString *currentRegion;
    NSMutableArray *regionArrayOfDict;
    BOOL isUserControlled;
}


@property(nonatomic,assign)id<MapProtocol>delegate;

@property(nonatomic,retain)IBOutlet GMSMapView *mapView;
@property(nonatomic,retain)CLLocationManager *locationManager;
@property(nonatomic,retain)NSArray *gpsDataArray;
@property(nonatomic,retain)NSString *mansionPath;
-(IBAction)removeView:(id)sender;

-(void)reCentreMapToDefault;

@end
