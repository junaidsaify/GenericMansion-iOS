//
//  ParseStarterProjectAppDelegate.m
//  ParseStarterProject
//
//  Copyright 2014 Parse, Inc. All rights reserved.
//

//#import <Parse/Parse.h>
//#import <ParseFacebookUtils/PFFacebookUtils.h>

#import "ParseStarterProjectAppDelegate.h"
#import "ParseStarterProjectViewController.h"

@implementation ParseStarterProjectAppDelegate
#pragma mark - UIApplicationDelegate

#define MetadataPath @"http://actiontour.blob.core.windows.net/genericgps/Metadata.sqlite"


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // ****************************************************************************
    // Uncomment and fill in with your Parse credentials:
    //.. [Parse setApplicationId:@"YhHKzTzw5flM6SmUVfiq2OkMkqjEZoRkhk211HTr" clientKey:@"5bqPGN66qGjBjhg3Hot8yX9iIcuzgEpLcNubHVr9"];
    // facebookID:- 661576930606539
    // If you are using Facebook, uncomment and add your FacebookAppID to your bundle's plist as
    // described here: https://developers.facebook.com/docs/getting-started/facebook-sdk-for-ios/
  //..   [PFFacebookUtils initializeFacebook];
    // ****************************************************************************

   //.. [PFUser enableAutomaticUser];

   //.. PFACL *defaultACL = [PFACL ACL];

    // If you would like all objects to be private by default, remove this line.
   //.. [defaultACL setPublicReadAccess:YES];

    //.. [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];

    // Override point for customization after application launch.
 //..   [NSThread sleepForTimeInterval:3];
    
 //   [FBLoginView class];
 //  [self copyMansionFromProjectToDirectory];
    // Marble House address wrong : 596 Bellevue Avenue Newport , Rhode Island 02840
 //..
    [GMSServices provideAPIKey:@"AIzaSyAa2Lc4Rjm5raLrkt1KFfLnyHWLwXxD5wc"];
    alert = [[UIAlertView alloc]initWithTitle:nil message:@"Preparing Your App" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [alert show];
    
    [self copyNonTourData];
    // Google Analytics
    NSLocale *locale = [NSLocale currentLocale];
    
    NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                             value:[locale localeIdentifier]];
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    
    
    [GAI sharedInstance].dispatchInterval = 20;
    NSLog(ActionChartsGA);
    
   self. tracker =  [[GAI sharedInstance] trackerWithName:@"NWPMansions"
                                                         trackingId:NWPMansionsGA];
 // [[GAI sharedInstance] trackerWithTrackingId:ActionChartsGA];
    
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    [ self. tracker set:kGAIAppVersion value:version];
    [ self. tracker set:kGAISampleRate value:@"50.0"];
     
    
    
    self.locationManager=[[CLLocationManager alloc]init];
    
    if([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    }
    if(isIphone){
        CGRect screenRect = [[UIScreen mainScreen] bounds];
         self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
        self.viewController.view.frame = screenRect;
    }
    else{
    self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController-iPad" bundle:nil];
    }
 //..   [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController-iPad" bundle:nil]
  //..  UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:self.viewController];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];

  /*  if (application.applicationState != UIApplicationStateBackground) {
        // Track an app open here if we launch with a push, unless
        // "content_available" was used to trigger a background push (introduced in iOS 7).
        // In that case, we skip tracking here to avoid double counting the app-open.
        BOOL preBackgroundPush = ![application respondsToSelector:@selector(backgroundRefreshStatus)];
        BOOL oldPushHandlerOnly = ![self respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)];
        BOOL noPushPayload = ![launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (preBackgroundPush || oldPushHandlerOnly || noPushPayload) {
         //..   [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        }
    } */
/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else
#endif
    {
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
    } */
    
    [[AVAudioSession sharedInstance] setDelegate:self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];


    return YES;
}

/*

///////////////////////////////////////////////////////////
// Uncomment this method if you are using Facebook
///////////////////////////////////////////////////////////

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [PFFacebookUtils handleOpenURL:url];
}

 */
/*
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    //..[PFPush storeDeviceToken:newDeviceToken];
    //..[PFPush subscribeToChannelInBackground:@"" target:self selector:@selector(subscribeFinished:error:)];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
   //.. [PFPush handlePush:userInfo];

    if (application.applicationState == UIApplicationStateInactive) {
       //.. [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

*/
///////////////////////////////////////////////////////////
// Uncomment this method if you want to use Push Notifications with Background App Refresh
///////////////////////////////////////////////////////////
/*
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}
 */

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state.
     This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message)
     or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates.
     Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state
     information to restore your application to its current state in case it is terminated later.
     If your application supports background execution,
     this method is called instead of applicationWillTerminate: when the user quits.
     */
    
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
 
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

-(void)copyNonTourData{
/* NSString *bundlePath=[[NSBundle mainBundle] pathForResource:@"Metadata" ofType:@"sqlite"];
    //.. NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Metadata.sqlite"];
    NSString *toPath=[DocumentDirectory stringByAppendingPathComponent:@"Metadata.sqlite"];
    NSFileManager *manager=[NSFileManager defaultManager];
    NSError *error=nil;
  if(![manager fileExistsAtPath:toPath]){
        [manager copyItemAtPath:bundlePath toPath:toPath error:&error];
        NSLog(@"error:%@",error);
      
     
   }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(isIphone){
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
            self.viewController.view.frame = screenRect;
        }
        else{
            self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController-iPad" bundle:nil];
        }
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    }); */
    
    
 
     NSFileManager *fManager = [NSFileManager defaultManager];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:MetadataPath]] queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", DocumentDirectory,@"Metadata.sqlite"];
        if(data){
            NSLog(@"Mil gaya file");
            if([fManager fileExistsAtPath:filePath]){
                [fManager removeItemAtPath:filePath error:nil];
            }
            [data writeToFile:filePath atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(isIphone){
                    CGRect screenRect = [[UIScreen mainScreen] bounds];
                    self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
                    self.viewController.view.frame = screenRect;
                }
                else{
                    self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController-iPad" bundle:nil];
                }
                self.window.rootViewController = self.viewController;
                [self.window makeKeyAndVisible];
                [alert dismissWithClickedButtonIndex:0 animated:YES];
            });
        }
        
        else{
            
           
            if  ( [fManager fileExistsAtPath:filePath isDirectory:NO]){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(isIphone){
                        CGRect screenRect = [[UIScreen mainScreen] bounds];
                        self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
                        self.viewController.view.frame = screenRect;
                    }
                    else{
                        self.viewController=[[MasterViewController alloc] initWithNibName:@"MasterViewController-iPad" bundle:nil];
                    }
                    self.window.rootViewController = self.viewController;
                    [self.window makeKeyAndVisible];
                    [alert dismissWithClickedButtonIndex:0 animated:YES];
                });
                
            }
            
            else{
                [alert dismissWithClickedButtonIndex:0 animated:YES];
                alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please connect to internet to prepare the app for the first time" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        
        
        
    }];

}

-(void)copyMansionFromProjectToDirectory{
    NSString *bundlePath=[[NSBundle mainBundle] pathForResource:@"HTN" ofType:nil];
    NSString *toPath=[DocumentDirectory stringByAppendingPathComponent:@"HTN"];
    NSFileManager *manager=[NSFileManager defaultManager];
    NSError *error=nil;
    if(![manager fileExistsAtPath:toPath]){
        [manager copyItemAtPath:bundlePath toPath:toPath error:&error];
        NSLog(@"error:%@",error);
    }
    
  
}

#pragma mark - ()

- (void)subscribeFinished:(NSNumber *)result error:(NSError *)error {
    if ([result boolValue]) {
        NSLog(@"ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
    } else {
        NSLog(@"ParseStarterProject failed to subscribe to push notifications on the broadcast channel.");
    }
}


#pragma Facebook Overrides

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    //BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // You can add your app-specific url handling code here if needed
    
    //return wasHandled;
    
    return YES;
}
@end
