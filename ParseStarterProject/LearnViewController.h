//
//  LearnViewController.h
//  Mansion
//
//  Created by Falguni Shah on 12/19/14.
//
//

#import <UIKit/UIKit.h>

@interface LearnViewController : UIViewController
{
    NSArray *mansionLearnArray;
}

@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;
@property(nonatomic,retain)IBOutlet UIView *view1,*view2,*view3;
@property(nonatomic,retain)IBOutlet UILabel *headerLabel;
@end
