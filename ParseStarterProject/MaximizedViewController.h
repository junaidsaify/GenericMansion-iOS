//
//  MaximizedViewController.h
//  Mansion
//
//  Created by Junaid Saify on 06/07/15.
//
//

#import <UIKit/UIKit.h>

@interface MaximizedViewController : UIViewController<UIScrollViewDelegate>

@property(nonatomic,retain) IBOutlet UIButton *maximizedDoneButton;
@property(nonatomic,retain) IBOutlet UIScrollView *maximizedImageScrollView;
@property(nonatomic,retain) IBOutlet UIImageView *maximizedImageView;
@property(nonatomic,retain) IBOutlet UIView *maximizedView;
@property(nonatomic,retain) UIImage *imageToMaximize;
@end
