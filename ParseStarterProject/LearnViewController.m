//
//  LearnViewController.m
//  Mansion
//
//  Created by Falguni Shah on 12/19/14.
//
//

#import "LearnViewController.h"

@interface LearnViewController ()

@end

@implementation LearnViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scrollView.pagingEnabled=YES;
    self.scrollView.autoresizingMask=UIViewAutoresizingNone;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self setupScrollView:self.scrollView];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MansionProperty" ofType: @"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile: path];
    mansionLearnArray = [dict objectForKey: @"Learn"];
    
    self.headerLabel.text=[mansionLearnArray objectAtIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupScrollView:(UIScrollView*)scrMain {
    
    
    
    
    for(int i=0;i<3;i++){
       
        if(i==0)
        self.view1.frame=CGRectMake((i)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height);
        else if(i==1)
          self.view2.frame=CGRectMake((i)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height);
        else if (i==2)
            self.view3.frame=CGRectMake((i)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height);
        
        
    }
    
    [self.scrollView addSubview:self.view1];
    [self.scrollView addSubview:self.view2];
    [self.scrollView addSubview:self.view3];
    

    [scrMain setContentSize:CGSizeMake(self.scrollView.frame.size.width*3, self.scrollView.frame.size.height)];
}

-(IBAction)removeTheView:(id)sender{
    [self.view removeFromSuperview];
}

#pragma ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat contentOffset = scrollView.contentOffset.x;
    int nextPage = (NSInteger)(contentOffset/scrollView.frame.size.width);
    
    self.headerLabel.text=[mansionLearnArray objectAtIndex:nextPage];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
