//
//  ExploreViewController.m
//  Mansion
//
//  Created by Falguni Shah on 3/15/15.
//
//

#import "ExploreViewController.h"

@interface ExploreViewController ()

@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName=@"ExploreScreen";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    NSLog(@"URL Hit");
    NSString *urlString=[[NSURL URLWithString:@"http://mobile.newportmansions.org/explore"] absoluteString];
    if(isIphone){
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    }
    else{
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    }

    webViewCtrl.urlString=urlString;
   //.. [self presentViewController:webViewCtrl animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil];
    return NO;
}

@end
