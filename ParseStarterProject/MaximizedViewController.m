//
//  MaximizedViewController.m
//  Mansion
//
//  Created by Junaid Saify on 06/07/15.
//
//

#import "MaximizedViewController.h"

@interface MaximizedViewController ()

@end

@implementation MaximizedViewController
@synthesize imageToMaximize;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    self.maximizedDoneButton.layer.borderWidth=2.0f;
    self.maximizedDoneButton.layer.cornerRadius=5.0;
    self.maximizedDoneButton.layer.borderColor=[UIColor whiteColor].CGColor;
    
    UITapGestureRecognizer *taptoRemove=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resetZoomScale:)];
    taptoRemove.numberOfTapsRequired=2;
    [self.maximizedView addGestureRecognizer:taptoRemove];
    
    self.maximizedImageView.image = self.imageToMaximize;
}

- (void)orientationChanged:(NSNotification *)notification{
    //..   NSLog(@"Change hua orientation %d", [[UIApplication sharedApplication] statusBarOrientation]);
    //..[[UIApplication sharedApplication] statusBarOrientation]
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(UIInterfaceOrientationIsPortrait(orientation)){
        
        NSLog(@"potrait hai");
        //   self.maximizedView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        //   NSLog(@"width is %@", NSStringFromCGPoint(CGRectGetWidth(self.view.frame)));
    }
    else{
        NSLog(@"landscape hai");
        //      self.maximizedView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        //    self.maximizedImageView.frame = self.maximizedView.frame;
        
        if (isIphone) {
          //  CGSize imageViewSize= CGSizeMake(CGRectGetWidth(self.maximizedView.frame), CGRectGetHeight(self.maximizedView.frame));
           // self.maximizedImageView.frame = CGRectMake(0, 0, imageViewSize.width, imageViewSize.height);
        }
        
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}




- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    if(scrollView.tag==50){
        if(scrollView.zoomScale<=1){
            
          //  self.maximizedDoneButton.hidden=NO;
        }
        else{
         //   self.maximizedDoneButton.hidden=YES;
            
        }
    }
    
    [self centerScrollViewContents];
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView.subviews objectAtIndex:0];
    //.. return self.maximizedImageView;
}



- (void)centerScrollViewContents {
    CGSize boundsSize = self.maximizedImageScrollView.bounds.size;
    CGRect contentsFrame = self.maximizedImageView.frame;
        if (contentsFrame.size.width < boundsSize.width) {
          contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
         }
    else {
                    contentsFrame.origin.x = 0.0f;
                }
    
        
        if (contentsFrame.size.height < boundsSize.height) {
                contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
            } else {
                    contentsFrame.origin.y = 0.0f;
                }
        
        self.maximizedImageView.frame = contentsFrame;
}


-(void)resetZoomScale:(UIGestureRecognizer*)gesture{
    if(self.maximizedImageScrollView.zoomScale>1 ){
        [self.maximizedImageScrollView setZoomScale:1.0];
    }
    else{
        [self.maximizedImageScrollView setZoomScale:4.0];
    }
}


-(IBAction)removeTheView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
