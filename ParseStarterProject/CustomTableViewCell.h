//
//  CustomTableViewCell.h
//  Mansion
//
//  Created by Falguni Shah on 10/18/14.
//
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell{
    
   //.. UIImageView *imageView;
   // UILabel *textLable;
    
}
@property (nonatomic,retain)  UIImageView *cellImageView;
@property (nonatomic,retain)   UILabel *cellTextLable;
@property(nonatomic)BOOL isExpanded;
@end
