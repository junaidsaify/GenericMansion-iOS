//
//  MansionAudioPlayer.m
//  Mansion
//
//  Created by Falguni Shah on 11/22/14.
//
//

#import "MansionAudioPlayer.h"

@implementation MansionAudioPlayer
@synthesize audioPath;



- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"eng_417"
                                         ofType:@"mp3"]];
    
    
    [self createAudioPlayerWithPath:self.audioPath];
    
}

-(void)createAudioPlayerWithPath:(NSString*)path{
    if(self.player==nil){
        
        path=[[NSBundle mainBundle] pathForResource:@"eng_417" ofType:@"mp3"];
        NSURL *fileURL =[[NSURL alloc] initFileURLWithPath: path];
        NSError *error = nil;
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
        self.player.delegate=self;
        self.player.numberOfLoops=0;
        [self.player prepareToPlay];
        [self.player play];
    }
    else{
        [self togglePlayer:self.player];
    }
    
}


@end
