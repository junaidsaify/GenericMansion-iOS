//
//  ManageMyDownloadViewController.h
//  GenericMansion
//
//  Created by Junaid Saify on 22/11/15.
//
//

#import <UIKit/UIKit.h>
#import "ManageDownloadTableViewCell.h"
#import "MansionModel.h"
#import "MansionDownloadManager.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
@interface ManageMyDownloadViewController : UIViewController
{
    NSArray *mansionArray;
    NSMutableArray *downloadedMansionArray;
    MansionDownloadManager *downloadManager;
    ManageDownloadTableViewCell *currentCell;
    BOOL isDownloadInProgress;
}

@property(nonatomic,retain)IBOutlet UITableView *mansionList;
@property(nonatomic,retain) NSArray *mansionArray;
@property(nonatomic,retain) IBOutlet UIView *tableBaseView;
@property(nonatomic,retain) NSString *deleteButtonURL;
-(IBAction)removeView:(UIButton*)sender;
@end
