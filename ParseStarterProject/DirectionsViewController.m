//
//  DirectionsViewController.m
//  Mansion
//
//  Created by Falguni Shah on 12/21/14.
//
//

#import "DirectionsViewController.h"

@interface DirectionsViewController ()

@end

@implementation DirectionsViewController
@synthesize destinationString,safariButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  //..  self.destinationString=ELMSAddress;
    self.safariButton.hidden=NO;
    if(![self connectedToInternet]){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\n No Internet Connection Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }

    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest; // 100 m
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization]; }
    
    
    [locationManager startUpdatingLocation];
    
   // [NSThread detachNewThreadSelector:@selector(startUpdatingLocation) toTarget:self withObject:nil];
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    self.safariButton.hidden=YES;
    locationManager.delegate=nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadTheWebView{
    
    CLLocationCoordinate2D start= [self deviceLocation]; //..[self geoCodeUsingAddress:BreakersAddress];
    
    CLLocationCoordinate2D destination =[self geoCodeUsingAddress:destinationString];
 //..   NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude, destination.latitude, destination.longitude];
   
     NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",start.latitude, start.longitude, destination.latitude, destination.longitude];
    
    
    NSURL *url=[NSURL URLWithString:googleMapsURLString];
    NSURLRequest *requestObj=[NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    
}

-(void)openAppleMaps{
   //.. http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f
    CLLocationCoordinate2D start= [self deviceLocation];
    CLLocationCoordinate2D destination =[self geoCodeUsingAddress:destinationString];
    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",start.latitude, start.longitude, destination.latitude, destination.longitude];
    
    NSURL *url=[NSURL URLWithString:googleMapsURLString];
    [[UIApplication sharedApplication] openURL:url];
    
}


- (CLLocationCoordinate2D)deviceLocation {
    
    return locationManager.location.coordinate;
    
}


- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude =-180, longitude =-180;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

-(IBAction)removeView:(UIButton*)sender{
    locationManager.delegate=nil;
    [self.view removeFromSuperview];
}

#pragma WebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
}


# pragma CLLocation Delegate

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
   /* UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show]; */
    //..[locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
  
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        locationManager=manager;
        
    }
    
    [self performSelector:@selector(loadTheWebView) withObject:nil afterDelay:0];
   //.. [self performSelector:@selector(openAppleMaps) withObject:nil afterDelay:0];
}


- (BOOL) connectedToInternet
{
    NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/m"];
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    if (data)
        return true;
    else
        return false;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
