//
//  GettingHereViewController.m
//  Mansion
//
//  Created by Falguni Shah on 12/13/14.
//
//

#import "GettingHereViewController.h"

@interface GettingHereViewController ()

@end

@implementation GettingHereViewController
@synthesize mansion,currentMansion,safariButton,currentMansionAddress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName=@"GettingHere";
    CGRect frame=self.view.frame;
    CGRect screenFrame= [[UIScreen mainScreen] bounds];
    frame.origin=screenFrame.origin;
    frame.size=screenFrame.size;
    self.view.frame=frame;
    // Do any additional setup after loading the view from its nib.
   [self.mapView removeFromSuperview];
    ;
    
   self.mapView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view addSubview:self.mapView];
    [self loadMaps];
    
    
    self.mapButton.selected=YES;
    self.mansionNameLable.text=mansion.mansionName;
    [self resetDatasourceForStores];
    
    
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    if(isIphone){
 //..   self.tableView.frame = CGRectMake(0, 40, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-40);
    }
}

-(void)loadMaps{
    NSString *str = @"https://www.google.com/maps/d/u/0/edit?mid=zIFZ4nJXYjsI.k1DRKhgyKM1Q";
    NSString *formatted =[str stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *addressString = [NSString stringWithFormat:@"%@%@",
                               @"https://maps.google.com/?q=", formatted];
    if(isIphone){
        newDirVC=[[NewDirectionViewController alloc]initWithNibName:@"NewDirectionViewController" bundle:nil];
    }
    else{
        newDirVC=[[NewDirectionViewController alloc]initWithNibName:@"NewDirectionViewController-iPad" bundle:nil];
    }
    
    newDirVC.urlString=str;
    newDirVC.view.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view addSubview:newDirVC.view];
}

-(void)resetDatasourceForStores{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"StoresProperty" ofType: @"plist"];
    storesDictionary = [NSDictionary dictionaryWithContentsOfFile: path];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:nil ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sort];
    
    
    
    NSArray *arr= [[storesDictionary allKeys] sortedArrayUsingDescriptors:sortDescriptors];
    storesKeys=[NSMutableArray arrayWithArray:arr];
    [storesKeys removeAllObjects];
    
    [storesKeys insertObject:@"Bannister's Wharf" atIndex:0];
    [storesKeys insertObject:@"The Breakers" atIndex:1];
    [storesKeys insertObject:@"The Elms" atIndex:2];
    [storesKeys insertObject:@"Marble House" atIndex:3];
     [storesKeys insertObject:@"Rosecliff" atIndex:4];
    [storesKeys insertObject:@"NewportStyle" atIndex:5];
   
  
    
   
}



-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
  //..  self.mapView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.mapView.frame)-CGRectGetHeight(self.bottomView.frame));
    self.mapView.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    //self.bottomView.layer.borderColor=[UIColor grayColor].CGColor;
    //self.bottomView.layer.borderWidth=1.0f;
  //..  [self getCurrentMansionAddress];
    [self setMapAndLegend];
    
}

-(void)setMapAndLegend{
    UIImage *mapImage=[UIImage imageNamed:[NSString stringWithFormat:@"%@_Map.png",currentMansion]];
    UIImage *legendImage=[UIImage imageNamed:[NSString stringWithFormat:@"%@_Legend.png",currentMansion]];
    self.mapImageView.image=mapImage;
    self.legendImageView.image=legendImage;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)getCurrentMansionAddress{
    NSArray * allKeys=[storesDictionary allKeys];
    int  index=[allKeys indexOfObject:currentMansion];
    
    currentMansionAddress=[NSString stringWithFormat:@"%@,%@",[allKeys objectAtIndex:index],[[storesDictionary objectForKey:[allKeys objectAtIndex:index]] objectAtIndex:0]];
        //..[[storesDictionary objectForKey:[allKeys objectAtIndex:index]] objectAtIndex:0];
    
}



-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"])
        
    {
        return self.mapImageView;
    }
    
    return nil;

}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContents ];
}


- (void)centerScrollViewContents {
    CGSize boundsSize = mapScrollView.bounds.size;
     CGRect contentsFrame = self.mapImageView.frame;
             if (contentsFrame.size.width < boundsSize.width) {
                contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
            } else {
                    contentsFrame.origin.x = 0.0f;
                }
    
        
        if (contentsFrame.size.height < boundsSize.height) {
                contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
            } else {
                    contentsFrame.origin.y = 0.0f;
                }
        
        self.mapImageView.frame = contentsFrame;
}



-(IBAction)removeView:(UIButton*)sender {
    [directionVC.view removeFromSuperview];
    [self.view removeFromSuperview];
}

-(IBAction)loadDirectionScreen:(UIButton*)sender{
 /*   [directionVC.view removeFromSuperview];
    directionVC=nil;
    if(isIphone){
       directionVC=[[DirectionsViewController alloc]initWithNibName:@"DirectionsViewController-iPhone" bundle:nil];
    }
    
    else{
         directionVC=[[DirectionsViewController alloc]initWithNibName:@"DirectionsViewController-iPad" bundle:nil];
    }
   
    directionVC.safariButton=self.safariButton;
    directionVC.destinationString=currentMansionAddress;
    directionVC.view.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view addSubview:directionVC.view]; */
    
    NSString *formatted =[currentMansionAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
 /*   NSString *addressString = [NSString stringWithFormat:@"%@%@",
                               @"http://maps.apple.com/?q=", formatted]; */
    
    NSString *addressString = [NSString stringWithFormat:@"%@%@",
                               @"https://maps.google.com/?q=", formatted];

 // http://maps.apple.com/maps?saddr=Current%20Location&daddr=Your_Address
    
        //addressString= currentMansionAddress;//[cell.addressButton currentTitle];
    
    if(isIphone){
        newDirVC=[[NewDirectionViewController alloc]initWithNibName:@"NewDirectionViewController" bundle:nil];
    }
    else{
        newDirVC=[[NewDirectionViewController alloc]initWithNibName:@"NewDirectionViewController-iPad" bundle:nil];
    }
    
    newDirVC.urlString=addressString;
    self.safariButton.hidden=NO;
    
    
    newDirVC.view.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view addSubview:newDirVC.view];

}

-(void)loadDirectionForStoreAddress:(NSString*)address{
    [directionVC.view removeFromSuperview];
    directionVC=nil;
    directionVC=[[DirectionsViewController alloc]initWithNibName:@"DirectionsViewController-iPad" bundle:nil];
    directionVC.safariButton=self.safariButton;
    directionVC.destinationString=address;
    directionVC.view.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view addSubview:directionVC.view];
}


-(void)loadStoresScreen{
    [self formatTheTextLable];
    self.storesView.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view addSubview:self.storesView];
}

-(IBAction)loadScreenForDetail:(UIButton*)sender{
    self.mapButton.selected=NO;
     self.directionButton.selected=NO;
     self.parkingButton.selected=NO;
     self.storesButton.selected=NO;
    self.safariButton.hidden=YES;

     sender.selected=YES;
    [self.mapView removeFromSuperview];
    [directionVC.view removeFromSuperview];
    [webViewCtrl.view removeFromSuperview];
    [self.storesView removeFromSuperview];
    [newDirVC.view removeFromSuperview];
    switch (sender.tag) {
        case 1:{
            
              [self.view addSubview:self.mapView];
            [self loadMaps];}
            break;
        case 2:
            [self loadDirectionScreen:sender];
            break;
        case 3:{
            [self.view addSubview:self.mapView];
            
        }
            break;
        case 4:
            //..[self.view addSubview:self.mapView];
            [self loadStoresScreen];
            break;
            
            
        default:
            break;
    }
    
    
}


#pragma tableview datasource and delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(isIphone){
       return  160;
    }
    return 230;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[storesDictionary allKeys] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StoresCell";
    
    StoresTableViewCell *cell = (StoresTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        //cell = [[SubStopsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSArray *nib;
        
        if(isIphone){
            nib = [[NSBundle mainBundle] loadNibNamed:@"StoresTableViewCell-iPhone" owner:self options:nil];
        }
        else{
          nib   = [[NSBundle mainBundle] loadNibNamed:@"StoresTableViewCell-iPad" owner:self options:nil];
        }
        
       
        cell = [nib objectAtIndex:0];
         cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        
        if(isIphone){
             cell.greenImage.frame=CGRectMake(10, 23, 109, 111);
            cell.mansionLable.frame = CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+10, 20, CGRectGetWidth(cell.mansionLable.frame), CGRectGetHeight(cell.mansionLable.frame));
            cell.addressButton.frame=CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+10, CGRectGetMaxY(cell.mansionLable.frame)+10, CGRectGetWidth(cell.contentView.frame)-CGRectGetWidth(cell.greenImage.frame)-50, CGRectGetHeight(cell.addressButton.frame));
             cell.phoneNumberButton.frame=CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+10, CGRectGetMaxY(cell.addressButton.frame)+10, CGRectGetWidth(cell.addressButton.frame)/2+20, CGRectGetHeight(cell.phoneNumberButton.frame));
             cell.extNumber.frame=CGRectMake(CGRectGetMaxX(cell.phoneNumberButton.frame)+5, CGRectGetMaxY(cell.addressButton.frame)+10, CGRectGetWidth(cell.extNumber.frame), CGRectGetHeight(cell.extNumber.frame));
        }
        
        else{
             cell.greenImage.frame=CGRectMake(10, 13, 350, 204);
        }
       
        
        
        //..cell.bagImage.frame=CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+50, 30, 86, 83);
        
    }
    cell.greenImage.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    cell.greenImage.layer.borderWidth=2.0f;
   
    cell.extNumber.hidden=YES;
    if(indexPath.row==[storesKeys count]-1)
        cell.extNumber.hidden=NO;
        
    cell.greenImage.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:2]]];
    cell.mansionLable.text=[storesKeys objectAtIndex:indexPath.row];
    NSString *address = [[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:0];
   
    
    
  /*  [cell.addressButton setTitle:address forState:UIControlStateNormal];
    [cell.phoneNumberButton setTitle:[[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:1] forState:UIControlStateNormal];
    NSLog(@"Phone Number : %@", [[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:1]); */
    cell.addressButton.tag=indexPath.row;
    [cell.addressButton setAttributedTitle:[attributedAddressArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    [cell.phoneNumberButton setAttributedTitle:[attributedPhoneNumberArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
   
    
    return cell;
}

#pragma Custom Cell Delegate

-(void)didClickOnAddressButtonForCell:(StoresTableViewCell*)cell withButton:(UIButton*)button{
    
    NSArray * allKeys=storesKeys;
    int  index=button.tag;
    NSLog(@"%@",[button currentAttributedTitle].string);
    NSString *reformAddress = [NSString stringWithFormat:@"%@,%@",[allKeys objectAtIndex:index],[button currentAttributedTitle].string];
    if([[allKeys objectAtIndex:index] isEqualToString:@"Bannister's Wharf"]){
        reformAddress =@"Bannister's Wharf Newport , Rhode Island 02840";
        //..[button currentAttributedTitle].string;
    }
    
    NSString *formatted = [reformAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *addressString = [NSString stringWithFormat:@"%@%@",
                               @"https://maps.google.com/?q=", formatted];
    //http://maps.apple.com/?q=
    //https://maps.google.com/?q=%@
    
    if([formatted isEqualToString:[cell.addressButton currentTitle]]){
        addressString=[cell.addressButton currentTitle];}
    
    if(button.tag==[storesKeys count]-1){
        addressString = [button currentAttributedTitle].string;
    }
    
    if(isIphone){
         webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    }
    else{
         webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    }
   
    webViewCtrl.urlString=addressString;
    [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil];
    
  /*  NSString *addressString = [cell.addressButton currentTitle];
    [self loadDirectionForStoreAddress:addressString]; */

}



-(void)didClickOnPhoneNumberButtonForCell:(StoresTableViewCell*)cell withButton:(UIButton*)button{
    NSString *phoneURLString = [NSString stringWithFormat:@"tel://%@", [button currentAttributedTitle].string];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
    
    {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\nCall Not Supported" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)formatTheTextLable {
    
    attributedAddressArray=[[NSMutableArray alloc]initWithCapacity:1];
    attributedPhoneNumberArray=[[NSMutableArray alloc]initWithCapacity:1];
    for(int i=0 ; i<storesKeys.count; i++){
         NSMutableAttributedString* addString = [[NSMutableAttributedString alloc] initWithString:[[storesDictionary objectForKey:[storesKeys objectAtIndex:i]] objectAtIndex:0]];
        NSMutableAttributedString* phoneString=[[NSMutableAttributedString alloc] initWithString:[[storesDictionary objectForKey:[storesKeys objectAtIndex:i]] objectAtIndex:1]];
        
        [addString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:(NSRange){0,1}];
        [addString addAttribute:NSUnderlineColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 1)];
        [addString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:(NSRange){0,[addString length]}];
        
        if(isIphone){
        [phoneString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:(NSRange){0,1}];
        [phoneString addAttribute:NSUnderlineColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 1)];
       
        [phoneString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:(NSRange){0,[phoneString length]}];
        }
        
        [attributedAddressArray addObject:addString];
        [attributedPhoneNumberArray addObject:phoneString];
        
        
        
        
    }
    
    
}



@end
