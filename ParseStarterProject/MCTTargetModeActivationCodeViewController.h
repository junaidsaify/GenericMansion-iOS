//
//  MCTTargetModeActivationCodeViewController.h
//  MCT
//
//  Created by Erwan Aguillon on 26/01/2015.
//  Copyright (c) 2015 Synchronoss Software Ltd. All rights reserved.
//

/**
 * Copyright 2009 Jeff Verkoeyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "Decoder.h"
#import "parsedResults/ParsedResult.h"
#import "OverlayView.h"
#import "P2PBaseViewController.h"
#import "PCVoucherManager.h"

//Activation code screen
#define MCT_ACTIVATION_CODE_TITLE NSLocalizedString(@"MCT_ACTIVATION_CODE_TITLE",@"Activate your phone...")
#define MCT_ACTIVATION_ACTIVATE_BUTTON_LABEL NSLocalizedString(@"MCT_ACTIVATION_ACTIVATE_BUTTON_LABEL",@"Activate")
#define MCT_ACTIVATION_CODE_INPUT_LABEL NSLocalizedString(@"MCT_ACTIVATION_CODE_INPUT_LABEL",@"Type your activation code...")
#define MCT_ACTIVATION_CODE_SCAN_CODE_LABEL NSLocalizedString(@"MCT_ACTIVATION_CODE_SCAN_CODE_LABEL",@"Scan code label")
#define MCT_ACTIVATION_CODE_HELP_BUTTON NSLocalizedString(@"MCT_ACTIVATION_CODE_HELP_BUTTON",@"Button Help")
//Activation code alert
#define MCT_ACTIVATION_CODE_ALERT_TITLE NSLocalizedString(@"MCT_ACTIVATION_CODE_ALERT_TITLE",@"Voucher warning")
#define MCT_ACTIVATION_CODE_INVALID_RESPONSE NSLocalizedString(@"MCT_ACTIVATION_CODE_INVALID_RESPONSE",@"Invalid response from server")
#define MCT_ACTIVATION_CODE_ERROR_NETWORK NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_NETWORK",@"Network error")
#define MCT_ACTIVATION_CODE_BODY_EMPTY NSLocalizedString(@"MCT_ACTIVATION_CODE_BODY_EMPTY",@"Empty code")
#define MCT_ACTIVATION_CODE_ERROR_NOT_STARTED NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_NOT_STARTED",@"Voucher not activated")
#define MCT_ACTIVATION_CODE_VALID_FROM_DATE_TO_DATE NSLocalizedString(@"MCT_ACTIVATION_CODE_VALID_FROM_DATE_TO_DATE",@"Voucher not activated")
#define MCT_ACTIVATION_CODE_ERROR_EXPIRED NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_EXPIRED",@"Voucher expired")
#define MCT_ACTIVATION_CODE_ERROR_EXPIRED_ON_DATE NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_EXPIRED_ON_DATE",@"Voucher expired")
#define MCT_ACTIVATION_CODE_ERROR_MAX_REACHED NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_MAX_REACHED",@"Voucher reached maximum")
#define MCT_ACTIVATION_CODE_ERROR_REVOKED NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_REVOKED",@"Voucher revoked")
#define MCT_ACTIVATION_CODE_ERROR_NOT_FOUND NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_NOT_FOUND",@"Voucher does not exist")
#define MCT_ACTIVATION_CODE_ERROR_FORMAT NSLocalizedString(@"MCT_ACTIVATION_CODE_ERROR_FORMAT",@"Error: check format of the activation code")
#define MCT_ACTIVATION_CODE_INVALID_BARCODE NSLocalizedString(@"MCT_ACTIVATION_CODE_INVALID_BARCODE",@"Invalid barcode")
#define MCT_ACTIVATION_CODE_IS_FOR_PAIRING NSLocalizedString(@"MCT_ACTIVATION_CODE_IS_FOR_PAIRING",@"Is pairing QR Code")
#define MCT_ACTIVATION_CODE_HELP NSLocalizedString(@"MCT_ACTIVATION_CODE_HELP",@"Activation code help")

@protocol ActivationCodeDelegate;

#if !TARGET_IPHONE_SIMULATOR
#define HAS_AVFF 1
#endif

@interface MCTTargetModeActivationCodeViewController : P2PBaseViewController <UITextFieldDelegate, DecoderDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, PCVoucherManagerDelegate
#if HAS_AVFF
, AVCaptureVideoDataOutputSampleBufferDelegate
#endif
> {
    NSSet *readers;
    ParsedResult *result;
    OverlayView *overlayView;
    SystemSoundID beepSound;
    BOOL showCancel;
    NSURL *soundToPlay;
    id<ActivationCodeDelegate> delegate;
    BOOL wasCancelled;
    BOOL oneDMode;
#if HAS_AVFF
    AVCaptureSession *captureSession;
    AVCaptureVideoPreviewLayer *prevLayer;
#endif
    BOOL decoding;
    BOOL isStatusBarHidden;
    BOOL isDisplayedAsOption;
    
    UIView *voucherCodeView;
    UITextField *textField;
    UIView* waitingView;
    UIActivityIndicatorView* activityIndicatorView;
}

#if HAS_AVFF
@property (nonatomic, retain) AVCaptureSession *captureSession;
@property (nonatomic, retain) AVCaptureVideoPreviewLayer *prevLayer;
#endif
@property (nonatomic, retain ) NSSet *readers;
@property (nonatomic, assign) id<ActivationCodeDelegate> delegate;
@property (nonatomic, retain) NSURL *soundToPlay;
@property (nonatomic, retain) ParsedResult *result;
@property (nonatomic, retain) OverlayView *overlayView;
@property (nonatomic,assign) BOOL isDisplayedAsOption;
@property (nonatomic, retain) UITextField *textField;
@property (nonatomic, retain) UIView* waitingView;
@property (nonatomic, retain) UIView* voucherCodeView;
@property (nonatomic, retain) UIActivityIndicatorView* activityIndicatorView;

- (id)initWithDelegate:(id<ActivationCodeDelegate>)scanDelegate;

- (BOOL)fixedFocus;

@end

@protocol ActivationCodeDelegate
- (void)onActivationCodeResult:(BOOL)success;
@end



