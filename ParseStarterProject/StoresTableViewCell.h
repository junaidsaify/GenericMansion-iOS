//
//  StoresTableViewCell.h
//  Mansion
//
//  Created by Falguni Shah on 1/4/15.
//
//

#import <UIKit/UIKit.h>
@class StoresTableViewCell;
@protocol StoreDelegate<NSObject>
@optional

-(void)didClickOnAddressButtonForCell:(StoresTableViewCell*)cell withButton:(UIButton*)button;
-(void)didClickOnPhoneNumberButtonForCell:(StoresTableViewCell*)cell withButton:(UIButton*)button;
@end

@interface StoresTableViewCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UIButton *addressButton,*phoneNumberButton;
@property(nonatomic,retain)IBOutlet UILabel *mansionLable,*extNumber;
@property(nonatomic,retain)IBOutlet UIImageView *greenImage,*bagImage;
@property(nonatomic,assign)id <StoreDelegate>delegate;
-(IBAction)addressButtonClicked:(UIButton*)sender;
-(IBAction)phoneNumberButtonClicked:(UIButton*)sender;
@end
