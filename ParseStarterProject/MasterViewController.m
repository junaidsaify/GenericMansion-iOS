//
//  MasterViewController.m
//  Mansion
//

//
//

#import "MasterViewController.h"
#import "UIActionSheet+Blocks.h"

#import "IDZTrace.h"
#import "IDZOggVorbisFileDecoder.h"


@interface MasterViewController ()
@property (nonatomic, strong) id<IDZAudioPlayer> customPlayer;
@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) setUpSideMenu{
    SideMenuManager *sideMenuManager = [[SideMenuManager alloc]init];
    self.sideTableItems =    [sideMenuManager fetchSideMenu];
    
}

-(void)setupBottomMenu
{
    SideMenuManager *sideMenuManager = [[SideMenuManager alloc] init];
    self.bottomMenuItems = [sideMenuManager fetchBottomMenu];
    
    CGRect prevCompartment = CGRectZero;
    int i=1;
    for(BottomMenu *bottomMenu in self.bottomMenuItems)
    {
        CGRect compartment = CGRectMake(CGRectGetMaxX(prevCompartment), CGRectGetMinY(prevCompartment), CGRectGetWidth(self.bottomView.frame)/[self.bottomMenuItems count], CGRectGetHeight(self.bottomView.frame));
        prevCompartment = compartment;
        MansionButton *button = [[MansionButton alloc] init];
        UILabel *textLabel = [[UILabel alloc]init];
        button.tag=i;
        button.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        textLabel.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
     //..   [button setBackgroundColor: [[UIColor blueColor] colorWithAlphaComponent:0.3]];
        button.frame = CGRectMake(0.0, 0.0, 30, 30);
        button.center = CGPointMake(CGRectGetMidX(compartment), (CGRectGetHeight(compartment)/2)-10);
               [self.bottomView addSubview:button];
        
        //..    textLabel.frame=CGRectMake(CGRectGetMinX(compartment), CGRectGetMaxY(compartment), 100, 80);
        CGSize expectedLabelSize = [bottomMenu.menuName sizeWithAttributes:
        @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:22]}];
        textLabel.frame=CGRectMake(CGRectGetMinX(compartment), CGRectGetMaxY(compartment), expectedLabelSize.width, expectedLabelSize.height);

        textLabel.center = CGPointMake(CGRectGetMidX(button.frame), CGRectGetMaxY(button.frame)+10);
        [textLabel setText:bottomMenu.menuName];
        textLabel.textAlignment = NSTextAlignmentCenter;
        [self.bottomView addSubview:textLabel];

        
        [button sd_setBackgroundImageWithURL:[NSURL URLWithString:bottomMenu.menuImagePath] forState:UIControlStateNormal];
        [button setContentMode:UIViewContentModeScaleAspectFit];
        button.associatedLink=bottomMenu.menuLink;
        button.isSafari=bottomMenu.menuBrowser;
        [button addTarget:self action:NSSelectorFromString([NSString stringWithFormat:@"%@:",bottomMenu.menuOnClick]) forControlEvents:UIControlEventTouchUpInside];
        NSLog(@"Current Title %@",button.currentTitle);
        i++;
    }

    
    
}




-(void)setupTopMenu{
   
    SideMenuManager *sideMenuManager = [[SideMenuManager alloc]init];
    self.topMenuItems =    [sideMenuManager fetchTopMenu];
    
    CGRect prevCompartment = CGRectZero; //CGRectMake(CGRectGetMaxX(self.sideMenuPopUpButton.frame)-50, 0, 10, 40);

    for(TopMenu *topMenu in self.topMenuItems)
    {
        CGRect compartment = CGRectMake(CGRectGetMaxX(prevCompartment), CGRectGetMinY(prevCompartment), CGRectGetWidth(self.topView.frame)/[self.topMenuItems count], CGRectGetHeight(self.topView.frame));
        prevCompartment = compartment;
         MansionButton *button = [[MansionButton alloc] init];
  //  [button setBackgroundColor: [[UIColor blueColor] colorWithAlphaComponent:0.3]];
       // button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
      //  button.contentEdgeInsets = UIEdgeInsetsMake(0, 80, 0, 0);
      

        CGSize stringsize;
        if(isIphone)
    stringsize    = [topMenu.topMenuName sizeWithAttributes:
                             @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:12]}];
   else
       stringsize    = [topMenu.topMenuName sizeWithAttributes:
                        @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18]}];
        
        
        if(isIphone){
              button.center = CGPointMake(CGRectGetMidX(compartment), CGRectGetHeight(compartment)/2);
           [button setFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y, stringsize.width, 40)];
           [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
        }
        else{
              button.center = CGPointMake(CGRectGetMidX(compartment), CGRectGetHeight(compartment)/2);
      [button setFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y, stringsize.width, 40)];
         [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        }
         button.center = CGPointMake(CGRectGetMidX(compartment), CGRectGetHeight(compartment)/2);
        [self.topView addSubview:button];
        
        
        button.associatedLink=topMenu.topMenuLink;
        button.isSafari=topMenu.topMenuBrowser;
   //    [button sd_setBackgroundImageWithURL:[NSURL URLWithString:topMenu.topMenuImagePath] forState:UIControlStateNormal];
        [button setContentMode:UIViewContentModeScaleAspectFit];
        [button setTitle:topMenu.topMenuName forState:UIControlStateNormal];
   
     
        if( ![topMenu.topMenuOnClick isEqualToString:@""])
        [button addTarget:self action:NSSelectorFromString([NSString stringWithFormat:@"%@:",topMenu.topMenuOnClick]) forControlEvents:UIControlEventTouchUpInside];
       //.. [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   //   button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
      /*  UILabel *textLabel = [[UILabel alloc]init];
        textLabel.frame =CGRectMake(CGRectGetMinX(button.frame), CGRectGetMaxY(button.frame), CGRectGetWidth(button.frame), 30);
        textLabel.center = CGPointMake(CGRectGetMidX(button.frame), CGRectGetMaxY(button.frame));
        textLabel.text=topMenu.topMenuName;
        textLabel.textAlignment = NSTextAlignmentCenter;
        if(isIphone)
        [textLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        else
            [textLabel setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        [self.topView addSubview:textLabel];
     */
    }

    
    
}


-(void)setupMansions{
    
    MansionManager *mansionManager =[[MansionManager alloc]init];
   self.mansionArray = [mansionManager fetchAllMansions];
}

-(void)setupCustomer{
    CustomerManager *customerManager = [[CustomerManager alloc]init];
    customerDetails = [[customerManager fetchCustomerDetails] firstObject];
}

-(void)setupFeedbackMailDetails{
    FeedbackMailManager *feedbackMailManager = [[FeedbackMailManager alloc]init];
    feedbackMailDetails =[[feedbackMailManager fetchFeedbackMailDetails] firstObject];
}

-(void)setupSegmentViewDetails{
    SegmentViewManager *segmentViewManager = [[SegmentViewManager alloc]init];
    segmentViewArray = [segmentViewManager fetchSegmentViewDetails];
    [self.segmentControl removeAllSegments];
    for(int i=0;i<segmentViewArray.count;i++){
        SegmentViewDetails *svd= [segmentViewArray objectAtIndex:i];
     //..   [self.segmentControl setTitle:svd.name forSegmentAtIndex:i];
        [self.segmentControl insertSegmentWithTitle:svd.name atIndex:i animated:NO];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.screenName=@"MainView";
    pageSwipe=NO;
    playerStopped=NO;
    CGRect frame=self.view.frame;
    CGRect screenFrame= [[UIScreen mainScreen] bounds];
    frame.origin=screenFrame.origin;
    frame.size=screenFrame.size;
    self.view.frame=frame;
    
   
    self.mainView.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
   
    [self.view addSubview:self.mainView];
    
    self.sideView.frame=CGRectMake(-CGRectGetWidth(self.sideView.frame), 0, CGRectGetWidth(self.sideView.frame), CGRectGetHeight(self.view.frame));
    [self.view addSubview:self.sideView];
   /* [self setUpSideMenu];
    [self setupTopMenu];
    [self setupBottomMenu];
    [self setupMansions];
    [self setupCustomer];
    [self setupFeedbackMailDetails];
    [self setupSegmentViewDetails];
    [self.actionShowLogoButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.actionShowImagePath] forState:UIControlStateNormal placeholderImage:nil];*/
    self.floorListTableView.delegate=self;
    if ([self.sideTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.sideTableView setSeparatorInset:UIEdgeInsetsZero];
        [self.floorListTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.sideTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    
    
 
    
    
    self.sideMansionArray=[[NSMutableArray alloc]initWithObjects:@"The Elms",@"The Breakers",@"Marble House",@"Rosecliff",@"Chateau-sur-Mer",@"Isaac Bell House",@"Kingscote",@"Chepstow",@"The Breakers Stable and Carriage House",@"Hunter House",@"Green Animals Topiary Garden",@"Newport Mansions Store",nil];
    //@"Chateaur-sur-Mer",@"Kingscote",@"Isaac Bell House",@"Green Animals"
  //..  self.sideTableItems=[[NSMutableArray alloc]initWithObjects:@"Select a Tour",@"Download Tours",@"Explore",@"Getting Here",@"Events",@"Share",@"App Feedback",@"Join & Support",@"About Us",nil];
    
 
    
    
  //.. wz final  self.sideTableItems=[[NSMutableArray alloc]initWithObjects:@"Select a Tour",@"Getting Here",@"About Us",@"Explore",@"Events",@"Join & Support",@"Share",@"Download Tours",@"App Feedback",nil];
    
    
    
    isExpandTouched=NO;
    indexOfExpandCell=0;
    
    
    
   //.. UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(-9, CGRectGetMaxY(self.downloadMediaButton.frame), 597 , 423)];// 620,400
    UIScrollView *scr = [[UIScrollView alloc]init];
    if(isIphone){
        scr.frame =CGRectMake(12, CGRectGetMaxY(self.downloadMediaButton.frame), CGRectGetWidth(self.patchImageView.frame), CGRectGetMinY(self.baseFrameButton.frame) -CGRectGetMaxY(self.downloadMediaButton.frame));
       
        
    }
    else{
        scr.frame = CGRectMake(-9, CGRectGetMaxY(self.downloadMediaButton.frame), 602 , 423);
    }
    
     scr.backgroundColor=[UIColor clearColor];
    scr.tag = 1;
    scr.pagingEnabled=YES;
    scr.delegate=self;
    scr.autoresizingMask=UIViewAutoresizingNone;
    [scr setShowsHorizontalScrollIndicator:NO];
    [scr setShowsVerticalScrollIndicator:NO];
    [self.mainView addSubview:scr];
    //..[self setupScrollView:scr];
    tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapOnMansion:)];
    [scr addGestureRecognizer:tap];
    
    tapOnView=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTapOnView:)];
    [self.mainView addGestureRecognizer:tapOnView];

    
    
    
    UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(scr.frame)+20, 480, 50)];
    pgCtr.userInteractionEnabled=NO;
    [pgCtr setTag:12];
    pgCtr.numberOfPages=self.mansionArray.count;
    pgCtr.pageIndicatorTintColor=[UIColor whiteColor];
    pgCtr.currentPageIndicatorTintColor=[UIColor colorWithString:@"897350"]; //00563F
    pgCtr.autoresizingMask=UIViewAutoresizingNone;
    [self.mainView addSubview:pgCtr];
    if([self.mansionArray count]==1){
        pgCtr.hidden=YES;
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(subStopRemoved:) name:@"SubstopsRemoved" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(audioPlayerFinishedPlaying:) name:@"AudioPlayerFinishedPlaying" object:nil];

    
   // self.topView.layer.borderWidth=1.0;
   // self.topView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MansionProperty" ofType: @"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile: path];
    mansionLearnArray = [dict objectForKey: @"Learn"];
    mansionAboutUsArray=[dict objectForKey:@"About Us"];
    cellCollectionArray=[[NSMutableArray alloc]initWithCapacity:1];
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    dbManager=[[DBManager alloc]initWithSQLPath:[NSString stringWithFormat:@"%@/%@.sqlite",mansionPath,currentMansion.shortName]];
    [dbManager openDataBase];
    
    self.downloadMansionDataButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.downloadMansionDataButton.contentEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    
    /*swipeLeftRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [swipeLeftRight setDirection:(UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft )];
    [self.tourDetailView addGestureRecognizer:swipeLeftRight]; */
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.tourDetailView addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.tourDetailView addGestureRecognizer:swiperight];
    dummy=[[UIButton alloc]init];
    
    [self configureAboutMansion];
    
    NSString *storesPath = [[NSBundle mainBundle] pathForResource:@"GettingHereProperty" ofType: @"plist"];
    storesDictionary = [NSDictionary dictionaryWithContentsOfFile: storesPath];
    
//..    [self setupAppUI]; // Used for all cosmetic changes,assets etc
    
    

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pauseThePlayer)
                                                 name:@"SubstopPlay"
                                               object:nil];
    [self.view bringSubviewToFront:self.topView];
    
   
    
    
}


-(void)setupAppUI {
    // Setting corresponding color scheme for Customer
    self.sideTableView.backgroundColor=[UIColor colorWithString:customerDetails.sideMenuColor];
    self.sideView.backgroundColor=[UIColor colorWithString:customerDetails.sideMenuColor];
    self.topView.backgroundColor=[UIColor colorWithString:customerDetails.topMenuColor];
    self.bottomView.backgroundColor=[UIColor colorWithString:customerDetails.bottomMenuColor];
    
    [leftArrow sd_setImageWithURL:[NSURL URLWithString:customerDetails.tourPreviousArrowPath] placeholderImage:nil ];
    [rightArrow sd_setImageWithURL:[NSURL URLWithString:customerDetails.tourNextArrowPath] placeholderImage:nil];
    [self.appFeedbackArrowImg sd_setImageWithURL:[NSURL URLWithString:customerDetails.tourNextArrowPath] placeholderImage:nil];
    
    [self.playPauseButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.tourPlayButtonPath] forState:UIControlStateNormal placeholderImage:nil];
    [self.playPauseButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.tourPauseButtonPath] forState:UIControlStateSelected placeholderImage:nil];
    [self.greenPlayButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.autoPlayButtonPath] forState:UIControlStateNormal placeholderImage:nil];
        [self.maximizeImageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.maximizeImagePath] forState:UIControlStateNormal placeholderImage:nil];
    [self.actionSheetButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.popoverButtonPath] forState:UIControlStateNormal placeholderImage:nil];
    [self.semiUp sd_setImageWithURL:[NSURL URLWithString:customerDetails.substopShowImagePath] placeholderImage:nil];
    [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    [self.closeTranscriptButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.closeButtonImagePath] forState:UIControlStateNormal placeholderImage:nil];
    [self.maximizeImageButton setContentMode:UIViewContentModeScaleAspectFit];
    self.maximizedImageView.clipsToBounds=YES;
    [self.segmentViewButton setTitle:customerDetails.segmentViewName forState:UIControlStateNormal];
    
}





-(IBAction)openActionShowSite:(UIButton*)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ActionShow]];
}

-(void)getCurrentMansionAddress{
    NSArray * allKeys=[storesDictionary allKeys];
    int  index=[allKeys indexOfObject:currentMansion];
    
    currentMansionAddress=[NSString stringWithFormat:@"%@,%@",[allKeys objectAtIndex:index],[[storesDictionary objectForKey:[allKeys objectAtIndex:index]] objectAtIndex:0]]; //[[storesDictionary objectForKey:[allKeys objectAtIndex:index]] objectAtIndex:0];
  /*  if([[allKeys objectAtIndex:index] isEqualToString:@"Marble House"]){
        currentMansionAddress=[[storesDictionary objectForKey:[allKeys objectAtIndex:index]] objectAtIndex:0];
    }*/
}

-(void)configureAboutMansion{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AboutMansion" ofType: @"plist"];
    aboutMansionDict = [NSDictionary dictionaryWithContentsOfFile: path];
    
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer{
   // NSLog(@"left swipe");
    if(self.nextRoom.hidden==YES){
        return;
    }
    dummy.tag=2;
    [self loadRooms:dummy];
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(self.previousRoom.hidden==YES){
        return;
    }

    dummy.tag=1;
    [self loadRooms:dummy];
    //Do what you want here
    // NSLog(@"right swipe");
}

- (void)orientationChanged:(NSNotification *)notification{
 //..   NSLog(@"Change hua orientation %d", [[UIApplication sharedApplication] statusBarOrientation]);
    //..[[UIApplication sharedApplication] statusBarOrientation]
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(UIInterfaceOrientationIsPortrait(orientation)){
        
        NSLog(@"potrait hai");
     //   self.maximizedView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
     //   NSLog(@"width is %@", NSStringFromCGPoint(CGRectGetWidth(self.view.frame)));
    }
    else{
           NSLog(@"landscape hai");
    //      self.maximizedView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    //    self.maximizedImageView.frame = self.maximizedView.frame;
        
    }
    self.maximizedImageView.center = self.maximizedView.center;
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
 //    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
  //  [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];

 /*   tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"MainScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
    
    [self.view addSubview:self.sideView];
    [self setUpSideMenu];
    [self setupTopMenu];
    [self setupBottomMenu];
    [self setupMansions];
    [self setupCustomer];
    [self setupFeedbackMailDetails];
    [self setupSegmentViewDetails];
    [self.actionShowLogoButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.actionShowImagePath] forState:UIControlStateNormal placeholderImage:nil];
    
    UIScrollView *mainScr= (UIScrollView*)[self.view viewWithTag:1];
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
  //..  mainScr.center=CGPointMake(self.mainView.center.x,CGRectGetMaxY(self.downloadMediaButton.frame)+CGRectGetHeight(mainScr.frame)/2);
    if(isIphone){
    mainScr.frame=CGRectMake(self.patchImageView.frame.origin.x, self.patchImageView.frame.origin.y, CGRectGetWidth(self.patchImageView.frame), CGRectGetHeight(self.patchImageView.frame));
  mainScr.contentSize=CGSizeMake(CGRectGetWidth(self.patchImageView.frame)*[self.mansionArray count], CGRectGetHeight(self.patchImageView.frame));
    }
    else{
        mainScr.center=CGPointMake(self.mainView.center.x,CGRectGetMaxY(self.downloadMediaButton.frame)+CGRectGetHeight(mainScr.frame)/2);
    }
    pgCtr.center=CGPointMake(self.mainView.center.x, CGRectGetMaxY(mainScr.frame)+30);
    [self setupScrollView:mainScr];
   //.. self.topView.backgroundColor=[UIColor colorWithString:@"#00563F"]; // green 00563F
    //..self.sideTableView.separatorColor=[UIColor colorWithString:customerDetails.sideMenuColor]; // 0F4D3A//0a382b//083025
    
    //..[self.homeImage setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Home.png"]]];
    //..self.homeImage.image=[UIImage imageNamed:@"Home.png"];
    [self.customerLogo sd_setImageWithURL:[NSURL URLWithString:customerDetails.sideMenuImagePath]];
    [self.customerLogo  setContentMode: UIViewContentModeScaleAspectFit];
    if(isIphone){
        [self.homeImage sd_setImageWithURL:[NSURL URLWithString:customerDetails.iphoneGridImagePath]];
    }
    else{
    [self.homeImage sd_setImageWithURL:[NSURL URLWithString:customerDetails.ipadGridImagePath]];
    }
    NSFileManager *manager = [NSFileManager defaultManager];
    if([self isContentPresentForMansion:currentMansion.shortName withFileManager:manager]){
        [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
        [self.downloadMansionDataButton setTitle:@"" forState:UIControlStateNormal];
        CGRect dlButtonRect =self.downloadMansionDataButton.frame;
        self.downloadMansionDataButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 52, 40);
    }
    else{
        [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"DownloadNow.png"] forState:UIControlStateNormal];
        [self.downloadMansionDataButton setTitle:@"75MB" forState:UIControlStateNormal];
        CGRect dlButtonRect =self.downloadMansionDataButton.frame;
        self.downloadMansionDataButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 74, 25);
    }
    
    //self.bottomView.layer.borderColor=[UIColor grayColor].CGColor;
    //self.bottomView.layer.borderWidth=1.0f;
    self.downloadOptionView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    self.downloadOptionBaseView.layer.cornerRadius=10.0f;
    self.downloadOptionBaseView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    self.downloadOptionBaseView.layer.borderWidth=5.0f;
//..    [self.homePageScrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame), 1180)];
  /*  if(isIphone){
        CGFloat diff = (CGRectGetHeight(self.interiorImage.frame)+CGRectGetHeight(self.aboutMansionTextView.frame)+CGRectGetHeight(self.interiorImage.frame))-CGRectGetHeight(self.view.frame);
       
        
        
        [self.homePageScrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame),CGRectGetHeight(self.view.frame)+diff)]; //1030
    
    } */

    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupAppUI];
    });
 // Used for all cosmetic changes,assets etc
}



-(void)setDynamicHeightOfTextView {
    
    CGRect frame = self.aboutMansionTextView.frame;
    
       
  frame.size=  [self calculateHeightForString:self.aboutMansionTextView.text];
    
    self.aboutMansionTextView.frame=frame;
    self.interiorImage.frame = CGRectMake(0, CGRectGetMaxY(self.aboutMansionTextView.frame), CGRectGetWidth(self.interiorImage.frame), CGRectGetHeight(self.interiorImage.frame));


    
    
}

-(void)resetScrollViewContent{
    CGFloat diff;
    
    if(isIphone)
    diff= (CGRectGetHeight(self.interiorImage.frame)+CGRectGetHeight(self.aboutMansionTextView.frame)+CGRectGetHeight(self.exteriorImage.frame)+CGRectGetHeight(self.bottomView.frame));
    else{
         diff= (CGRectGetHeight(self.interiorImage.frame)+CGRectGetHeight(self.aboutMansionTextView.frame)+CGRectGetHeight(self.exteriorImage.frame)+20);
    }
    
    
    [self.homePageScrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.frame),diff)]; //1030
}



- (CGSize)calculateHeightForString:(NSString *)str
{
    CGSize size = CGSizeZero;
    
    UIFont *labelFont = [UIFont fontWithName:@"Helvetica" size:16];//[UIFont systemFontOfSize:17.0f];
    NSDictionary *systemFontAttrDict = [NSDictionary dictionaryWithObject:labelFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *message = [[NSMutableAttributedString alloc] initWithString:str attributes:systemFontAttrDict];
    CGRect rect = [message boundingRectWithSize:(CGSize){CGRectGetWidth(self.view.frame)-20, MAXFLOAT}
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                        context:nil];//you need to specify the some width, height will be calculated
    
    size = CGSizeMake(rect.size.width, rect.size.height + 22); //padding// 18
    if(size.width<CGRectGetWidth(self.view.frame)){
        size.width = CGRectGetWidth(self.view.frame)-20;
    }
    
    return size;
    
    
}


- (void)setupScrollView:(UIScrollView*)scrMain {
   
    
    for (int i=0; i<[self.mansionArray count]; i++) {
       
        MansionModel *mansion = [self.mansionArray objectAtIndex:i];
        
        UIImageView *mansionImageView=[[UIImageView alloc] initWithFrame:CGRectMake((i)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height)];
        mansionImageView.backgroundColor=[UIColor clearColor];
        [mansionImageView sd_setImageWithURL:[NSURL URLWithString:mansion.imagePath] placeholderImage:[UIImage imageNamed:@"Loading1.png"]];
        
    //..    mansionImageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"Mansion-Image%d.png",i]];
    //..    mansionImageView.tag=i;
     //..    scrMain.layer.cornerRadius=50.0;
       //.. scrMain.layer.borderWidth=1.0;
        [scrMain addSubview:mansionImageView];
       
        
        
        
    }
    // set the content size to mansionarray width
  //..  [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*self.mansionArray.count, scrMain.frame.size.height)];
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*[self.mansionArray count], scrMain.frame.size.height)];
   }



- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if(sender.tag!=1){
        return;
    }
    CGFloat contentOffset = sender.contentOffset.x;
    int nextPage = (int)(contentOffset/sender.frame.size.width);
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
    pgCtr.currentPage=nextPage;
    //NSLog(@"Scroll view scrolled to page %d",nextPage+1);
    
    
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if(scrollView.tag!=1){
        return;
    }
    
  /*  CGFloat contentOffset = scrollView.contentOffset.x;
    int nextPage = (NSInteger)(contentOffset/scrollView.frame.size.width);
    NSArray *imageViewArray=[scrollView subviews];
    UIImageView *imageView=[imageViewArray objectAtIndex:nextPage];
    UIImage * toImage = [UIImage imageNamed:[NSString stringWithFormat:@"MansionImage%d_Int",nextPage]];
   
    for (int i=0; i<imageViewArray.count; i++) {
        UIImageView *imageView=[imageViewArray objectAtIndex:i];
        imageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"MansionImage%d",i]];
    }
    
   [UIView transitionWithView:imageView
                      duration:1.5f
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        imageView.image=toImage;
                    } completion:nil];
    
    */
   
  
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    if(scrollView.tag==50){
        if(scrollView.zoomScale<=1){
          
     self.maximizedDoneButton.hidden=NO;
        }
        else{
            self.maximizedDoneButton.hidden=YES;

        }
        }
    [self centerScrollViewContents];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)showLeftMenu:(UIButton*)sender {
    
    if(self.sideMenuPopUpButton.tag==0){
         sender.tag=1;
         self.sideMenuPopUpButton.tag=1;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        self.sideView.frame=CGRectMake(0, 0, CGRectGetWidth(self.sideView.frame), CGRectGetHeight(self.view.frame));
        self.mainView.frame=CGRectMake(CGRectGetWidth(self.sideView.frame), 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        [UIView commitAnimations];
        
    }
    
    else {
         sender.tag=0;
         self.sideMenuPopUpButton.tag=0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        self.sideView.frame=CGRectMake(-CGRectGetWidth(self.sideView.frame), 0, CGRectGetWidth(self.sideView.frame), CGRectGetHeight(self.view.frame));
        self.mainView.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
        [UIView commitAnimations];
    }
     [self.sideTableView reloadData];// hack
}

#pragma tableview datasource and delegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   //.. if(indexPath.row == 3 && tableView.tag!=19)
   //..     return 0;
    
    if(tableView.tag!=19){
    
    if(cellCollectionArray.count>0){
        
        if(indexPath.row ==[[cellCollectionArray objectAtIndex:0] integerValue]){
            
            if(indexPath.row==3){
            //   return ((self.mansionArray.count*50)+60);
                return ((self.sideMansionArray.count*50)+60);
            }
            else if(indexPath.row==4){
               return ((mansionLearnArray.count*50)+60);
            }
            else if (indexPath.row==7){
               return ((mansionAboutUsArray.count*50)+60);
            }
            
        }
        else{
            return 60;
        }
        
    }
    
    else{
         //..NSLog(@"Size of cell 106 baba and row %d",indexPath.row);
        return 60;
    }
        
    }
    
    else{
        if(isIphone){
            return 60;
        }
        
        return 75;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    
    if(tableView.tag!=19){
    return [self.sideTableItems count];
    }
    else{
       //.. return [self.floorRoomsArray count];
        
        return [self.allFloorRoomArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    if(tableView.tag!=19){
    
    CustomTableViewCell *cell = (CustomTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       //.. cell.contentView.clipsToBounds=YES;
        cell.contentView.backgroundColor=[UIColor colorWithString:customerDetails.sideMenuColor];
        cell.cellTextLable.textColor=[UIColor whiteColor];
        cell.cellTextLable.numberOfLines=2;
        cell.cellTextLable.lineBreakMode = NSLineBreakByWordWrapping;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
   
        SideMenu *sideMenu = [self.sideTableItems objectAtIndex:indexPath.row];
        cell.cellTextLable.text = sideMenu.menuName;
        NSURL *url = sideMenu.menuImagePath;
        [cell.cellImageView sd_setImageWithURL:url placeholderImage:nil];
        [cell.cellImageView setContentMode:UIViewContentModeScaleAspectFit];
   // cell.cellTextLable.text=[self.sideTableItems objectAtIndex:indexPath.row];
  //  cell.cellImageView.image= [UIImage imageNamed:[NSString stringWithFormat:@"side%ld",(long)indexPath.row]];
    cell.tag=indexPath.row;
    [self dePopulateTheButtonForCell:cell];
        
    
    
        
    if(cellCollectionArray.count>0){
       /* if(indexPath.row==[[cellCollectionArray objectAtIndex:0] integerValue]){
            if(isIphone){
                
            }
            else{
            cell.cellImageView.frame= CGRectMake(20, 10, 41, 38);
            cell.cellTextLable.frame = CGRectMake(CGRectGetMaxX(cell.cellImageView.frame)+15, CGRectGetMinY(cell.cellImageView.frame), 300, 50);
            }
            [self populateTheButtonForCell:cell withIndexPath:indexPath];
        } */
    }
    else {
     
        if(isIphone){
            cell.cellImageView.center=CGPointMake(25, CGRectGetMidY(cell.contentView.frame));
           
            cell.cellImageView.frame = CGRectMake(cell.cellImageView.frame.origin.x, cell.cellImageView.frame.origin.y, 30, 30);
            cell.cellTextLable.frame = CGRectMake(CGRectGetMaxX(cell.cellImageView.frame)+10, CGRectGetMidY(cell.contentView.frame)-25, 160, 60);
        }
        
        else{
        cell.cellImageView.center=CGPointMake(30, CGRectGetMidY(cell.contentView.frame));
        cell.cellTextLable.frame = CGRectMake(CGRectGetMaxX(cell.cellImageView.frame)+15, CGRectGetMidY(cell.contentView.frame)-35, 200, 60);
        }
        
    }
    
   /* if(indexPath.row == 3 && tableView.tag!=19)
    {
        [cell setHidden:YES];
    } */
    
    return cell;
        
    }
    
    else{
        ListViewTableViewCell_iPad *cell =(ListViewTableViewCell_iPad*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       
        if (cell==nil) {
            NSArray *nib;
            if(isIphone){
                  nib = [[NSBundle mainBundle] loadNibNamed:@"ListViewTableViewCell-iPhone" owner:self options:nil];
            }
            else{
                 nib = [[NSBundle mainBundle] loadNibNamed:@"ListViewTableViewCell-iPad" owner:self options:nil];
            }
           
            
            
            
            cell = (ListViewTableViewCell_iPad*) [nib objectAtIndex:0];
            
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        

//..        if([[self.floorRoomsArray objectAtIndex:indexPath.row] isEqualToString:presentRoom]){
     
        if([[self.allFloorRoomArray objectAtIndex:indexPath.row] isEqualToString:presentRoom]){

      //  cell.roomImage.image=[UIImage imageNamed:@"currentAudio.png"];
            [cell.roomImage sd_setImageWithURL:[NSURL URLWithString:customerDetails.listViewSelectedImagePath] placeholderImage:nil];
            //..cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"List_View_Audio"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        }
        else{
            //cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"List_View"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
           // cell.roomImage.image=[UIImage imageNamed:@"AllCell.png"];
            [cell.roomImage sd_setImageWithURL:[NSURL URLWithString:customerDetails.listViewDefaultImagePath] placeholderImage:nil];
        }

        
        cell.roomTextLable.text=[NSString stringWithFormat:@"%@ %@",[self.allFloorRoomArray objectAtIndex:indexPath.row],[dbManager getNameForRoom:[self.allFloorRoomArray objectAtIndex:indexPath.row]]];
       // cell.textLabel.font=[UIFont fontWithName:@"Futura" size:20.0];
        cell.floorTextLable.text = [dbManager getFloorNameForRoom:[self.allFloorRoomArray objectAtIndex:indexPath.row]];
        return cell;
    }
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   /*  CustomTableViewCell *cell = (CustomTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
   
    if(indexPath.row!=indexOfExpandCell){
        isExpandTouched=NO;
    }
    
    if(indexPath.row==0){
        [self.mansionDetailsView removeFromSuperview];
        isExpandTouched=NO;
        indexOfExpandCell=nil;
      
    }
    
    if(indexPath.row ==1 ){
        
        if(!cell.isExpanded){
        
        isExpandTouched=YES;
        indexOfExpandCell=indexPath.row;
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
        }
        
        else{
            isExpandTouched=NO;
            indexOfExpandCell=indexPath.row;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
       
    }
    
    if(indexPath.row ==5 ){
        
        if(!cell.isExpanded){
            
            isExpandTouched=YES;
            indexOfExpandCell=indexPath.row;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
        
        else{
            isExpandTouched=NO;
            indexOfExpandCell=indexPath.row;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
        
    }
    
    
    if(indexPath.row ==6 ){
        
        if(!cell.isExpanded){
            
            isExpandTouched=YES;
            indexOfExpandCell=indexPath.row;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
        
        else{
            isExpandTouched=NO;
            indexOfExpandCell=indexPath.row;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
        
    }

    
    
    
    
    
    else{
     //..   isExpandTouched=NO;
      //..  indexOfExpandCell=nil;
        //..indexOfExpandCell=indexPath.row;
    }
    
   */
    //..[cellCollectionArray removeAllObjects];
    if(tableView.tag!=19){
     
        [self removeAllViews];
        self.webActionSheetButton.hidden=YES;
        [self.tourDetailView removeFromSuperview];
        [subStopVC.view removeFromSuperview];
        subStopVC=nil;
        [directionVC.view removeFromSuperview];
        [storesVC.view removeFromSuperview];
        [webViewCtrl.view removeFromSuperview];
        [newDirVC.view removeFromSuperview];
        
        SideMenu *sideMenu = [self.sideTableItems objectAtIndex:indexPath.row];
        NSString *browser= sideMenu.menuBrowser;
        NSString *methodToCall = sideMenu.menuOnClick;
        [self performSelector:NSSelectorFromString([NSString stringWithFormat:@"%@:",methodToCall]) withObject:sideMenu];
        
        
 /*
    if(indexPath.row==0){
         currentMansion=@""; // added later
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        [self.mansionDetailsView removeFromSuperview];
        [gettingHereVC.view removeFromSuperview];
        if(directionVC){
            [directionVC.view removeFromSuperview];
            
        }
        isExpandTouched=NO;
        indexOfExpandCell=nil;
        
        
        
    }
    
    
    if(indexPath.row==3){
        
        if([cellCollectionArray containsObject:[NSNumber numberWithInt:indexPath.row]]){
           [cellCollectionArray removeAllObjects];
        }
        
        else{
            [cellCollectionArray removeAllObjects];
            [cellCollectionArray addObject:[NSNumber numberWithInt:indexPath.row]];
        }
        
    }
        
    else if(indexPath.row==8){
        [cellCollectionArray removeAllObjects];
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        
        [self showAboutUs:nil];
    }
        
    else if(indexPath.row==2){
        [cellCollectionArray removeAllObjects];
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        
        [self showExplore];

    }
    
    
    else if (indexPath.row==4){
       
  
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        
        [self showEvents];

    }
    
    else if (indexPath.row==7){
             if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        
        [self showJoin];
    }
    else if (indexPath.row==5){
        [cellCollectionArray removeAllObjects];
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        imageToShare=[UIImage imageNamed:@"Mansion_Splash.png"];
       //.. [self showShareScreen];
        [self sendDataForTrackingForAction:@"Share" label:@"SplashScreen"];
        [self shareText:Subject andImage:imageToShare andUrl:[NSURL URLWithString:NewportURL]];
        
        
    }
    
    else if (indexPath.row==1){
        [cellCollectionArray removeAllObjects];
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
        [self downloadMansionData:nil];
    }
    else if (indexPath.row==6){
        [cellCollectionArray removeAllObjects];
        if(self.sideMenuPopUpButton.tag==1){
            [self showLeftMenu:self.sideMenuPopUpButton];}
     //..   UIImage *image=[UIImage imageNamed:@"Mansion_Splash.png"];
        [self sendEmailWIthAttachments:nil withMessage:[NSString stringWithFormat:@"\n%@",AppFeedbackMessage] ];
    }
    
    
    else{
        [cellCollectionArray removeAllObjects];
    }
        
        */

    [tableView beginUpdates];
  //..  [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView reloadData];
    [tableView endUpdates];

    }
    
    else {
        [self stopAudioPlayer];
        [self loadRoomForNumber:[self.allFloorRoomArray objectAtIndex:indexPath.row]andAutoPlay:NO];
        [self.mainView addGestureRecognizer:tapOnView];
        [self removeSegmentedView:nil];
    }
    
}

-(void)removeAllViews{
    
    [exploreVC.view removeFromSuperview];
    [aboutUsVC.view removeFromSuperview];
    [joinVC.view removeFromSuperview];
    [eventsVC.view removeFromSuperview];
    
}

-(void)dePopulateTheButtonForCell:(CustomTableViewCell*)cell{
    
    for(UIView *buttonView in cell.contentView.subviews){
        if([buttonView isKindOfClass:[UIButton class]])
        [buttonView removeFromSuperview];
    }
}


-(void)populateTheButtonForCell:(CustomTableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath{
  
    if(indexPath.row==3){
        [self dePopulateTheButtonForCell:cell];
        
        for (int i=0;i<self.sideMansionArray.count;i++){
            
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            button.tag=i;
            button.backgroundColor=[UIColor blackColor];
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=1.0f;
            if(isIphone){
                
                button.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:14];
            }
            else{
                 button.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:16];
            }
            
            [button setBackgroundImage:[UIImage imageNamed:@"GettingHere.png"] forState:UIControlStateNormal];
            [button setTitle:[self.sideMansionArray objectAtIndex:i] forState:UIControlStateNormal];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0, 80, 0, 0);
            
            button.frame=CGRectMake(0, (i*50)+60, CGRectGetWidth(cell.contentView.frame), 50);
           button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            [button addTarget:self action:@selector(showMansion:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:button];
        }

    }
    
    if(indexPath.row==4){
        
        [self dePopulateTheButtonForCell:cell];
        for (int i=0;i<mansionLearnArray.count;i++) {
            
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            button.tag=i;
            button.backgroundColor=[UIColor blackColor];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.contentEdgeInsets = UIEdgeInsetsMake(0, 80, 0, 0);
            [button setTitle:[mansionLearnArray objectAtIndex:i] forState:UIControlStateNormal];
            button.frame=CGRectMake(0, (i*50)+60, CGRectGetWidth(cell.contentView.frame), 50);
            [button addTarget:self action:@selector(showLearn:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:button];
        }
        
    }

    
    
    if(indexPath.row==7){
        [self dePopulateTheButtonForCell:cell];
    for (int i=0;i<mansionAboutUsArray.count;i++){
        
        UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
        button.tag=i;
        button.backgroundColor=[UIColor blackColor];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 80, 0, 0);
        [button setTitle:[mansionAboutUsArray objectAtIndex:i] forState:UIControlStateNormal];
        button.frame=CGRectMake(0, (i*50)+60, CGRectGetWidth(cell.contentView.frame), 50);
        [button addTarget:self action:@selector(showAboutUs:) forControlEvents:UIControlEventTouchUpInside];
       
        [cell.contentView addSubview:button];
    }
    
    }

    
}

-(void)loadMansionOfIndex:(NSInteger)mansionIndex
{
    currentMansion=[self.mansionArray objectAtIndex:mansionIndex];
 //..   [self getCurrentMansionAddress]; // for address
  //..  [self sendDataForTrackingForAction:@"TourHomeScreen" label:currentMansion];
    self.aboutMansionTextView.text=currentMansion.aboutMansion;
    [self.nextMansionButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.mansionRightArrowPath] forState:UIControlStateNormal];
    [self.previousMansionButton sd_setBackgroundImageWithURL:[NSURL URLWithString:customerDetails.mansionLeftArrowPath] forState:UIControlStateNormal];
    
    
    
    [self.exteriorImage sd_setImageWithURL:[NSURL URLWithString:currentMansion.exteriorImagePath] placeholderImage:[UIImage imageNamed:@"Loading1.png"]];
    [self.interiorImage sd_setImageWithURL:[NSURL URLWithString:currentMansion.interiorImagePath] placeholderImage:[UIImage imageNamed:@"Loading1.png"]];
    
    
    currentMansionAddress=currentMansion.address;
    self.mansion=[[Mansion alloc]init];
    self.mansion= [self.mansion fetchDataForMansionName:currentMansion.shortName];
    
    
    self.mansionDetailsView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
    //..  self.homePageScrollView.frame=self.mansionDetailsView.frame;
    [self.mainView addSubview:self.mansionDetailsView];
    self.aboutButton.selected=YES;
    //if(isIphone){
    [self setDynamicHeightOfTextView];
    
    if(currentMansion.interiorImagePath==nil || [currentMansion.interiorImagePath isEqualToString:@""]){
        [self.interiorImage setHidden:YES];
    }
    
    else{
        [self.interiorImage setHidden:NO];
        [self performSelector:@selector(resetScrollViewContent) withObject:nil afterDelay:0.5];
    }

    //..}
    self.currentPage = mansionIndex;
    if([self.mansionArray count]==1){
        [self.previousMansionButton setHidden:YES];
        [self.nextMansionButton setHidden:YES];
        return;
    }

    
    
    if(self.currentPage == 0)
    {
        [self.previousMansionButton setHidden:YES];
    }
    else if (self.currentPage == self.mansionArray.count-1)
    {
        [self.nextMansionButton setHidden:YES];
    }
    else
    {
        [self.previousMansionButton setHidden:NO];
        [self.nextMansionButton setHidden:NO];
    }
}

-(void)didTapOnMansion:(UITapGestureRecognizer*)tapGesture{
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(showLeft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.mansionDetailsView addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(showRight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.mansionDetailsView addGestureRecognizer:swiperight];

    
    
    
    
    if(self.sideMenuPopUpButton.tag==1)
        [self showLeftMenu:self.sideMenuPopUpButton];
    
    
    [self resetAllTabButtons];
    [self.aboutButton setSelected:YES];
    UIScrollView *scrollView=(UIScrollView*)tapGesture.view;
    CGFloat contentOffset = scrollView.contentOffset.x;
    self.currentPage = (int)(contentOffset/scrollView.frame.size.width);
    
    [self loadMansionOfIndex:self.currentPage];
}


-(void)sendDataForTrackingForAction:(NSString*)action label:(NSString*)lable{
    NSMutableDictionary *event =   [[GAIDictionaryBuilder createEventWithCategory:@"UI"
                                                                          action:action
                                                                           label:lable
                                                                           value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    [[GAI sharedInstance] dispatch];
}


-(void)didTapOnView:(UITapGestureRecognizer*)tapGesture{
    if(self.sideMenuPopUpButton.tag==1)
    [self showLeftMenu:self.sideMenuPopUpButton];
}


-(void)showAboutUs:(UIButton*)sender {
    sender.tag=1;
    self.sideMenuPopUpButton.tag=1;
    [self showLeftMenu:sender];
    
    if(isIphone){
        aboutUsVC=[[AboutUsViewController alloc]initWithNibName:@"AboutUsViewController" bundle:nil];
 
    }
    else{
   aboutUsVC=[[AboutUsViewController alloc]initWithNibName:@"AboutUsViewController-iPad" bundle:nil];
    }
     aboutUsVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
   
    [self.mainView addSubview:aboutUsVC.view];
}

-(void)showLearn:(UIButton*)sender {
    sender.tag=1;
    self.sideMenuPopUpButton.tag=1;
    [self showLeftMenu:sender];
    if(isIphone){
          learnVC=[[LearnViewController alloc]initWithNibName:@"LearnViewController" bundle:nil];
    }
    else{
    learnVC=[[LearnViewController alloc]initWithNibName:@"LearnViewController-iPad" bundle:nil];
    }
    [self.mainView addSubview:learnVC.view];
}

-(void)showExplore{
    self.sideMenuPopUpButton.tag=1;
    [self showLeftMenu:nil];
    if(isIphone){
         exploreVC=[[ExploreViewController alloc]initWithNibName:@"ExploreViewController" bundle:nil];
    }
    else{
    exploreVC=[[ExploreViewController alloc]initWithNibName:@"ExploreViewController-iPad" bundle:nil];
    }
    exploreVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
    [self.mainView addSubview:exploreVC.view];
}


-(void)showMansion:(UIButton*)sender {
    
    NSLog(@"The mansion to visit is %@", [self.sideMansionArray objectAtIndex:sender.tag]);
 //..   self.mansion=[[Mansion alloc]init];
  //.. self.mansion= [self.mansion fetchDataForMansionName:@"Elms"];
   //.. sender.tag=1;
//    if(sender.tag>self.mansionArray.count-1){
//        return;
//    }
    currentMansion=[self.sideMansionArray objectAtIndex:sender.tag];
    self.sideMenuPopUpButton.tag=1;
    
    [self showLeftMenu:sender];
    
   
    if(gettingHereVC){
        [gettingHereVC.view removeFromSuperview];
        gettingHereVC=nil;
    }
    
    if(isIphone){
         gettingHereVC=[[GettingHereViewController alloc]initWithNibName:@"GettingHereViewController-iPhone" bundle:[NSBundle mainBundle]];
        
   
    }
    
    else{
    gettingHereVC=[[GettingHereViewController alloc]initWithNibName:@"GettingHereViewController-iPad" bundle:nil];
    }
    
    gettingHereVC.currentMansion=currentMansion;
    [self getCurrentMansionAddress];// set address
    gettingHereVC.safariButton=self.webActionSheetButton;
    gettingHereVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
    gettingHereVC.mansion=self.mansion;
    gettingHereVC.currentMansionAddress=currentMansionAddress;
    [self.mainView bringSubviewToFront:self.topView];
    [self.mainView addSubview:gettingHereVC.view];
    [self.view bringSubviewToFront:gettingHereVC.view]; // added
    
    
}

-(void)loadRoomForNumber:(NSString*)roomNumber andAutoPlay:(BOOL)value{
    
    if(subStopVC){
        [subStopVC stopAudioPlayer];
        [subStopVC.view removeFromSuperview];
        subStopVC=nil;
    }
    
    
    self.roomNameFakeBotton.enabled=false;
    [self.greenPlayButton setHidden:NO];
    if([customerDetails.isAutoPlay caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
        self.greenPlayButton.hidden=NO;
    }
    
    else{
        self.greenPlayButton.hidden=YES;
    }
    

    presentRoom=roomNumber;
    [self prepareFloorRoomArrayForRoom:roomNumber]; // floorRoomArray Prepared
    [self.appFeedbackButton setHidden:YES];//mandar
    [self.appFeedbackArrowImg setHidden:YES];
    
    
    self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
    
    if([self.allFloorRoomArray firstObject]==roomNumber && [self.allFloorRoomArray count]==1)
    
    {
        self.previousRoom.hidden=YES;
        leftArrow.hidden=YES;
        self.nextRoom.hidden=YES;
        rightArrow.hidden=YES;
        [self.greenPlayButton setHidden:YES];//mandar
        [self.appFeedbackButton setHidden:NO];//mandar
        [self.appFeedbackArrowImg setHidden:NO];
        
    }
    
    
    
    else   if([self.allFloorRoomArray firstObject] == roomNumber){
        self.previousRoom.hidden=YES;
        leftArrow.hidden=YES;
        self.nextRoom.hidden=NO;
        rightArrow.hidden=NO;
        [self.nextRoom setTitle:[self.allFloorRoomArray objectAtIndex:1] forState:UIControlStateNormal];
        
    }
    
    else if([self.allFloorRoomArray lastObject] == roomNumber){
        self.roomNameFakeBotton.enabled=true;
        
        self.previousRoom.hidden=NO;
        leftArrow.hidden=NO;
        [self.greenPlayButton setHidden:YES];//mandar
        [self.appFeedbackButton setHidden:NO];//mandar
        [self.appFeedbackArrowImg setHidden:NO];
        self.nextRoom.hidden=YES;
        rightArrow.hidden=YES;
        int indexOfPresentRoom=[self.allFloorRoomArray indexOfObject:roomNumber];
        if([self.floorRoomsArray count]==1){
            [self.previousRoom setTitle:[self.allFloorRoomArray objectAtIndex:indexOfPresentRoom-1] forState:UIControlStateNormal];
        }
        else{
            
            [self.previousRoom setTitle:[self.allFloorRoomArray objectAtIndex:[self.floorRoomsArray count]-2] forState:UIControlStateNormal];
        }
    }
    
    else{
        self.nextRoom.hidden=NO;
        self.previousRoom.hidden=NO;
        leftArrow.hidden=NO;
        rightArrow.hidden=NO;
        NSInteger index = [self.allFloorRoomArray indexOfObject:roomNumber];
        
        [self.previousRoom setTitle:[self.allFloorRoomArray objectAtIndex:index-1] forState:UIControlStateNormal];
        
        [self.nextRoom setTitle:[self.allFloorRoomArray objectAtIndex:index+1] forState:UIControlStateNormal];
        
    }
    
    
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
    [self setTourImageForPath:roomPath];
    
     if(value)
        [self playTheAudio:nil];
    
  /*  self.roomNameFakeBotton.enabled=false;
    [self.greenPlayButton setHidden:NO];
     presentRoom=roomNumber;
    [self prepareFloorRoomArrayForRoom:roomNumber]; // floorRoomArray Prepared
    [self.appFeedbackButton setHidden:YES];//mandar
    [self.appFeedbackArrowImg setHidden:YES];
    
   
    self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
    
    if([self.allFloorRoomArray firstObject] == roomNumber){
        self.previousRoom.hidden=YES;
        leftArrow.hidden=YES;
        self.nextRoom.hidden=NO;
        rightArrow.hidden=NO;
        [self.nextRoom setTitle:[self.allFloorRoomArray objectAtIndex:1] forState:UIControlStateNormal];
        
    }
    
    else if([self.allFloorRoomArray lastObject] == roomNumber){
        self.roomNameFakeBotton.enabled=true;

        self.previousRoom.hidden=NO;
        leftArrow.hidden=NO;
        [self.greenPlayButton setHidden:YES];//mandar
        [self.appFeedbackButton setHidden:NO];//mandar
        [self.appFeedbackArrowImg setHidden:NO];
        self.nextRoom.hidden=YES;
        rightArrow.hidden=YES;
        [self.previousRoom setTitle:[self.allFloorRoomArray objectAtIndex:[self.floorRoomsArray count]-2] forState:UIControlStateNormal];
        
    }
    
    else{
        self.nextRoom.hidden=NO;
        self.previousRoom.hidden=NO;
        leftArrow.hidden=NO;
        rightArrow.hidden=NO;
        NSInteger index = [self.allFloorRoomArray indexOfObject:roomNumber];
        
        [self.previousRoom setTitle:[self.allFloorRoomArray objectAtIndex:index-1] forState:UIControlStateNormal];
        
        [self.nextRoom setTitle:[self.allFloorRoomArray objectAtIndex:index+1] forState:UIControlStateNormal];
        
    }

    
     NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
    [self setTourImageForPath:roomPath];
    
    // set sub stop
    
    [self sendDataForTrackingForAction:@"TourScreen" label:self.roomLable.text];
    
    */
    
}

-(void)prepareFloorRoomArrayForRoom:(NSString*)roomNumber{
self.floorLabel.text= [dbManager getFloorNameForRoom:roomNumber];
    
    NSDictionary *mansionDataDictionary = self.mansion.floorDict;
    
    NSArray *allRooms = [[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys];
    allRooms = [self sortTheDatasource:allRooms];
    self.floorRoomsArray = [allRooms copy];
    
    
    // Setting Substops
    
    NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
    NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
    
    subStops = [roomDetails objectForKey:@"SubStops"];
    if([[roomDetails objectForKey:@"SubStops"] count]>0)
        subStops= [self getSubStopArrayForRoom];//..
    
    [self prepareSubStopsNameForArray:subStops];
    [self prepareImagePathForSubStops:subStops];
    //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
//..edit    [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
    [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    if([subStops count]==1)
        //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
   //.. edit     [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
        [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    //..    self.substopButton.userInteractionEnabled=YES;
    self.substopButton.hidden=NO;
    self.semiUp.hidden=NO;
    if([subStops count]==0){
        //..   self.substopButton.userInteractionEnabled=NO;
        self.substopButton.hidden=YES;
        self.semiUp.hidden=YES;
    }
    

    
}



-(void)resetAllTabButtons{
    self.aboutButton.selected=NO;
    self.tourButton.selected=NO;
    self.directionButton.selected=NO;
    self.ticketButton.selected=NO;
    self.storesButton.selected=NO;
}


-(IBAction)loadScreenForDetail:(UIButton*)sender{
    [self resetAllTabButtons];
       [self stopAudioPlayer];
  
    if(directionVC){
        
        [directionVC.view removeFromSuperview];
        directionVC=nil;
        self.webActionSheetButton.hidden=YES;
    }
    if(storesVC){
        [storesVC.view removeFromSuperview];
        storesVC=nil;
        
    }
    self.webActionSheetButton.hidden=YES;
    [webViewCtrl.view removeFromSuperview];
    [newDirVC.view removeFromSuperview];
 //..   [self.tourDetailView removeFromSuperview];
   //.. [subStopVC.view removeFromSuperview];
    
    sender.selected=YES;
    
    if(sender.tag==2){
        [self sendDataForTrackingForAction:@"TourScreen" label:currentMansion.shortName];
       
        
        audioDetailDict = [[NSMutableDictionary alloc]initWithCapacity:1];
     
        
        sender.selected=NO;
        if(!self.mansion){
            self.mansion=[[Mansion alloc]init];
        }
       self.mansion= [self.mansion fetchDataForMansionName:currentMansion.shortName];
       NSDictionary *mansionDataDictionary = self.mansion.floorDict;
        if(mansionDataDictionary==nil){
         //..   [self downloadMansionData:sender];
            [self manageDownloads:nil];
     
        
            self.aboutButton.selected=YES;
            return;
        }
        NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
        //dbManager=[[DBManager alloc]initWithSQLPath:[NSString stringWithFormat:@"%@/Elms.sqlite",mansionPath]]; commented by mandar
        dbManager=[[DBManager alloc]initWithSQLPath:[NSString stringWithFormat:@"%@/%@.sqlite",mansionPath,currentMansion.shortName]];
        [dbManager openDataBase];
        subStopsName=[[NSMutableArray alloc]initWithCapacity:1];
        subStopsImagePath=[[NSMutableArray alloc]initWithCapacity:1];
        self.tourDetailView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
        
        //-CGRectGetHeight(self.bottomView.frame)
        [self.mainView addSubview:self.tourDetailView];
        [self.view bringSubviewToFront:self.tourDetailView];
        
        
        allFloors=[self sortTheFloorForArray];  //[mansionDataDictionary allKeys];
        
        [self getAllFloorsRoomForDict:mansionDataDictionary];
        
        self.floorLabel.text=[allFloors objectAtIndex:0];
        NSArray *allRooms=[[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys];
        allRooms=[self sortTheDatasource:allRooms];
        self.floorRoomsArray=[allRooms copy];
        presentRoom=[self.floorRoomsArray objectAtIndex:0];
        self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
        
        if((self.floorRoomsArray.count==1) && [allFloors count]==1) //// single floor with single room
        {
            self.previousRoom.hidden=YES;
            self.nextRoom.hidden=YES;
            leftArrow.hidden=YES;
            rightArrow.hidden=YES;
            
            
            
            // Later written
            
            leftArrow.hidden=YES;
            [self.greenPlayButton setHidden:YES];//mandar
            [self.appFeedbackButton setHidden:NO];//mandar
            [self.appFeedbackArrowImg setHidden:NO];
            
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
            // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
                subStops= [self getSubStopArrayForRoom];//..
            
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
            //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
       //..edit      [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
           [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            if([subStops count]==1)
                //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
           //..edit      [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
                 [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            //     [self.appFeedbackButton setHidden:YES];
            //    [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
                //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }
            
            
            
            
        }
        
        
        
        
        
        if(self.floorRoomsArray.count==1 && [allFloors count]>1) // single room for first floor but multiple floor
        {
            self.previousRoom.hidden=YES;
            leftArrow.hidden=YES;
            self.nextRoom.hidden=NO;
            rightArrow.hidden=NO;
            [self.nextRoom setTitle:[self.allFloorRoomArray objectAtIndex:1] forState:UIControlStateNormal];
            
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
            // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
                subStops= [self getSubStopArrayForRoom];//..
            
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
            //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
         //..edit    [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
             [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            if([subStops count]==1)
                //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
          //..edit       [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
                 [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            [self.appFeedbackButton setHidden:YES];
            [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
                //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }
            
            
            
        }
        
        
        
        
        else   if(self.floorRoomsArray.count>1)  //  mutiple rooms
            
        {
            self.previousRoom.hidden=YES;
            leftArrow.hidden=YES;
            self.nextRoom.hidden=NO;
            rightArrow.hidden=NO;
            [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:1] forState:UIControlStateNormal];
            
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
            // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
                subStops= [self getSubStopArrayForRoom];//..
            
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
            //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
       //..edit      [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
             [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            if([subStops count]==1)
                //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
         //..edit        [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
                 [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            [self.appFeedbackButton setHidden:YES];
            [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
                //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }
            
            
            
            
            
            
        }
        
        else{
            self.previousRoom.hidden=YES;
            self.nextRoom.hidden=YES;
            leftArrow.hidden=YES;
            rightArrow.hidden=YES;
        }
        
        UIImage *minImage=[[UIImage imageNamed:@"GreenSlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        UIImage *maxImage=[[UIImage imageNamed:@"GreySlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        [audioProgressSlider setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [audioProgressSlider setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [audioProgressSlider setThumbImage:[UIImage imageNamed:@"TrackBall"] forState:UIControlStateNormal];
        
      
        
     /*   NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
        dbManager=[[DBManager alloc]initWithSQLPath:[NSString stringWithFormat:@"%@/%@.sqlite",mansionPath,currentMansion.shortName]];
        [dbManager openDataBase];
        subStopsName=[[NSMutableArray alloc]initWithCapacity:1];
        subStopsImagePath=[[NSMutableArray alloc]initWithCapacity:1];
    self.tourDetailView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
        
        //-CGRectGetHeight(self.bottomView.frame)
        [self.mainView addSubview:self.tourDetailView];
        [self.view bringSubviewToFront:self.tourDetailView];
        
        
        allFloors=[self sortTheFloorForArray];  //[mansionDataDictionary allKeys];
        
        [self getAllFloorsRoomForDict:mansionDataDictionary];
        
        self.floorLabel.text=[allFloors objectAtIndex:0];
        NSArray *allRooms=[[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys];
        allRooms=[self sortTheDatasource:allRooms];
        self.floorRoomsArray=[allRooms copy];
        presentRoom=[self.floorRoomsArray objectAtIndex:0];
        self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
        
        if(self.floorRoomsArray.count>1) {
            self.previousRoom.hidden=YES;
            leftArrow.hidden=YES;
            self.nextRoom.hidden=NO;
            rightArrow.hidden=NO;
        [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:1] forState:UIControlStateNormal];
        
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
   // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
            subStops= [self getSubStopArrayForRoom];//..
          
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
       //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
            [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
            
            if([subStops count]==1)
             //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
                [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
        //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            [self.appFeedbackButton setHidden:YES];
            [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
             //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }

            
            
            
            
            
        }
        
        else{
            self.previousRoom.hidden=YES;
            self.nextRoom.hidden=YES;
            leftArrow.hidden=YES;
            rightArrow.hidden=YES;
        }
        UIImage *minImage=[[UIImage imageNamed:@"GreenSlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        UIImage *maxImage=[[UIImage imageNamed:@"GreySlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        [audioProgressSlider setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [audioProgressSlider setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [audioProgressSlider setThumbImage:[UIImage imageNamed:@"TrackBall"] forState:UIControlStateNormal];
        
        [self playTheAudio:nil]; // later added
        */
    }
    
    else if(sender.tag==3){
        [self loadDirectionScreen:sender];
    }
    
    else if(sender.tag==4){
        sender.selected=NO;
        self.aboutButton.selected=YES;
        int index = [self.mansionArray indexOfObject:currentMansion];
        imageToShare=[UIImage imageNamed:[NSString stringWithFormat:@"MansionImage%d.png",index]];
     //..   [self showShareScreen];
        [self sendDataForTrackingForAction:@"Share" label:currentMansion];
        [self shareText:Subject andImage:imageToShare andUrl:[NSURL URLWithString:NewportURL]];
        
        
    }
    
}


-(void)getAllFloorsRoomForDict:(NSDictionary*)dict{
  self.allFloorRoomArray = [dbManager getAllRooms];
    
    /*   self.allFloorRoomArray = [[NSMutableArray alloc]initWithCapacity:1];
    for(int i=0;i<allFloors.count;i++){
       //.. [[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys]
        
        NSArray *tempArray = [[dict objectForKey:[allFloors objectAtIndex:i]] allKeys];
        tempArray=[self sortTheDatasource:tempArray];
        
      
        
        
        
        [self.allFloorRoomArray addObjectsFromArray:tempArray];
    } */
    
}

-(NSMutableArray*)sortTheFloorForArray {
    NSMutableArray *sortedFloorName=[[self.mansion.floorDict allKeys] mutableCopy];//[[NSMutableArray alloc]initWithCapacity:1];
    for(NSString *floorName in [self.mansion.floorDict allKeys]){
        int indexToAdd=[dbManager getFloorNumberForName:floorName];
        [sortedFloorName replaceObjectAtIndex:indexToAdd withObject:floorName];
    }
    
    return sortedFloorName;
}

-(void)prepareSubStopsNameForArray:(NSArray*)subStopsArray {
    [subStopsName removeAllObjects];
    for(NSString *stopNumber in subStopsArray){
        [subStopsName addObject:[dbManager getSubStopNameForRoom:stopNumber]];
    }
    
}

-(void)prepareImagePathForSubStops:(NSArray*)subStopsArray{
    [subStopsImagePath removeAllObjects];
    NSFileManager *fileMngr=[NSFileManager defaultManager];
   NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *subStopPath=[NSString stringWithFormat:@"%@/%@/%@/SubStops",mansionPath,self.floorLabel.text,presentRoom];
     for(NSString *stopNumber in subStopsArray){
         NSString *imagePath=[NSString stringWithFormat:@"%@/%@/Images",subStopPath,stopNumber];
         NSArray *imageArray = [fileMngr subpathsOfDirectoryAtPath:imagePath error:nil];
         if([imageArray count]>0){
             [subStopsImagePath addObject:[NSString stringWithFormat:@"%@/%@",imagePath,[imageArray objectAtIndex:0]]];
         }
         else{
             [subStopsImagePath addObject:[[NSBundle mainBundle]pathForResource:@"SubStops" ofType:@"png"]];
         }
     }
}

-(UIImage*)returnImageForPath:(NSString*)path{
    NSFileManager *fileMngr=[NSFileManager defaultManager];
    UIImage *newImage;
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@/Images",path,presentRoom];
    NSArray *imageArray=[fileMngr contentsOfDirectoryAtPath:imagePath error:nil];
    if(imageArray>0){
        newImage= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",imagePath,[imageArray objectAtIndex:0]]];
        if(newImage==nil){
             newImage= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",imagePath,[imageArray objectAtIndex:1]]];
        }
        
    }
    else{
        newImage=self.exteriorImage.image;//[UIImage imageNamed:@"MansionImage1.png"];
    }
    return newImage;
}

-(void)setTourImageForPath:(NSString*)path{
   /* NSFileManager *fileMngr=[NSFileManager defaultManager];
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@/Images",path,presentRoom];
    NSArray *imageArray=[fileMngr contentsOfDirectoryAtPath:imagePath error:nil];
    if(imageArray>0){
        UIImage *newImage= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",imagePath,[imageArray objectAtIndex:0]]]; */
        UIImage *newImage=[self returnImageForPath:path];
    imageToBeMaximized=newImage;
        [UIView transitionWithView:tourImageView
                          duration:1.0f
                           options:UIViewAnimationOptionCurveEaseIn
                        animations:^{
                            tourImageView.image = newImage;
                        } completion:nil];
    
    
}


-(IBAction)removeCurrentView:(id)sender{
    pageSwipe=NO;
    
    if(subStopVC){
        [subStopVC stopAudioPlayer];
        [subStopVC.view removeFromSuperview];
        subStopVC=nil;
    }
    [self stopAudioPlayer];
    [audioDetailDict removeAllObjects];
    audioDetailDict=nil;
    
    [self.tourDetailView removeFromSuperview];
    self.floorLabel.text=@""; // for sharing fallback.
    self.aboutButton.selected=YES;
    [self.appFeedbackButton setHidden:YES];
    [self.appFeedbackArrowImg setHidden:YES];
}


-(IBAction)downloadMansionData:(UIButton*)sender{
  
    [self.tourDetailView removeFromSuperview];
    NSArray *subViews=[self.transcriptView subviews];
    for(UIView *view in subViews){
        if(view.tag != 19){
            [view removeFromSuperview];
        }
    }
    
   
  //..  self.downloadButton.layer.cornerRadius=5.0;
  //..  self.downloadButton.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
  //..  self.downloadButton.layer.borderWidth=1.0;
    self.transcriptView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    self.downloadMansionView.layer.cornerRadius=10.0;
    self.downloadMansionView.layer.borderWidth=5.0;
    self.downloadMansionView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    //..[UIColor colorWithRed:120.0 green:181.0 blue:226.0 alpha:1].CGColor;
 
    self.transcriptView.frame=self.mainView.frame; // later iphone
    [self.transcriptView addSubview:self.downloadMansionView];
    
    [self.transcriptView bringSubviewToFront:self.closeTranscriptButton];
    self.downloadMansionView.center=self.transcriptView.center;
    self.closeTranscriptButton.center=CGPointMake(CGRectGetMaxX(self.downloadMansionView.frame), CGRectGetMinY(self.downloadMansionView.frame));
    [self.view addSubview:self.transcriptView];
    NSFileManager *manager=[NSFileManager defaultManager];
    if([self isContentPresentForMansion:currentMansion.shortName withFileManager:manager]){
      //..  [self.downloadButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
        self.downloadButton.layer.cornerRadius=0.0;
        self.downloadButton.layer.borderColor=[UIColor whiteColor].CGColor;
        
        [self.progressLabel setText:@"0%"];
        self.progressLabel.hidden=YES;
        [self.downloadButton setHidden:NO];
        [self.downloadProgressSlider setProgress:0.0 animated:YES];
        [self.downloadProgressSlider setHidden:YES];
        [self.downloadButton setTitle:@"" forState:UIControlStateNormal];
        [self.downloadButton setUserInteractionEnabled:NO];
       //.. CGRect dlButtonRect =self.downloadButton.frame;
      //    self.downloadButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 32, 25);
        [self.downloadButton setTitle:@"Download Complete" forState:UIControlStateNormal];
        self.deleteContentButton.hidden=NO;
    }
    else{
   //..     [self.downloadButton setBackgroundImage:[UIImage imageNamed:@"DownloadNow.png"] forState:UIControlStateNormal];
        [self.progressLabel setText:@"0%"];
        self.progressLabel.hidden=YES;
        [self.downloadButton setHidden:NO];
        [self.downloadProgressSlider setProgress:0.0 animated:YES];
        [self.downloadProgressSlider setHidden:YES];
        [self.downloadButton setTitle:@"Download Now" forState:UIControlStateNormal];
        [self.downloadButton setUserInteractionEnabled:YES];
        CGRect dlButtonRect =self.downloadButton.frame;
      //..  self.downloadButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 74, 25);
        self.deleteContentButton.hidden=YES;
        
        if(sender)
            [self startDownload:nil]; // added later for auto download.
    }
    
    self.closeTranscriptButton.hidden=YES; // added later for auto download.
   
    //..ftp://waws-prod-bn1-001.ftp.azurewebsites.windows.net/
  //..  ftp://waws-prod-bn1-001.ftp.azurewebsites.windows.net/Elms.zip
  
   
    
}

-(IBAction)startDownload:(id)sender{
    // check network connection;
    NSFileManager *manager=[NSFileManager defaultManager];
    if([self isContentPresentForMansion:currentMansion.shortName withFileManager:manager]) {
        return;
    }
   
    
   
    if(![self connectedToInternet]) {
        self.downloadButton.hidden=NO;
        [self.mansionNameButon setUserInteractionEnabled:YES];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\n No Internet Connection Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if(sender!=nil){
     UIButton *dlButton=(UIButton*)sender;
     dlButton.userInteractionEnabled=NO;
    }
    
    [self sendDataForTrackingForAction:@"DownloadNow" label:@"DownloadNow"];
    
    [self.mansionNameButon setUserInteractionEnabled:NO];
    self.downloadButton .hidden=YES;
    self.downloadProgressSlider.hidden=NO;
    self.progressLabel.hidden=NO;
    downloadManager=[[MansionDownloadManager alloc] init];
    downloadManager.delegate=self;
 //..  [downloadManager downloadFileForAddress:@"ftp://actioncharts%5Cactioncharts:Sarla2013@waws-prod-bn1-001.ftp.azurewebsites.windows.net/Elms.zip" forMansion:@"Elms"];
    if(sender){
        downloadManager.shouldJump=NO;
    }
    else{
        downloadManager.shouldJump=YES;
    }
    
    //https://actiontour.blob.core.windows.net/newportmansions/Elms.zip
    [downloadManager downloadFileForAddress:@"https://actiontour.blob.core.windows.net/newportmansions/Elms.zip" forMansion:@"Elms"];
    //  [downloadManager downloadFileForAddress:@"ftp://actioncharts%5Cactioncharts:Sarla2013@waws-prod-bn1-001.ftp.azurewebsites.windows.net/Elms.zip" forMansion:@"Elms"];
}
-(IBAction)deleteMansionContent:(UIButton*)sender{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\nDo you want to delete the tour?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=19;
    [alert show];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==19){
   if(buttonIndex==1)
       [self deleteTheDownloadedPackage];
    }
    
    else{
        if(buttonIndex==1)
        [self downloadMansionData:nil];
    }
}

-(void)deleteTheDownloadedPackage{
    NSFileManager *manager=[NSFileManager defaultManager];
    NSString *path=[DocumentDirectory stringByAppendingPathComponent:@"Elms"];
    NSError *error=nil;
    if([self isContentPresentForMansion:self.mansion.mansionName withFileManager:manager]){
        [manager removeItemAtPath:path error:&error];
    //..    self.downloadButton.layer.cornerRadius=5.0;
    //..    self.downloadButton.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
     //..   self.downloadButton.layer.borderWidth=1.0;
      //..  [self.downloadButton setBackgroundImage:[UIImage imageNamed:@"DownloadNow.png"] forState:UIControlStateNormal];
        [self.progressLabel setText:@"0%"];
        self.progressLabel.hidden=YES;
        [self.downloadButton setHidden:NO];
        [self.downloadProgressSlider setProgress:0.0 animated:YES];
        [self.downloadProgressSlider setHidden:YES];
        [self.downloadButton setTitle:@"Download Now" forState:UIControlStateNormal];
        [self.downloadButton setUserInteractionEnabled:YES];
        CGRect dlButtonRect =self.downloadButton.frame;
      //..  self.downloadButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 74, 25);
        self.deleteContentButton.hidden=YES;
        
        [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"DownloadNow.png"] forState:UIControlStateNormal];
        [self.downloadMansionDataButton setTitle:@"75MB" forState:UIControlStateNormal];
        CGRect ButtonRect =self.downloadMansionDataButton.frame;
        self.downloadMansionDataButton.frame=CGRectMake(ButtonRect.origin.x, ButtonRect.origin.y, 74, 25);
        
    }
    

}



-(BOOL)isContentPresentForMansion:(NSString*)mansion withFileManager:(NSFileManager*)fileManager{
    
   //.. NSString *bundlePath=[[NSBundle mainBundle] pathForResource:@"Elms" ofType:nil];
    NSString *path=[DocumentDirectory stringByAppendingPathComponent:mansion];
    
  if([fileManager fileExistsAtPath:path])
      return YES;
    else
        return NO;
    
}


#pragma DownloadManager Delegate

-(void)downloadManagerDidRecieveResponseWithContentLength:(long)contentLength{
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    NSLog(@"Download Began");
}
-(void)downloadManagerDidRecieveResponseWithPercent:(float)percent{
     NSLog(@"Percent began");
    int displayPercent = (percent/1)*100;
     self.downloadProgressSlider.hidden=NO;
    self.progressLabel.hidden=NO;
    self.downloadButton.hidden=YES;
    [self.downloadProgressSlider setProgress:percent animated:YES];
    [self.progressLabel setText:[NSString stringWithFormat:@"%d%%",displayPercent]];
}
-(void)downloadManagerDidFinishLoading{
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    NSLog(@"Download Finished");
  //..  [self.downloadButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
    [self.downloadButton setTitle:@"Download Complete" forState:UIControlStateNormal];
    self.downloadButton.layer.cornerRadius=0.0;
    self.downloadButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
    [self.progressLabel setText:@"0%"];
    self.progressLabel.hidden=YES;
    [self.downloadButton setHidden:NO];
    [self.downloadProgressSlider setProgress:0.0 animated:YES];
    [self.downloadProgressSlider setHidden:YES];
    //..[self.downloadButton setTitle:@"" forState:UIControlStateNormal];
    [self.downloadButton setUserInteractionEnabled:NO];
    CGRect dlButtonRect =self.downloadButton.frame;
  //..  self.downloadButton.frame=CGRectMake(dlButtonRect.origin.x, dlButtonRect.origin.y, 32, 25);
    self.deleteContentButton.hidden=NO;
    
    [self.downloadMansionDataButton setBackgroundImage:[UIImage imageNamed:@"CntentDownloaded.png"] forState:UIControlStateNormal];
    [self.downloadMansionDataButton setTitle:@"" forState:UIControlStateNormal];
    CGRect ButtonRect =self.downloadMansionDataButton.frame;
    self.downloadMansionDataButton.frame=CGRectMake(ButtonRect.origin.x, ButtonRect.origin.y, 52, 40);
    
    [self.mansionNameButon setUserInteractionEnabled:YES];
    
 
    
 //..   if([self.mansionArray indexOfObject:currentMansion]==0 && [self.mansionArray containsObject:currentMansion])
    if(downloadManager.shouldJump)
    {
    
        // remove view and show the tour
        downloadManager=nil;
        [self stopTheDownload:nil];// remove view
    UIButton *tButton =[UIButton buttonWithType:UIButtonTypeCustom];
    tButton.tag=2;
    [self loadScreenForDetail:tButton];
    }
    
    
}
-(void)downloadManagerDidFailWithError:(NSError*)error{
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"Sorry! Download Failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [self.progressLabel setText:@"0%"];
    [self.downloadButton setHidden:NO];
    [self.downloadButton setUserInteractionEnabled:YES];
    [self.mansionNameButon setUserInteractionEnabled:YES];
    [self.downloadProgressSlider setProgress:0.0 animated:YES];
    [self.downloadProgressSlider setHidden:YES];
    [self.progressLabel setHidden:YES];
    [self.deleteContentButton setHidden:YES];
}

-(IBAction)stopTheDownload:(UIButton*)sender{
    if(downloadManager){
        [downloadManager stopTheDownload];
        downloadManager=nil;
    }
    
    [self.progressLabel setText:@"0%"];
    [self.downloadButton setHidden:NO];
    [self.downloadButton setUserInteractionEnabled:YES];
    [self.mansionNameButon setUserInteractionEnabled:YES];
    [self.downloadProgressSlider setProgress:0.0 animated:YES];
    [self.downloadProgressSlider setHidden:YES];
    [self.progressLabel setHidden:YES];
    [self.deleteContentButton setHidden:YES];
    
    [self removeView:nil];
}

#pragma Audio Tour Methods

// Audio SLider Setup and Progress
- (IBAction)slide {
   /*   [UIView animateWithDuration:1.0 animations:^{
     self.customPlayer.currentTime = audioProgressSlider.value;
    }];
    */
    
    [audioTimer invalidate];
   
   
}

- (IBAction)currentTimeSliderTouchUpInside:(id)sender
{

    UISlider *slider = (UISlider*)sender;
    NSLog(@"the player time is %f",slider.value);
  //  [UIView animateWithDuration:1.0 animations:^{
    self.customPlayer.currentTime = slider.value;//audioProgressSlider.value;
      //  [self.customPlayer play];
  //  }];
    audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];


}

/*-(IBAction)sliderTouchedOutside:(id)sender{
    UISlider *slider = (UISlider*)sender;
    NSLog(@"ouside");
    [self stopAudioPlayer];
    audioProgressSlider.value=0.0;
    [audioTimer invalidate];
} */

- (void)updateTime:(NSTimer *)timer {
  //..  [audioProgressSlider setValue: _player.player.currentTime animated:YES];
       [UIView animateWithDuration:1.0 animations:^{
        [audioProgressSlider setValue:self.customPlayer.currentTime  animated:YES];
    }];
  
    [audioDetailDict setObject:[NSNumber numberWithFloat:self.customPlayer.currentTime] forKey:presentRoom];
    
    if(self.customPlayer.currentTime == self.customPlayer.duration){
        [audioDetailDict setObject:[NSNumber numberWithFloat:0.0] forKey:presentRoom];
    }
    
}

-(IBAction)playTheAudio:(UIButton*)sender {
    
    if(subStopVC){
        [subStopVC pauseThePlayer];
    } // later
   
    /*
    if(!_player){
    NSString *audioPath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Elms/%@/%@/Audio/eng_%@.mp3",self.floorLabel.text,presentRoom,presentRoom]];
  _player=[[AudioPlayer alloc]init];

    [_player createAudioPlayerWithPath:audioPath];
        [self.playPauseButton setSelected:YES];
        audioProgressSlider.maximumValue = [_player.player duration];
        audioProgressSlider.value = 0.0;
        
       // set last played time
        id cachedTime = [audioDetailDict valueForKey:presentRoom] ;
        if(cachedTime!=nil){
            _player.player.currentTime = [[audioDetailDict valueForKey:presentRoom] floatValue];
            audioProgressSlider.value = [[audioDetailDict valueForKey:presentRoom] floatValue];
        }
        else
        {
            _player.player.currentTime = 0.0;
        }
        // end of last played time
        
        audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    }
    else if(_player.player.isPlaying){
        
    [self.playPauseButton setSelected:NO];
    [_player pauseAudioPlayer];
     
    }
    else{
        [self.playPauseButton setSelected:YES];
        [_player.player play];
    }
    
     */
    if(playerStopped){
        self.customPlayer=nil;
        playerStopped=NO;
    }
    NSError* error = nil;
    if(!self.customPlayer){
        NSString *audioPath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@/Audio/eng_%@.ogg",currentMansion.shortName,self.floorLabel.text,presentRoom,presentRoom]];
     //   NSString *audioPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"ogg"];
        NSURL *oggUrl = [NSURL fileURLWithPath:audioPath];
    //    NSURL* oggUrl = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"ogg"];

        IDZOggVorbisFileDecoder* decoder = [[IDZOggVorbisFileDecoder alloc] initWithContentsOfURL:oggUrl error:&error];
        NSLog(@"Ogg Vorbis file duration is %g", decoder.duration);
        self.customPlayer = [[IDZAQAudioPlayer alloc] initWithDecoder:decoder error:nil];
        self.customPlayer.delegate=self;
        [self.customPlayer prepareToPlay];
        [self.customPlayer play];
        
         NSLog(@"Player duration is %f", self.customPlayer.duration);
      //  _player=[[AudioPlayer alloc]init];
        
     //   [_player createAudioPlayerWithPath:audioPath];
        [self.playPauseButton setSelected:YES];
        audioProgressSlider.maximumValue = self.customPlayer.duration;
        audioProgressSlider.value = 0.0;
        
        // set last played time
        id cachedTime = [audioDetailDict valueForKey:presentRoom] ;
        if(cachedTime!=nil){
            self.customPlayer.currentTime = [[audioDetailDict valueForKey:presentRoom] floatValue];
            audioProgressSlider.value = [[audioDetailDict valueForKey:presentRoom] floatValue];
        }
        else
        {
           self.customPlayer.currentTime = 0.0;
        }
        // end of last played time
        
        audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES]; // timer changed to 0.1
    }
    else if(self.customPlayer.playing){
        
        [self.playPauseButton setSelected:NO];
        [self.customPlayer pause];
        
        
        
    }
    else{
        [self.playPauseButton setSelected:YES];
        [self.customPlayer play];
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
     /*  NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"ogg"];
    NSURL *oggUrl = [NSURL fileURLWithPath:path];
    IDZOggVorbisFileDecoder* decoder = [[IDZOggVorbisFileDecoder alloc] initWithContentsOfURL:oggUrl error:&error];
    NSLog(@"Ogg Vorbis file duration is %g", decoder.duration);
    _customPlayer = [[IDZAQAudioPlayer alloc] initWithDecoder:decoder error:nil];
   
    [_customPlayer prepareToPlay];
    [_customPlayer play]; */
}

// Notification of Audio Player
-(void)audioPlayerFinishedPlaying:(NSNotification*)noti{
  //  [self stopAudioPlayer];
   
   // audioProgressSlider.value=0.0;//..self.player.player.duration;//0.0;
  //..  [audioTimer invalidate];
    
}


#pragma mark - IDZAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(id<IDZAudioPlayer>)player successfully:(BOOL)flag
{
   //.. NSLog(@"%s successfully=%@", __PRETTY_FUNCTION__, flag ? @"YES"  : @"NO");
    playerStopped=YES;
// [self stopAudioPlayer];
   [player stop];
    
    [self.playPauseButton setSelected:NO];
    [UIView animateWithDuration:1.0 animations:^{
        [audioProgressSlider setValue:0.0 animated:YES];
    }];
    [audioTimer invalidate];
    audioTimer=nil;
    if(pageSwipe==NO){
        [audioDetailDict setObject:[NSNumber numberWithFloat:0.0] forKey:presentRoom];
    }
    if(gpsVC)
    {
      /*  [UIView animateWithDuration:0.5 animations:^{
            
            gpsVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
            [gpsVC reCentreMapToDefault];
            
        }completion:^(BOOL finished) {
            
        }];*/
      //..  [[NSNotificationCenter defaultCenter]postNotificationName:@"LoadGPSMap" object:presentRoom];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadGPSMap" object:nil userInfo:@{@"PresentRoom":presentRoom}];
        
    }
   
    
    //self.customPlayer=nil;
}

- (void)audioPlayerDecodeErrorDidOccur:(id<IDZAudioPlayer>)player error:(NSError *)error
{
    NSLog(@"%s error=%@", __PRETTY_FUNCTION__, error);
   
}





-(IBAction)showSubStopScreen:(UIButton*)sender{
 //..   [self stopAudioPlayer];
  //..  [self pauseThePlayer];
    [self sendDataForTrackingForAction:@"SubstopScreen" label:presentRoom];
    
    
   CGRect frame=self.tourDetailView.frame;
    frame.origin.y=CGRectGetMinY(self.view.frame)-CGRectGetHeight(self.tourDetailView.frame);
    if(!subStopVC){
        if(isIphone){
            subStopVC=[[SubStopsViewController alloc]initWithNibName:@"SubStopsViewController" bundle:nil];
        }
        else{
            subStopVC=[[SubStopsViewController alloc]initWithNibName:@"SubStopsViewController-iPad" bundle:nil];
        }
    
    subStopVC.subStopsArray=subStops;
    subStopVC.presentRoom=presentRoom;
    subStopVC.floorName=self.floorLabel.text;
    subStopVC.mansionName=currentMansion.shortName;
    subStopVC.subStopsNamesArray=subStopsName;
    subStopVC.subStopsImagePathArray=subStopsImagePath;
          subStopVC.dbManager=dbManager;
        subStopVC.customerDetails=customerDetails;
  //..  CGRect frame=self.tourDetailView.frame;
  //..  frame.origin.y=CGRectGetMinY(self.view.frame)-CGRectGetHeight(self.tourDetailView.frame);
    subStopVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.tourDetailView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    [self.mainView addSubview:subStopVC.view];
        [self.mainView bringSubviewToFront:subStopVC.view];
    }
    
    [UIView animateWithDuration:2.0 animations:^{
          self.tourDetailView.frame=frame;
         subStopVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.tourDetailView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
      
        
        
    } completion:^(BOOL finished)
     {
         if(finished)
         {
             [subStopVC.tableView flashScrollIndicators];
         }
     }];
    
    [UIView commitAnimations];
   
   
    
}

-(void)subStopRemoved:(NSNotification*)noti{
     CGRect frame=self.tourDetailView.frame;
    frame.origin.y=CGRectGetMaxY(self.topView.frame);
  [UIView animateWithDuration:2.0 animations:^{
      self.tourDetailView.frame=frame;
      subStopVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.tourDetailView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
     
  } completion:^(BOOL finished) {
 //..     [subStopVC.view removeFromSuperview];
      
  }];
    
    [UIView commitAnimations];

}

-(IBAction)loadNextRoomToPlayAudio:(id)sender
{
    pageSwipe=YES;
    self.roomNameFakeBotton.enabled=false;
    if(subStopVC){
        [subStopVC stopAudioPlayer];
        [subStopVC.view removeFromSuperview];
        subStopVC=nil;
    }
    [self stopAudioPlayer];
    int presentIdexOfRoom;
    self.floorLabel.hidden=NO;
    
    
    if([self.floorRoomsArray containsObject:presentRoom] && [self.floorRoomsArray lastObject]!=presentRoom){
        presentIdexOfRoom=[self.floorRoomsArray indexOfObject:presentRoom];
    }
    else{
        presentIdexOfRoom=-1;
    }

    
    
    // next button pressed
        if(presentIdexOfRoom==-1){
            
            [self.previousRoom setTitle:presentRoom forState:UIControlStateNormal];
            int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
            if(indexOfPresentFloor==[allFloors count]-1){
                self.nextRoom.hidden=YES;
                self.greenPlayButton.hidden=YES;
                rightArrow.hidden=YES;
            }
            else{
                NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
                allRooms=[self sortTheDatasource:allRooms];
                self.floorRoomsArray=[allRooms copy];
                
                presentRoom=[self.floorRoomsArray objectAtIndex:0];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
                [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:1] forState:UIControlStateNormal];
                self.floorLabel.text = [dbManager getFloorNameForRoom:presentRoom];
            }
        }
        
        else{
            
            if(self.floorRoomsArray.count-1>=presentIdexOfRoom+2){
                [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+2] forState:UIControlStateNormal];
                [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
                self.previousRoom.hidden=NO;
                leftArrow.hidden=NO;
                presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+1];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
            }
            else{
                [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
                presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+1];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
                // Navigate to other room
                int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
                //        if([[self.nextRoom currentTitle] caseInsensitiveCompare:@"Donate"] == NSOrderedSame ){
                //            NSLog(@"nxt donate");
                //       sender.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                //        sender.contentEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
                //        }
                if(indexOfPresentFloor==[allFloors count]-1){
                    // End of floor
                    self.roomNameFakeBotton.enabled=true;

                    self.floorLabel.hidden=YES; // For Donate Page
                    self.nextRoom.hidden=YES;
                    self.greenPlayButton.hidden=YES;
                    [self.appFeedbackButton setHidden:NO];
                    [self.appFeedbackArrowImg setHidden:NO];

                    rightArrow.hidden=YES;
                    
                }
                else{
                    NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
                    allRooms=[self sortTheDatasource:allRooms];
                    [self.nextRoom setTitle:[allRooms objectAtIndex:0] forState:UIControlStateNormal];
                }
                // End of next floor logic
                //..self.nextRoom.hidden=YES; // Earlier there
            }
        }// else of -1
        
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
    [self setTourImageForPath:roomPath];
    
    NSDictionary *rooms=[self.mansion.floorDict objectForKey:self.floorLabel.text];
    NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
    subStops = [roomDetails objectForKey:@"SubStops"];
    if([[roomDetails objectForKey:@"SubStops"] count]>0)
        subStops= [self getSubStopArrayForRoom];
    [self prepareSubStopsNameForArray:subStops];
    [self prepareImagePathForSubStops:subStops];
    //..  [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
 //..edit    [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
     [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    if([subStops count]==1)
        //..  [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
    //..edit     [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
         [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    //..   self.substopButton.userInteractionEnabled=YES;
    self.substopButton.hidden=NO;
    self.semiUp.hidden=NO;
    if([subStops count]==0){
        //..   self.substopButton.userInteractionEnabled=NO;
        self.substopButton.hidden=YES;
        self.semiUp.hidden=YES;
    }
    
    
    [self setCachedValueForSlider];
    
    pageSwipe=NO;
 
    [self sendDataForTrackingForAction:@"TourScreen" label:self.roomLable.text];
    // play the audio automatically
    
    [self playTheAudio:nil];
    
}




-(IBAction)loadRooms:(UIButton*)sender{
    pageSwipe=YES;
    self.roomNameFakeBotton.enabled=false;
    [self.appFeedbackButton setHidden:YES];//mandar
    [self.appFeedbackArrowImg setHidden:YES];
    [self.nextRoom setHidden:NO];
    [rightArrow setHidden:NO];
    [leftArrow setHidden:NO];
    [self.previousRoom setHidden:NO];
    [self.greenPlayButton setHidden:NO];
    if([customerDetails.isAutoPlay caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
        self.greenPlayButton.hidden=NO;
    }
    
    else{
        self.greenPlayButton.hidden=YES;
    }

    
    
    if(subStopVC){
        [subStopVC stopAudioPlayer];
        [subStopVC.view removeFromSuperview];
        subStopVC=nil;
    }
  //..  [self stopAudioPlayer];
    int presentIdexOfRoom;
    self.floorLabel.hidden=NO;
    
    
//..    if([self.floorRoomsArray containsObject:presentRoom] && [self.floorRoomsArray lastObject]!=presentRoom){
      if([self.floorRoomsArray containsObject:presentRoom] && ![[self.floorRoomsArray lastObject] isEqualToString:presentRoom]){
    presentIdexOfRoom=[self.floorRoomsArray indexOfObject:presentRoom];
    }
    else{
        presentIdexOfRoom=-1;
    }
    
    if(sender.tag==2){// next button pressed
        if(presentIdexOfRoom==-1){
            
            [self.previousRoom setTitle:presentRoom forState:UIControlStateNormal];
            int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
            if(indexOfPresentFloor==[allFloors count]-1){
                self.nextRoom.hidden=YES;
                self.greenPlayButton.hidden= YES;
                rightArrow.hidden=YES;
            }
            else{
                NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
                allRooms=[self sortTheDatasource:allRooms];
                self.floorRoomsArray=[allRooms copy];
                
                presentRoom=[self.floorRoomsArray objectAtIndex:0];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
                self.floorLabel.text = [dbManager getFloorNameForRoom:presentRoom];
                int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
                // Below newly added if-else logic
                if([self.floorRoomsArray count]==1 && (indexOfPresentFloor == [allFloors count]-1)){
                    
                    // End of floor
                    self.roomNameFakeBotton.enabled=true;
                    
                    //.. self.floorLabel.hidden=YES; // For Donate Page
                    self.nextRoom.hidden=YES;
                    self.greenPlayButton.hidden= YES;
                    [self.appFeedbackButton setHidden:NO];
                    [self.appFeedbackArrowImg setHidden:NO];
                    rightArrow.hidden=YES;
                    
                }
                
                
                
                else     if([self.floorRoomsArray count]==1){
                    
                    NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
                    allRooms=[self sortTheDatasource:allRooms];
                    [self.nextRoom setTitle:[allRooms objectAtIndex:0] forState:UIControlStateNormal];
                }
                else{
                    
                    [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:1] forState:UIControlStateNormal];
                    
                }
            }
        }
        
        else{
            
            if(self.floorRoomsArray.count-1>=presentIdexOfRoom+2){
                [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+2] forState:UIControlStateNormal];
                [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
                self.previousRoom.hidden=NO;
                leftArrow.hidden=NO;
                presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+1];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
            }
            else{
                [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
                presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+1];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
                // Navigate to other room
                int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
                //        if([[self.nextRoom currentTitle] caseInsensitiveCompare:@"Donate"] == NSOrderedSame ){
                //            NSLog(@"nxt donate");
                //       sender.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                //        sender.contentEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
                //        }
                if(indexOfPresentFloor==[allFloors count]-1){
                    // End of floor
                    self.roomNameFakeBotton.enabled=true;
                    
                    self.floorLabel.hidden=YES; // For Donate Page
                    self.nextRoom.hidden=YES;
                    self.greenPlayButton.hidden= YES;
                    [self.appFeedbackButton setHidden:NO];
                    [self.appFeedbackArrowImg setHidden:NO];
                    rightArrow.hidden=YES;
                    
                }
                else{
                    NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
                    allRooms=[self sortTheDatasource:allRooms];
                    [self.nextRoom setTitle:[allRooms objectAtIndex:0] forState:UIControlStateNormal];
                }
                // End of next floor logic
                //..self.nextRoom.hidden=YES; // Earlier there
            }
        }// else of -1
        
        
    }
    
    else{
        
    //..    if([self.floorRoomsArray containsObject:presentRoom] && [self.floorRoomsArray firstObject]!=presentRoom){
           if([self.floorRoomsArray containsObject:presentRoom] && ![[self.floorRoomsArray firstObject] isEqualToString:presentRoom]){
        presentIdexOfRoom=[self.floorRoomsArray indexOfObject:presentRoom];
        }
        else{
            presentIdexOfRoom=-1;
        }
        
        
        
        if(presentIdexOfRoom==-1){
            
            [self.nextRoom setTitle:presentRoom forState:UIControlStateNormal];
            int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
            if(indexOfPresentFloor==0){
                // finished back
            }
            else{
                NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor-1]] allKeys];
                allRooms=[self sortTheDatasource:allRooms];
                self.floorRoomsArray=[allRooms copy];
                
                presentRoom=[self.floorRoomsArray lastObject];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
                self.floorLabel.text = [dbManager getFloorNameForRoom:presentRoom];
                // Below if-else logic added later
                
                
                if([self.floorRoomsArray count]==1){
                    int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
                    
                    // if else written post palithana
                    if(indexOfPresentFloor==0){
                        [self.previousRoom setHidden:YES];
                        leftArrow.hidden=YES;
                        
                    }
                    
                    else{
                        NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor-1]] allKeys];
                        allRooms=[self sortTheDatasource:allRooms];
                        //..  self.floorRoomsArray=[allRooms copy];
                        [self.previousRoom setTitle:[allRooms lastObject] forState:UIControlStateNormal];
                    }
                }
                else{
                    
                    [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:[self.floorRoomsArray count]-2] forState:UIControlStateNormal];
                }
            }
            
            
            
        }
        
        
        else{
            
            if(presentIdexOfRoom>1){
                [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom-2] forState:UIControlStateNormal];
                [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
                self.nextRoom.hidden=NO;
                self.greenPlayButton.hidden=NO;
               
                if([customerDetails.isAutoPlay caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
                    
                }
                
                else{
                    self.greenPlayButton.hidden=YES;
                }
                
                
                rightArrow.hidden=NO;
                presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom-1];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
            }
            else{
                [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
                presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom-1];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
                //..  self.previousRoom.hidden=YES;
                
                
                // Navigate to other room
                int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
                if(indexOfPresentFloor==0){
                    // End of floor
                    self.previousRoom.hidden=YES;
                    leftArrow.hidden=YES;
                }
                else{
                    //.. NSArray *allRooms=[[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys];
                    NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor-1]] allKeys];
                    allRooms=[self sortTheDatasource:allRooms];
                    //..  self.floorRoomsArray=[allRooms copy];
                    [self.previousRoom setTitle:[allRooms lastObject] forState:UIControlStateNormal];
                }
                // End of navigate to other
                
            }
            
        }
        
    }
    
    [self stopAudioPlayer];
    
    
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
    [self setTourImageForPath:roomPath];
    
    NSDictionary *rooms=[self.mansion.floorDict objectForKey:self.floorLabel.text];
    NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
    subStops = [roomDetails objectForKey:@"SubStops"];
    if([[roomDetails objectForKey:@"SubStops"] count]>0)
        subStops= [self getSubStopArrayForRoom];
    [self prepareSubStopsNameForArray:subStops];
    [self prepareImagePathForSubStops:subStops];
    //..  [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
//..edit     [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
     [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    if([subStops count]==1)
        //..  [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
 //..edit        [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
         [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
    //..   self.substopButton.userInteractionEnabled=YES;
    self.substopButton.hidden=NO;
    self.semiUp.hidden=NO;
    if([subStops count]==0){
        //..   self.substopButton.userInteractionEnabled=NO;
        self.substopButton.hidden=YES;
        self.semiUp.hidden=YES;
    }
    
    
    [self setCachedValueForSlider];
    
    pageSwipe=NO;
    
    [self sendDataForTrackingForAction:@"TourView" label:self.roomLable.text];
    
    if([[customerDetails isAutoPlay] caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
        [self playTheAudio:nil];
    }
    
    
    
 /*   pageSwipe=YES;
    self.roomNameFakeBotton.enabled=false;
    [self.appFeedbackButton setHidden:YES];//mandar
    [self.appFeedbackArrowImg setHidden:YES];
    if(subStopVC){
      [subStopVC stopAudioPlayer];
    [subStopVC.view removeFromSuperview];
        subStopVC=nil;
    }
    [self stopAudioPlayer];
    int presentIdexOfRoom;
    self.floorLabel.hidden=NO;
    
    
    if([self.floorRoomsArray containsObject:presentRoom] && [self.floorRoomsArray lastObject]!=presentRoom){
     presentIdexOfRoom=[self.floorRoomsArray indexOfObject:presentRoom];
    }
    else{
        presentIdexOfRoom=-1;
    }
    
    if(sender.tag==2){// next button pressed
        if(presentIdexOfRoom==-1){
            
            [self.previousRoom setTitle:presentRoom forState:UIControlStateNormal];
            int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
            if(indexOfPresentFloor==[allFloors count]-1){
                self.nextRoom.hidden=YES;
                 self.greenPlayButton.hidden= YES;
                rightArrow.hidden=YES;
            }
            else{
            NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
            allRooms=[self sortTheDatasource:allRooms];
            self.floorRoomsArray=[allRooms copy];
            
            presentRoom=[self.floorRoomsArray objectAtIndex:0];
            self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
            [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:1] forState:UIControlStateNormal];
            self.floorLabel.text = [dbManager getFloorNameForRoom:presentRoom];
            }
        }
        
        else{
        
    if(self.floorRoomsArray.count-1>=presentIdexOfRoom+2){
    [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+2] forState:UIControlStateNormal];
        [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
        self.previousRoom.hidden=NO;
        leftArrow.hidden=NO;
         presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+1];
        self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
    }
    else{
        [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
        presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom+1];
        self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
        // Navigate to other room
        int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
//        if([[self.nextRoom currentTitle] caseInsensitiveCompare:@"Donate"] == NSOrderedSame ){
//            NSLog(@"nxt donate");
//       sender.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        sender.contentEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
//        }
            if(indexOfPresentFloor==[allFloors count]-1){
            // End of floor
                self.roomNameFakeBotton.enabled=true;

            self.floorLabel.hidden=YES; // For Donate Page
            self.nextRoom.hidden=YES;
            self.greenPlayButton.hidden= YES;
            [self.appFeedbackButton setHidden:NO];
            [self.appFeedbackArrowImg setHidden:NO];
            rightArrow.hidden=YES;
            
                   }
        else{
            NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor+1]] allKeys];
            allRooms=[self sortTheDatasource:allRooms];
            [self.nextRoom setTitle:[allRooms objectAtIndex:0] forState:UIControlStateNormal];
        }
        // End of next floor logic
        //..self.nextRoom.hidden=YES; // Earlier there
    }
    }// else of -1
    
        
    }
        
    else{
        
        if([self.floorRoomsArray containsObject:presentRoom] && [self.floorRoomsArray firstObject]!=presentRoom){
            presentIdexOfRoom=[self.floorRoomsArray indexOfObject:presentRoom];
        }
        else{
            presentIdexOfRoom=-1;
        }

        
        
        if(presentIdexOfRoom==-1){
            
            [self.nextRoom setTitle:presentRoom forState:UIControlStateNormal];
            int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
            if(indexOfPresentFloor==0){
                // finished back
            }
            else{
                NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor-1]] allKeys];
                allRooms=[self sortTheDatasource:allRooms];
                self.floorRoomsArray=[allRooms copy];
                
                presentRoom=[self.floorRoomsArray lastObject];
                self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];
                [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:[self.floorRoomsArray count]-2] forState:UIControlStateNormal];
                self.floorLabel.text = [dbManager getFloorNameForRoom:presentRoom];
            }
          
          
         
        }
        
        
        else{
            
        if(presentIdexOfRoom>1){
            [self.previousRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom-2] forState:UIControlStateNormal];
            [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
            self.nextRoom.hidden=NO;
            self.greenPlayButton.hidden=NO;
            rightArrow.hidden=NO;
            presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom-1];
            self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
        }
        else{
            [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:presentIdexOfRoom] forState:UIControlStateNormal];
             presentRoom=[self.floorRoomsArray objectAtIndex:presentIdexOfRoom-1];
            self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
          //..  self.previousRoom.hidden=YES;
          
          
            // Navigate to other room
            int indexOfPresentFloor=[allFloors indexOfObject:self.floorLabel.text];
            if(indexOfPresentFloor==0){
                // End of floor
                self.previousRoom.hidden=YES;
                leftArrow.hidden=YES;
            }
            else{
                //.. NSArray *allRooms=[[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys];
                NSArray *allRooms=[[self.mansion.floorDict objectForKey:[allFloors objectAtIndex:indexOfPresentFloor-1]] allKeys];
                allRooms=[self sortTheDatasource:allRooms];
              //..  self.floorRoomsArray=[allRooms copy];
                [self.previousRoom setTitle:[allRooms lastObject] forState:UIControlStateNormal];
            }
              // End of navigate to other
            
        }
        
    }
    
    }
     NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
    [self setTourImageForPath:roomPath];
   
    NSDictionary *rooms=[self.mansion.floorDict objectForKey:self.floorLabel.text];
    NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
    subStops = [roomDetails objectForKey:@"SubStops"];
    if([[roomDetails objectForKey:@"SubStops"] count]>0)
        subStops= [self getSubStopArrayForRoom];
    [self prepareSubStopsNameForArray:subStops];
    [self prepareImagePathForSubStops:subStops];
  //..  [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
    [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
    if([subStops count]==1)
      //..  [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
        [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
 //..   self.substopButton.userInteractionEnabled=YES;
    self.substopButton.hidden=NO;
    self.semiUp.hidden=NO;
    if([subStops count]==0){
     //..   self.substopButton.userInteractionEnabled=NO;
        self.substopButton.hidden=YES;
        self.semiUp.hidden=YES;
    }
    
    
    [self setCachedValueForSlider];
    
    pageSwipe=NO;
    
    [self sendDataForTrackingForAction:@"TourScreen" label:self.roomLable.text];
    
    */
}


-(void)setCachedValueForSlider{
   
    id cachedTime = [audioDetailDict valueForKey:presentRoom] ;
    if(cachedTime!=nil){
        audioProgressSlider.value = [[audioDetailDict valueForKey:presentRoom] floatValue];
    }
    else
    {
        audioProgressSlider.value = 0.0;
    }
    
    
    int indexOfPresentRoom =[self.allFloorRoomArray indexOfObject:presentRoom];
    if(indexOfPresentRoom<=[self.allFloorRoomArray count]-3 && indexOfPresentRoom>=2){
        NSString *previousRoomToDelete =[self.allFloorRoomArray objectAtIndex:indexOfPresentRoom-2];
        NSString *nextRoomToDelete = [self.allFloorRoomArray objectAtIndex:indexOfPresentRoom+2];
        
        [audioDetailDict removeObjectForKey:previousRoomToDelete];
        [audioDetailDict removeObjectForKey:nextRoomToDelete];
    }
    
    
   /* NSString *previousRoomToDelete =[self.allFloorRoomArray objectAtIndex:<#(NSUInteger)#>]
    
    NSString *previousRoomToDelete = [self.allFloorRoomArray objectAtIndex:[self.allFloorRoomArray indexOfObject:presentRoom]-2];
    NSString *nextRoomToDelete = [self.allFloorRoomArray objectAtIndex:[self.allFloorRoomArray indexOfObject:presentRoom]+2];
    if(previousRoomToDelete!=nil){
        [audioDetailDict removeObjectForKey:previousRoomToDelete];
    }
    
    if(nextRoomToDelete!=nil){
        [audioDetailDict removeObjectForKey:nextRoomToDelete];
    } */
    
}


-(void)performOperationOnUserDefaultWithDirection:(NSString*)direction{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:audioDetailDict forKey:@"SliderDetails"];
    
    
}



-(void)stopAudioPlayer{
   
    [audioTimer invalidate];
    [self.playPauseButton setSelected:NO];
    [self.customPlayer stop];
    self.customPlayer=nil;
    [UIView animateWithDuration:1.0 animations:^{
        [audioProgressSlider setValue:0.0 animated:YES];
    }];
    
    
    
}

-(void)pauseThePlayer{
    [self.playPauseButton setSelected:NO];
    [self.customPlayer pause];
}

-(IBAction)showWebActionSheet:(id)sender
{
    [UIActionSheet showFromRect:self.webActionSheetButton.frame inView:self.view animated:YES withTitle:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@[@"Open in Safari"]  tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 0:{
                
              //..   NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",start.latitude, start.longitude, destination.latitude, destination.longitude];
      // NSString *add=  [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", currentMansionAddress ];
                
                NSString *formatted =[currentMansionAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                
                NSString *addressString = [NSString stringWithFormat:@"%@%@",
                                           @"https://maps.google.com/?q=", formatted];
                
                //..NSString *add=[NSString stringWithFormat:@"https://maps.google.com/?q=%@",currentMansionAddress];

                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:addressString]];
                
            }
                break;
            default:
                break;
        }
        
    }];

}


-(IBAction)showActionSheet:(id)sender{
  /*  UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Transcript",
                            @"Share",
                            @"Mark as Favourite",
                             nil];
    popup.tag = 1;
    [popup showInView:self.view]; */
    
    
    
    
    
  /*  [UIActionSheet showInView:self.view withTitle:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@[@"Transcript",@"Share",@"Mark as Favourite"] tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        
        switch (buttonIndex) {
            case 0:
                [self showTranscript];
                break;
            case 1:
                [self showShareScreen];
                break;
            case 2:
                [self markAsFavourite];
                break;
            default:
                break;
        }
    }]; */
    
    
    [UIActionSheet showFromRect:self.actionSheetButton.frame inView:self.tourDetailView animated:YES withTitle:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@[@"Transcript",@"Share"]  tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 0:
                [self showTranscript];
                break;
            case 1:
            {
                [self setShareImageForTour];
              //..  [self showShareScreen];
               // [self shareText:Subject andImage:imageToShare andUrl:[NSURL URLWithString:NewportURL]];
                   [self performSelector:@selector(shareWithDelay) withObject:nil afterDelay:0.5];
                break;
            }
            case 2:
               //.. [self markAsFavourite];
                break;
            default:
                break;
        }

    }];
    
    
}


// Action Sheet Delegate
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
switch (buttonIndex) {
    case 0:
        [self showTranscript];
        
        break;
    case 1:
     //..   [self showShareScreen];
       [self shareText:Subject andImage:imageToShare andUrl:[NSURL URLWithString:NewportURL]];
     
        break;
    case 2:
        [self markAsFavourite];
        break;
    default:
        break;
}

    
}

-(void)shareWithDelay{
    [self sendDataForTrackingForAction:@"Share" label:presentRoom];
    [self shareText:customerDetails.tagline andImage:imageToShare andUrl:[NSURL URLWithString:customerDetails.website]];
}


-(void)showTranscript{
    
//..    NSMutableString *str=[[NSMutableString alloc] initWithString:[dbManager getTranscriptForRoom:presentRoom andTable:@"Main"]];
 //..   [str appendString:@"\n \nCopyright \u00A9 The Preservation Society of Newport County"];
 //..   [str appendString:[NSString stringWithFormat:@"\n \nCopyright \u00A9 %@",customerDetails.copywriteText]];
    
    NSMutableString *str = [dbManager getTranscriptForRoom:presentRoom andTable:@"Main"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
     self.transcriptTextView.attributedText = attributedString;
  //..  [self.transcriptTextView setFont:[UIFont fontWithName:@"Helvetica" size:40]];
    
    NSArray *subViews=[self.transcriptView subviews];
    for(UIView *view in subViews){
        if(view.tag != 19){
            [view removeFromSuperview];
        }
    }

    
    self.transcriptView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    self.transcriptInnerView.layer.cornerRadius=10.0;
    self.transcriptInnerView.layer.borderWidth=5.0;
    self.transcriptInnerView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    //..[UIColor colorWithRed:120.0 green:181.0 blue:226.0 alpha:1].CGColor;
 //..   self.transcriptTextView.text=str;
    [self.transcriptView addSubview:self.transcriptInnerView];
    
    self.transcriptInnerView.center=self.mainView.center;
    self.closeTranscriptButton.center=CGPointMake(CGRectGetMaxX(self.transcriptInnerView.frame), CGRectGetMinY(self.transcriptInnerView.frame));
    [self.transcriptView bringSubviewToFront:self.closeTranscriptButton];
    self.transcriptView.frame=self.mainView.frame;
    self.closeTranscriptButton.hidden=NO;
    [self.mainView addSubview:self.transcriptView];
}

-(void)showShareScreen{
 // UIView *nowShowingView=[self.transcriptView viewWithTag:<#(NSInteger)#>]
    NSArray *subViews=[self.transcriptView subviews];
    for(UIView *view in subViews){
        if(view.tag != 19){
            [view removeFromSuperview];
        }
    }
    
   
    self.transcriptView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    self.socialNetworkView.layer.cornerRadius=10.0;
    self.socialNetworkView.layer.borderWidth=5.0;
    self.socialNetworkView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    //..[UIColor colorWithRed:120.0 green:181.0 blue:226.0 alpha:1].CGColor;
    self.socialNetworkView.center=self.mainView.center;
     [self.transcriptView addSubview:self.socialNetworkView];
    self.closeTranscriptButton.center=CGPointMake(CGRectGetMaxX(self.socialNetworkView.frame), CGRectGetMinY(self.socialNetworkView.frame));
    [self.transcriptView bringSubviewToFront:self.closeTranscriptButton];
     self.transcriptView.frame=self.mainView.frame;
    [self.mainView addSubview:self.transcriptView];
    
}

-(void)showEvents{
    
    if(isIphone){
         eventsVC=[[EventsViewController alloc]initWithNibName:@"EventsViewController" bundle:nil];
    }
    else{
    eventsVC=[[EventsViewController alloc]initWithNibName:@"EventsViewController-iPad" bundle:nil];
    }
    
    
    [self.mainView addSubview:eventsVC.view];
     eventsVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));

}

-(void)showJoin{
    
    if(isIphone){
         joinVC=[[JoinViewController alloc]initWithNibName:@"JoinViewController-iPhone" bundle:nil];
    }
    else{
    joinVC=[[JoinViewController alloc]initWithNibName:@"JoinViewController-iPad" bundle:nil];
    }
    joinVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
    [self.view bringSubviewToFront:joinVC.view];
    [self.mainView addSubview:joinVC.view];
}

-(void)markAsFavourite{
    
}



-(IBAction)removeView:(UIButton*)sender{
    [self.transcriptView removeFromSuperview];
    [self resetAllTabButtons];
    self.aboutButton.selected=YES;
}

-(IBAction)loadDirectionScreen:(UIButton*)sender{
    
   /* if(![self connectedToInternet]){
        self.aboutButton.selected=YES;
        self.directionButton.selected=NO;
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\n No Internet Connection Available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    } */
   /* if(isIphone){
      directionVC=[[DirectionsViewController alloc]initWithNibName:@"DirectionsViewController-iPhone" bundle:nil];
    }
    
    else{
    directionVC=[[DirectionsViewController alloc]initWithNibName:@"DirectionsViewController-iPad" bundle:nil];
    }
    
    directionVC.safariButton=self.webActionSheetButton;
    directionVC.destinationString=currentMansionAddress;
    directionVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame)-CGRectGetHeight(self.bottomView.frame));
     [self.view bringSubviewToFront:directionVC.view];
     [self.mainView addSubview:directionVC.view];
    
    */
   //.. [self.view addSubview:directionVC.view];
    
    [self sendDataForTrackingForAction:@"Directions" label:@"MainStop"];
    NSString *formatted =[currentMansionAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *addressString = [NSString stringWithFormat:@"%@%@",
                               @"https://maps.google.com/?q=", formatted];
    // http://maps.apple.com/maps?saddr=Current%20Location&daddr=Your_Address
    
    //addressString= currentMansionAddress;//[cell.addressButton currentTitle];
    
    
    if(isIphone){
        newDirVC=[[NewDirectionViewController alloc]initWithNibName:@"NewDirectionViewController" bundle:nil];
    }
    else{
        newDirVC=[[NewDirectionViewController alloc]initWithNibName:@"NewDirectionViewController-iPad" bundle:nil];
    }
    
    newDirVC.urlString=addressString;
    //..currentMansionAddress=addressString;// added later
    self.webActionSheetButton.hidden=NO;
    // [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil];
    
    newDirVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.mainView addSubview:newDirVC.view];
    
}


// Sharing

-(void)sendEmailWIthAttachments:(id)attachment withMessage:(NSString*)messageString{
    
    UIImage *shareImage=(UIImage*)attachment;
    NSData *imageData=UIImagePNGRepresentation(imageToShare);
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:SubjectFeedback];
    if(shareImage!=nil){
        [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"mansion.png"];
        [controller setSubject:Subject];
        
        NSString *bodyText =@"<html>";
        bodyText = [bodyText stringByAppendingString:@"<head>"];
        bodyText = [bodyText stringByAppendingString:@"</head>"];
        bodyText = [bodyText stringByAppendingString:@"<body>"];
        bodyText = [bodyText stringByAppendingString:@"<a     href=\"http://www.newportmansions.org\">Newport Mansions!"];
        bodyText = [bodyText stringByAppendingString:@"</a>"];
        
        
        [controller setMessageBody:[NSString stringWithFormat:@"%@%@",messageString,bodyText] isHTML:YES];
        
    }
    else{
        [controller setToRecipients:[NSArray arrayWithObject:RecipientFeedback]];
        [controller setMessageBody:messageString isHTML:NO];
    }
    
  /*    NSString *bodyText =@"<html>";
    bodyText = [bodyText stringByAppendingString:@"<head>"];
    bodyText = [bodyText stringByAppendingString:@"</head>"];
    bodyText = [bodyText stringByAppendingString:@"<body>"];
    bodyText = [bodyText stringByAppendingString:@"<a     href=\"http://www.newportmansions.org\">Newport Mansions!"];
    bodyText = [bodyText stringByAppendingString:@"</a>"];
    
    
    [controller setMessageBody:bodyText isHTML:YES]; */
    
    if ([MFMailComposeViewController canSendMail]) {
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\nPlease Configure Your Mail" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    

}

-(void)setShareImageForTour{
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
    imageToShare=[self returnImageForPath:roomPath];
}


-(IBAction)shareLink:(UIButton*)sender {
    
    
    //NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:@"Elms"];
   // NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
 //   UIImage *imageToShare=[self returnImageForPath:roomPath];
    SLComposeViewController *socialPost;
   //.. if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
    
    if(sender.tag==3){
    /*    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"My Subject"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        
        if ([MFMailComposeViewController canSendMail]) {
           if (controller) [self presentViewController:controller animated:YES completion:nil];
        } else {
        
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport" message:@"Configure Your Mail" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
       */
        [self sendEmailWIthAttachments:imageToShare withMessage:GreatTimeMessage];
        
        
        return;
    }
    
    if(sender.tag==1){
         socialPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    }
    
    else if(sender.tag==2){
      socialPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    }
    
    
   
        [socialPost setInitialText:[NSString stringWithFormat:@"%@Newport Mansions",GreatTimeMessage]];
    [socialPost addURL:[NSURL URLWithString:@"www.NewportMansions.org"]];
    [socialPost addImage:imageToShare];
        
        [self presentViewController:socialPost animated:YES completion:nil];
    
    // set up a completion handler
    [socialPost setCompletionHandler:^(SLComposeViewControllerResult result) {
        switch (result) {
            case SLComposeViewControllerResultDone:
                // post was completed
                NSLog(@"Completed");
                break;
            case SLComposeViewControllerResultCancelled:
                // post was cancelled
                 NSLog(@"Not Completed");
                break;
            default:
                break;
        }
    }];
 /*  NSArray * activityItems = @[[NSString stringWithFormat:@"Check Internet Connection"], [NSURL URLWithString:@"http://www.google.com"]];
        UIActivityViewController * objVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    objVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    objVC.popoverPresentationController.sourceView = self.view;
    
    [self presentViewController:objVC animated:TRUE completion:nil];
    
    
    [objVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         NSLog(@"Activity = %@",activityType);
         NSLog(@"Completed Status = %d",completed);
         
         if (completed)
         {
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Posting was success" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }else
         {
             UIAlertView *objalert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Posting was not successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [objalert show];
             objalert = nil;
         }
     }]; */
 /*  NSDictionary *postParams =[[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"https://developers.facebook.com/ios", @"link",
     @"https://developers.facebook.com/attachment/iossdk_logo.png", @"picture",
     @"Facebook SDK for iOS", @"name",
     @"Build great social apps and get more installs.", @"caption",
     @"The Facebook SDK for iOS makes it easier and faster to develop Facebook integrated iOS apps.", @"description",
     nil];
    
   
    
    
    
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          if (error) {
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                              [alertView show];
                                          } else if (session.isOpen) {
                                              //run your user info request here
                                              [FBRequestConnection startWithGraphPath:@"me/feed"
                                                                           parameters:postParams
                                                                           HTTPMethod:@"POST"
                                                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                        NSLog(@"Error is %@",error);
                                                                        // Handle Success/Failure
                                                                        
                                                                    }];
                                          }
                                      }];
    }
    */
    
    
    

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}



// Facebook Post
/*
-(void)postOnFacebook
{
    if (FBSession.activeSession.isOpen)
        [self postOnWall];
    else
    {
        [FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObjects:@"publish_actions", nil]
                                           defaultAudience:FBSessionDefaultAudienceEveryone
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error)
         {
             if (error)
                 NSLog(@"Login failed");
             else if (FB_ISSESSIONOPENWITHSTATE(status))
                 [self postOnWall];
         }];
    };
}

- (void)postOnWall
{
    FBRequestConnection *newConnection = [[FBRequestConnection alloc] init];
    
    FBRequestHandler handler =
    ^(FBRequestConnection *connection, id result, NSError *error) {
        [self requestCompleted:connection forFbID:@"me" result:result error:error];
    };
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   @"pippo", @"message",
                                   nil];
    
    FBRequest *request=[[FBRequest alloc] initWithSession:FBSession.activeSession graphPath:@"me/feed" parameters:params HTTPMethod:@"POST"];
    [newConnection addRequest:request completionHandler:handler];
    [requestConnection cancel];
    requestConnection = newConnection;
    [newConnection start];
}
*/
-(void)setMapImage{
    NSFileManager *fileMngr=[NSFileManager defaultManager];
    NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
    NSString *mapPath=[NSString stringWithFormat:@"%@/%@/%@/Map",mansionPath,self.floorLabel.text,presentRoom];
    NSArray *imageArray=[fileMngr contentsOfDirectoryAtPath:mapPath error:nil];
    if(imageArray>0){
        self.floorMapImageView.image= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",mapPath,[imageArray objectAtIndex:0]]];
    }
    else{
        self.floorMapImageView.image=[UIImage imageNamed:@"MansionImage1.png"];
    }

}


-(IBAction)showSegmentedView:(UIButton*)sender{
    if(gpsVC){
        [self willShowGPSMap];
    }
    else{
        
    
    
     self.segmentView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
    [self.mainView addSubview:self.segmentView];
   
    if([segmentViewArray count]>0){
    
    SegmentViewDetails *svd = [segmentViewArray objectAtIndex:0];
    [self.segmentControl setSelectedSegmentIndex:0];
    NSString *methodToCall = svd.onClick;
    [self performSelector:NSSelectorFromString([NSString stringWithFormat:@"%@:",methodToCall]) withObject:svd];
    } 
     }
  //  [self stopAudioPlayer];
    
    
    [self sendDataForTrackingForAction:@"FloorMapScreen" label:self.floorLabel.text];
    
}

- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    [self.floorListView removeFromSuperview];
    [self.floorMapView removeFromSuperview];
    
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
    [self.mainView addGestureRecognizer:tapOnView];
    NSString *methodToCall = [[segmentViewArray objectAtIndex:selectedSegment] onClick];
    [self performSelector:NSSelectorFromString([NSString stringWithFormat:@"%@:",methodToCall]) withObject:[segmentViewArray objectAtIndex:selectedSegment]];
 /*   if (selectedSegment == 0) {
        [self.mainView addGestureRecognizer:tapOnView];
        //toggle the correct view to be visible
        self.floorMapView.frame=CGRectMake(0, CGRectGetMaxY(self.segmentTopView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.segmentView.frame)-CGRectGetHeight(self.segmentTopView.frame));
        [self.segmentView addSubview:self.floorMapView];
      
        [self setMapImage];
        [self sendDataForTrackingForAction:@"FloorMapScreen" label:self.floorLabel.text];
    }
    else{
        //toggle the correct view to be visible
        [self.mainView removeGestureRecognizer:tapOnView]; //remove for table view
        
        self.floorListView.frame=CGRectMake(0, CGRectGetMaxY(self.segmentTopView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.segmentView.frame)-CGRectGetHeight(self.segmentTopView.frame));
        [self.segmentView addSubview:self.floorListView];
        [self.floorListTableView reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.allFloorRoomArray indexOfObject:presentRoom] inSection:0];
        [self.floorListTableView scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];
        [self sendDataForTrackingForAction:@"FloorListScreen" label:self.floorLabel.text];
    } */
}

-(IBAction)removeSegmentedView:(id)sender{
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton]; }
    [self.mainView addGestureRecognizer:tapOnView];
    [self.mapScrollView setZoomScale:1.0];
    [self.segmentView removeFromSuperview];
   }

-(IBAction)donateToday:(UIButton*)sender{
 /*   webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    webViewCtrl.urlString=DonateLink;
    [self presentViewController:webViewCtrl animated:YES completion:nil];  */
    
    if(customerDetails.donateLink == nil || [customerDetails.donateLink isEqualToString:@""]){
        return;
    }
    
    [self sendDataForTrackingForAction:@"DonateNowScreen" label:@"Donate Now"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customerDetails.donateLink]];
    
}

-(IBAction)maximizeTheImage:(UIButton*)sender {

    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];
        return;
    }
 //   self.maximizedBaseView.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
  //  self.maximizedBaseView.layer.borderWidth=5.0f;
   self.maximizedView.backgroundColor=[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    self.maximizedImageView.image=imageToBeMaximized;
   
    
    self.maximizedView.frame=self.mainView.frame;
     self.maximizedCloseButton.center=CGPointMake(CGRectGetMaxX(self.maximizedBaseView.frame), CGRectGetMinY(self.maximizedBaseView.frame));
    self.maximizedCloseButton.hidden=YES; // added for done
    self.maximizedDoneButton.layer.borderWidth=2.0f;
    self.maximizedDoneButton.layer.cornerRadius=5.0;
    self.maximizedDoneButton.layer.borderColor=[UIColor whiteColor].CGColor;
    
    self.maximizedImageView.contentMode =UIViewContentModeScaleAspectFit;
    [self.mainView addSubview:self.maximizedView];
    
    

    
    
    UITapGestureRecognizer *taptoRemove=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resetZoomScale:)];
    taptoRemove.numberOfTapsRequired=2;
    [self.maximizedView addGestureRecognizer:taptoRemove];
    
    [self.view bringSubviewToFront:self.maximizedView];
 /*   if(isIphone){
    maxVC=[[MaximizedViewController alloc]initWithNibName:@"MaximizedViewController" bundle:nil];
    }
    
    else{
        maxVC=[[MaximizedViewController alloc]initWithNibName:@"MaximizedViewController-iPad" bundle:nil];
    }
        
    maxVC.imageToMaximize = imageToBeMaximized;
    
    [self presentViewController:maxVC animated:YES completion:nil]; */
}

-(void)resetZoomScale:(UIGestureRecognizer*)gesture{
    if(self.maximizedImageScrollView.zoomScale>1 ){
        [self.maximizedImageScrollView setZoomScale:1.0];
    }
    else{
        [self.maximizedImageScrollView setZoomScale:4.0];
    }
}

-(IBAction)removeMaximizedView:(id)sender{
    [self.maximizedImageScrollView setZoomScale:1];
    [self.maximizedView removeFromSuperview];
}

-(IBAction)removeDownloadOptionView:(UIButton*)sender{
    if(sender.tag==0){
        [self downloadMansionData:sender];
    }
    [self.downloadOptionView removeFromSuperview];
}


-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView.subviews objectAtIndex:0];
   //.. return self.maximizedImageView;
}


- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    //..[self sendDataForTrackingForAction:@"Share" label:@"MainStop"];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    activityController.preferredContentSize=CGSizeMake(CGRectGetWidth(self.mainView.frame), 200);
    
    [activityController setValue:customerDetails.tagline forKey:@"subject"];
    if(!isIphone) {
    //    activityController.popoverPresentationController.sourceRect = self.exteriorImage.frame;
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityController];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0,0)inView:self.mainView permittedArrowDirections:0 animated:YES];
    
  //      activityController.preferredContentSize=CGSizeMake(CGRectGetWidth(self.mainView.bounds), 200);
        
    }
    else{
    [self presentViewController:activityController animated:YES completion:nil];
        

    }
    
    
 
    
  }

- (BOOL) connectedToInternet
{
    NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/m"];
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    if (data)
        return true;
    else
        return false;
}

-(NSArray*)getSubStopArrayForRoom{
    NSString *subStopString = [dbManager getOrderOfSubStopForRoom:presentRoom];
    NSArray *items = [subStopString componentsSeparatedByString:@","];
    return items;
}

-(NSArray*)sortTheDatasource:(NSArray*)array{
    NSString *floorname = [dbManager getFloorNameForRoom:[array firstObject]];
    NSArray *sortedArray = [dbManager getRoomsForFloor:floorname];
    return sortedArray;

    
    /*NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray; */
}

-(IBAction)loadStores:(UIButton*)sender{
    [self sendDataForTrackingForAction:@"Stores" label:@"MainStop"];
    [self resetAllTabButtons];
    sender.selected=YES;
    if(isIphone){
    storesVC=[[StoresViewController alloc]initWithNibName:@"StoresViewController" bundle:nil];
    }
    else{
            storesVC=[[StoresViewController alloc]initWithNibName:@"StoresViewController-iPad" bundle:nil];
    }
    storesVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame)-CGRectGetHeight(self.bottomView.frame));
    [self.view bringSubviewToFront:storesVC.view];
    [self.mainView addSubview:storesVC.view];
    
}


- (void)centerScrollViewContents {
    CGSize boundsSize = self.maximizedImageScrollView.bounds.size;
     CGRect contentsFrame = self.maximizedImageView.frame;
             if (contentsFrame.size.width < boundsSize.width) {
                contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
            } else {
                    contentsFrame.origin.x = 0.0f;
                }

        
        if (contentsFrame.size.height < boundsSize.height) {
                contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
            } else {
                    contentsFrame.origin.y = 0.0f;
                }
        
        self.maximizedImageView.frame = contentsFrame;
}

-(BOOL)hasGoogleMap{
BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];

if (canHandle) {
    // Google maps installed
    return YES;
} else {
    // Use Apple maps?
    return NO;
}
 }

- (IBAction)showAppFeedback:(id)sender {
 //..   [self sendEmailWIthAttachments:nil withMessage:[NSString stringWithFormat:@"\n%@",AppFeedbackMessage] ];
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:feedbackMailDetails.subject];
    [controller setToRecipients:[NSArray arrayWithObject:feedbackMailDetails.recipient]];
    [controller setMessageBody:feedbackMailDetails.body isHTML:NO];
    if ([MFMailComposeViewController canSendMail]) {
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"\nPlease Configure Your Mail" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }

}

- (IBAction)showPrevious:(id)sender {
    [self resetAllTabButtons];
    [self.aboutButton setSelected:YES];
    NSInteger nextMansion = 0;
    if(self.currentPage == 0)
    {
        nextMansion = self.mansionArray.count-1;
    }
    else
    {
        nextMansion = self.currentPage - 1;
    }
    [self loadMansionOfIndex:nextMansion];
}

- (IBAction)showNext:(id)sender {
    [self resetAllTabButtons];
    [self.aboutButton setSelected:YES];
    NSInteger nextMansion = 0;
    if(self.currentPage == self.currentPage - 1)
    {
        nextMansion = 0;
    }
    else
    {
        nextMansion = self.currentPage + 1;
    }
    [self loadMansionOfIndex:nextMansion];
}

-(void)showLeft:(UISwipeGestureRecognizer*)gestureRecognizer{
 
  //  [self resetAllTabButtons];
  //  [self.aboutButton setSelected:YES];
//NSInteger nextMansion = 0;
    if(self.currentPage == self.mansionArray.count-1){
        return;
    }

    
    [self showNext:nil];
    
}

-(void)showRight:(UISwipeGestureRecognizer*)gestureRecognizer{
//[self resetAllTabButtons];
 //   [self.aboutButton setSelected:YES];
 //   NSInteger nextMansion = 0;
    
    if(self.currentPage == 0){
        return;
    }
    
    [self showPrevious:nil];
}


#pragma database selector

-(void)loadHomePage:(NSString*)browser{
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
    [self.mansionDetailsView removeFromSuperview];
    [self.tourDetailView removeFromSuperview];
    [gettingHereVC.view removeFromSuperview];
    [newDirVC.view removeFromSuperview];
    [self.segmentView removeFromSuperview];
    if(directionVC){
        [directionVC.view removeFromSuperview];
        
    }
    isExpandTouched=NO;
    indexOfExpandCell=nil;
    
    
}

-(void)loadVisitPage:(SideMenu*)sideMenu{
    [cellCollectionArray removeAllObjects];
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
    
    [self showAboutUs:nil];
}

-(void)loadEventsPage:(SideMenu*)sideMenu{
    [cellCollectionArray removeAllObjects];
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
    
    [self showEvents];
}

-(void) openLink:(id)sender {
    [cellCollectionArray removeAllObjects];
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
    
    if([sender isKindOfClass:[SideMenu class]]){
        SideMenu *sideMenu = (SideMenu*)sender;
    if([sideMenu.menuBrowser caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
        NSURL *url= [NSURL URLWithString:sideMenu.menuLink];
        [[UIApplication sharedApplication] openURL:url];
    }
    
    else{
        
        WebViewController *wController = [[WebViewController alloc]init];
        wController.urlString = sideMenu.menuLink;
        [self presentViewController:wController animated:YES completion:nil];
   
    }
    }
    
    else if ([sender isKindOfClass:[MansionButton class]]){
        MansionButton *mansionButton=(MansionButton*)sender;
        if([mansionButton.isSafari caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
            NSURL *url= [NSURL URLWithString:mansionButton.associatedLink];
            [[UIApplication sharedApplication] openURL:url];
        }
        
        else{
            WebViewController *wController = [[WebViewController alloc]init];
            wController.urlString = mansionButton.associatedLink;
            [self presentViewController:wController animated:YES completion:nil];

        }
    }
    
    else if ([sender isKindOfClass:[SegmentViewDetails class]]){
        SegmentViewDetails *svd = (SegmentViewDetails*)sender;
        if([svd.browser caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
            NSURL *url= [NSURL URLWithString:svd.link];
            [[UIApplication sharedApplication] openURL:url];
        }
        
        else{
         /*   WebViewController *wController;
            
            if(isIphone){
                wController=[[WebViewController alloc]init];
                
            }
            
            else{
                 wController=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
            } */
            
            UIWebView *wController =[[UIWebView alloc]init];
           
            NSURL *nsurl=[NSURL URLWithString:svd.link];
            NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
            [wController loadRequest:nsrequest];
            wController.frame=CGRectMake(0, CGRectGetMaxY(self.segmentTopView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.segmentView.frame)-CGRectGetHeight(self.segmentTopView.frame));
            [self.segmentView addSubview:wController];
        }
    }
    
}

-(void)sendFeedback:(SideMenu*)sideMenu{
    [cellCollectionArray removeAllObjects];
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];
    }
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:feedbackMailDetails.subject];
    [controller setToRecipients:[NSArray arrayWithObject:feedbackMailDetails.recipient]];
    [controller setMessageBody:feedbackMailDetails.body isHTML:NO];
    if ([MFMailComposeViewController canSendMail]) {
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"\nPlease Configure Your Mail" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }

    
  //..  [[[UIAlertView alloc]initWithTitle:@"Mail will be sent" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    
}

-(void)shareClicked:(id)data  {
    [cellCollectionArray removeAllObjects];
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
    if([data isKindOfClass:[SideMenu class]]){
        SideMenu *menu = (SideMenu*)data;
        [self shareText:@"" andImage:[UIImage imageNamed:@"Mansion.png"] andUrl:[NSURL URLWithString:menu.menuLink]];
    }
    
    else{
        
        [self shareText:customerDetails.tagline andImage:self.exteriorImage.image andUrl:[NSURL URLWithString:customerDetails.website]];
    }
    
    
    
}

-(void)manageDownloads: (id)anyObject  //(SideMenu*)sideMenu{
{
    
    [cellCollectionArray removeAllObjects];
    if(self.sideMenuPopUpButton.tag==1){
        [self showLeftMenu:self.sideMenuPopUpButton];}
  //..  [self downloadMansionData:nil];
    
   //
    
    if(isIphone){
         manageDownload   = [[ManageMyDownloadViewController alloc]initWithNibName:@"ManageMyDownloadViewController" bundle:nil];
    }
    
    else{
        manageDownload   = [[ManageMyDownloadViewController alloc]initWithNibName:@"ManageMyDownloadViewController-iPad" bundle:nil];
       
    
    }
    
    manageDownload.mansionArray=self.mansionArray;
    manageDownload.deleteButtonURL=customerDetails.deleteButtonImagePath;
    [self.view addSubview:manageDownload.view];
    manageDownload.view.frame = self.view.frame;
}

-(void)homeClicked:(UIButton*)sender{
    [self resetAllTabButtons];
    [self stopAudioPlayer];
    if(directionVC){
        
        [directionVC.view removeFromSuperview];
        directionVC=nil;
        self.webActionSheetButton.hidden=YES;
    }
    if(storesVC){
        [storesVC.view removeFromSuperview];
        storesVC=nil;
        
    }
    self.webActionSheetButton.hidden=YES;
    [webViewCtrl.view removeFromSuperview];
    [newDirVC.view removeFromSuperview];
    sender.selected=YES;
}

-(void)loadTour:(UIButton*)sender{
    [self resetAllTabButtons];
    [self stopAudioPlayer];
    if(directionVC){
        
        [directionVC.view removeFromSuperview];
        directionVC=nil;
        self.webActionSheetButton.hidden=YES;
    }
    if(storesVC){
        [storesVC.view removeFromSuperview];
        storesVC=nil;
        
    }
    self.webActionSheetButton.hidden=YES;
    [webViewCtrl.view removeFromSuperview];
    [newDirVC.view removeFromSuperview];
    sender.selected=YES;
    
    if([customerDetails.shareEnabled caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
        self.actionSheetButton.hidden=NO;
    }
    
    else{
        self.actionSheetButton.hidden=YES;
    }

    
    if([customerDetails.isAutoPlay caseInsensitiveCompare:@"Yes"]==NSOrderedSame){
        self.greenPlayButton.hidden=NO;
    }
    
    else{
        self.greenPlayButton.hidden=YES;
    }
    
        [self sendDataForTrackingForAction:@"TourScreen" label:currentMansion.shortName];
        
        
        audioDetailDict = [[NSMutableDictionary alloc]initWithCapacity:1];
        
        
        sender.selected=NO;
        if(!self.mansion){
            self.mansion=[[Mansion alloc]init];
        }
        self.mansion= [self.mansion fetchDataForMansionName:currentMansion.shortName];
        NSDictionary *mansionDataDictionary = self.mansion.floorDict;
        if(mansionDataDictionary==nil){
            //..   [self downloadMansionData:sender];
            [self manageDownloads:nil];
            
            
            self.aboutButton.selected=YES;
            return;
        }
        NSString *mansionPath=[DocumentDirectory stringByAppendingPathComponent:currentMansion.shortName];
        //dbManager=[[DBManager alloc]initWithSQLPath:[NSString stringWithFormat:@"%@/Elms.sqlite",mansionPath]]; commented by mandar
        dbManager=[[DBManager alloc]initWithSQLPath:[NSString stringWithFormat:@"%@/%@.sqlite",mansionPath,currentMansion.shortName]];
        [dbManager openDataBase];
        subStopsName=[[NSMutableArray alloc]initWithCapacity:1];
        subStopsImagePath=[[NSMutableArray alloc]initWithCapacity:1];
        self.tourDetailView.frame=CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
        
        //-CGRectGetHeight(self.bottomView.frame)
        [self.mainView addSubview:self.tourDetailView];
        [self.view bringSubviewToFront:self.tourDetailView];
        
        
        allFloors=[self sortTheFloorForArray];  //[mansionDataDictionary allKeys];
        
        [self getAllFloorsRoomForDict:mansionDataDictionary];
        
        self.floorLabel.text=[allFloors objectAtIndex:0];
        NSArray *allRooms=[[mansionDataDictionary objectForKey:self.floorLabel.text] allKeys];
        allRooms=[self sortTheDatasource:allRooms];
        self.floorRoomsArray=[allRooms copy];
        presentRoom=[self.floorRoomsArray objectAtIndex:0];
        self.roomLable.text=[NSString stringWithFormat:@"%@ %@",presentRoom,[dbManager getNameForRoom:presentRoom]];//presentRoom;
        
        if((self.floorRoomsArray.count==1) && [allFloors count]==1) //// single floor with single room
        {
            self.previousRoom.hidden=YES;
            self.nextRoom.hidden=YES;
            leftArrow.hidden=YES;
            rightArrow.hidden=YES;
            
            
            
            // Later written
            
            leftArrow.hidden=YES;
            [self.greenPlayButton setHidden:YES];//mandar
            [self.appFeedbackButton setHidden:NO];//mandar
            [self.appFeedbackArrowImg setHidden:NO];
            
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
            // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
                subStops= [self getSubStopArrayForRoom];//..
            
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
            //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
         //..edit    [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
             [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            if([subStops count]==1)
                //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
         //..edit        [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
                 [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            //     [self.appFeedbackButton setHidden:YES];
            //    [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
                //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }
            
            
            
            
        }
        
        
        
        
        
        if(self.floorRoomsArray.count==1 && [allFloors count]>1) // single room for first floor but multiple floor
        {
            self.previousRoom.hidden=YES;
            leftArrow.hidden=YES;
            self.nextRoom.hidden=NO;
            rightArrow.hidden=NO;
            [self.nextRoom setTitle:[self.allFloorRoomArray objectAtIndex:1] forState:UIControlStateNormal];
            
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
            // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
                subStops= [self getSubStopArrayForRoom];//..
            
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
            //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
          //..edit   [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
             [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            if([subStops count]==1)
                //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
         //..edit        [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
                 [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            [self.appFeedbackButton setHidden:YES];
            [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
                //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }
            
            
            
        }
        
        
        
        
        else   if(self.floorRoomsArray.count>1)  //  mutiple rooms
            
        {
            self.previousRoom.hidden=YES;
            leftArrow.hidden=YES;
            self.nextRoom.hidden=NO;
            rightArrow.hidden=NO;
            [self.nextRoom setTitle:[self.floorRoomsArray objectAtIndex:1] forState:UIControlStateNormal];
            
            NSString *roomPath=[NSString stringWithFormat:@"%@/%@",mansionPath,self.floorLabel.text];
            [self setTourImageForPath:roomPath];
            // setting room images
            NSDictionary *rooms=[mansionDataDictionary objectForKey:self.floorLabel.text];
            NSDictionary *roomDetails=[rooms objectForKey:presentRoom];
            // Setting Substops
            subStops = [roomDetails objectForKey:@"SubStops"];
            if([[roomDetails objectForKey:@"SubStops"] count]>0)
                subStops= [self getSubStopArrayForRoom];//..
            
            [self prepareSubStopsNameForArray:subStops];
            [self prepareImagePathForSubStops:subStops];
            //..     [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stops",[subStops count]] forState:UIControlStateNormal];
   //..edit          [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
             [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            if([subStops count]==1)
                //..   [self.substopButton setTitle:[NSString stringWithFormat:@"%d Sub-Stop",[subStops count]] forState:UIControlStateNormal];
          //..edit       [self.substopButton setTitle:@"Audio Label​s" forState:UIControlStateNormal];
                 [self.substopButton setTitle:customerDetails.substopText forState:UIControlStateNormal];
            //..    self.substopButton.userInteractionEnabled=YES;
            self.substopButton.hidden=NO;
            self.semiUp.hidden=NO;
            [self.appFeedbackButton setHidden:YES];
            [self.appFeedbackButton setHidden:YES];
            if([subStops count]==0){
                //..   self.substopButton.userInteractionEnabled=NO;
                self.substopButton.hidden=YES;
                self.semiUp.hidden=YES;
            }
            
            
            
            
            
            
        }
        
        else{
            self.previousRoom.hidden=YES;
            self.nextRoom.hidden=YES;
            leftArrow.hidden=YES;
            rightArrow.hidden=YES;
        }
        
        UIImage *minImage=[[UIImage imageNamed:@"GreenSlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        UIImage *maxImage=[[UIImage imageNamed:@"GreySlider.png"]stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        [audioProgressSlider setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [audioProgressSlider setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [audioProgressSlider setThumbImage:[UIImage imageNamed:@"TrackBall"] forState:UIControlStateNormal];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupAppUI];
    });
    
    if(self.mansion.isGPSPackage){
    // Load GPS Screen
    gpsVC=[[GPSViewController alloc]init];
    gpsVC.gpsDataArray=[dbManager getDataForGPS];
    gpsVC.mansionPath=mansionPath;
    gpsVC.delegate=self;
    gpsVC.view.frame=CGRectMake(0,self.mainView.frame.origin.y, CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame));
    [self.mainView addSubview:gpsVC.view];
    }
   }

#pragma Segmented Control

-(void)showFloorMap:(id)sender{
    
   /* SegmentViewDetails *svd = (SegmentViewDetails*)sender;
    int pos = [segmentViewArray indexOfObject:svd];
    [self.segmentControl setSelectedSegmentIndex:0] */
    
    [self.mainView addGestureRecognizer:tapOnView];
    //toggle the correct view to be visible
   self.floorMapView.frame=CGRectMake(0, CGRectGetMaxY(self.segmentTopView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.segmentView.frame)-CGRectGetHeight(self.segmentTopView.frame));
    [self.segmentView addSubview:self.floorMapView];
    
    [self setMapImage];
    [self sendDataForTrackingForAction:@"FloorMapScreen" label:self.floorLabel.text];
    
}

-(void)showListView:(id)sender{
    //toggle the correct view to be visible
    [self.mainView removeGestureRecognizer:tapOnView]; //remove for table view
    
    self.floorListView.frame=CGRectMake(0, CGRectGetMaxY(self.segmentTopView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.segmentView.frame)-CGRectGetHeight(self.segmentTopView.frame));
    [self.segmentView addSubview:self.floorListView];
    [self.floorListTableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.allFloorRoomArray indexOfObject:presentRoom] inSection:0];
    [self.floorListTableView scrollToRowAtIndexPath:indexPath
                                   atScrollPosition:UITableViewScrollPositionTop
                                           animated:YES];
    [self sendDataForTrackingForAction:@"FloorListScreen" label:self.floorLabel.text];


}


#pragma Map Protocol

-(void)willShowGPSMap{
    if(subStopVC){
        [subStopVC removeView:nil];
        [subStopVC stopAudioPlayer];
        [subStopVC.view removeFromSuperview];
        subStopVC=nil;
    }
    
    
    [self removeView:nil];
    [self removeSegmentedView:nil];
    [self removeMaximizedView:nil];
    [self removeAllViews];
    [UIView animateWithDuration:0.5 animations:^{
        
        gpsVC.view.frame=CGRectMake(0, self.mainView.frame.origin.y, CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame));
        [gpsVC reCentreMapToDefault];
        
    }completion:^(BOOL finished) {
        
    }];
   
}

-(void)didEnterRegionWithName:(NSString *)name{
 
    if(subStopVC){
        [subStopVC removeView:nil];
    }
    [self removeView:nil];
    [self removeSegmentedView:nil];
    [self removeMaximizedView:nil];
    [self removeAllViews];
    [UIView animateWithDuration:0.5 animations:^{
        
       gpsVC.view.frame=CGRectMake(0, CGRectGetMaxY(self.mainView.frame), CGRectGetWidth(self.mainView.frame), CGRectGetHeight(self.mainView.frame)-CGRectGetHeight(self.topView.frame));
        if(name==nil){
            return ;
        }
        
        if(self.customPlayer){
            [self stopAudioPlayer];}
        [self loadRoomForNumber:[self.allFloorRoomArray objectAtIndex:[name integerValue]-1] andAutoPlay:YES];
        
    }completion:^(BOOL finished) {
        
    }];

    
    
    
    
}




@end
