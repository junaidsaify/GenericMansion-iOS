//
//  DirectionsViewController.h
//  Mansion
//
//  Created by Falguni Shah on 12/21/14.
//
//

#import <UIKit/UIKit.h>
#import <Mapkit/MapKit.h>

@interface DirectionsViewController : UIViewController <UIWebViewDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}
@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *spinner;
@property(nonatomic,retain)NSString *destinationString;
-(IBAction)removeView:(UIButton*)sender;
@property(nonatomic,retain)UIButton *safariButton;
@end
