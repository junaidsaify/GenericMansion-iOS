//
//  AudioPlayer.h
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//
#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
@interface AudioPlayer : NSObject<AVAudioPlayerDelegate,AVAudioSessionDelegate>

@property(nonatomic,strong)AVAudioPlayer *player;

-(void)createAudioPlayerWithPath:(NSString*)path;
-(void)togglePlayer:(AVAudioPlayer*)player;
-(void)stopAndDistroyPlayer;
-(void)pauseAudioPlayer;
@end
