//
//  ManageDownloadTableViewCell.h
//  GenericMansion
//
//  Created by Junaid Saify on 22/11/15.
//
//

#import <UIKit/UIKit.h>
@class ManageDownloadTableViewCell;
@protocol ManageDownloadDelegate<NSObject>
@optional
-(void)didClickOnDownloadButtonForCell:(ManageDownloadTableViewCell*)cell withDownloadButton:(UIButton*)button;
-(void)didClickOnDeleteButtonForCell:(ManageDownloadTableViewCell*)cell withDeleteButton:(UIButton*)button;


@end

@interface ManageDownloadTableViewCell : UITableViewCell





@property(nonatomic,retain) IBOutlet UILabel *mansionName , *progressLable;
@property(nonatomic,retain) IBOutlet UIButton *downloadNow , *deleteNow;
@property(nonatomic,retain) IBOutlet UIProgressView *progress;
@property(nonatomic,assign)id <ManageDownloadDelegate>delegate;

-(IBAction)downloadTourClicked:(UIButton*)sender;
-(IBAction)deleteTourClicked:(UIButton*)sender;

@end
