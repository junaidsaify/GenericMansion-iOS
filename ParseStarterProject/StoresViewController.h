//
//  StoresViewController.h
//  Mansion
//
//  Created by Snehal Shah on 5/24/15.
//
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
#import "StoresTableViewCell.h"
@interface StoresViewController : GAITrackedViewController

{
    NSDictionary *storesDictionary;
    WebViewController *webViewCtrl;
    NSMutableArray *attributedPhoneNumberArray,*attributedAddressArray;
       NSMutableArray *storesKeys;
     }

@property(nonatomic,retain)IBOutlet UITableView *storesTableView;

@end
