//
//  CustomTableViewCell.m
//  Mansion
//
//  Created by Falguni Shah on 10/18/14.
//
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell
@synthesize cellImageView,cellTextLable,isExpanded;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
   /*    self.cellImageView=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.contentView.frame)+20, 5, 40, 40)];
        self.cellTextLable= [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imageView.frame)+5, 2, 300, 50)];
        
        [self.contentView addSubview:self.cellImageView];
        [self.contentView addSubview:self.cellTextLable]; */
       
        
        self.cellImageView=[[UIImageView alloc]init];
        self.cellTextLable=[[UILabel alloc]init];
               [self.cellTextLable setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        if(isIphone){
            [self.cellTextLable setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        }

       self.cellImageView.frame=CGRectMake(20, CGRectGetMidY(self.contentView.bounds)-10, 41, 38);
         self.cellImageView.center=CGPointMake(30, CGRectGetMidY(self.contentView.bounds));
        self.cellTextLable.frame = CGRectMake(CGRectGetMaxX(self.cellImageView.frame)+5, CGRectGetMidY(self.contentView.bounds)-25, 300, 50);
        
      /*  self.cellImageView.frame= CGRectMake(self.contentView.frame.origin.x+10, 5, 40, 40);
        self.cellTextLable.frame = CGRectMake(CGRectGetMaxX(self.cellImageView.frame)+5, CGRectGetMinY(self.cellImageView.frame), 300, 50);
        
        self.cellTextLable.contentMode = UIViewContentModeScaleAspectFit;
        
        if(!isExpanded){
            self.cellImageView.center=CGPointMake(self.cellImageView.frame.origin.x+10, self.contentView.center.y);
            self.cellTextLable.frame = CGRectMake(CGRectGetMaxX(self.cellImageView.frame)+5, CGRectGetMidY(self.contentView.frame)-25, 300, 50);
        } */
        [self.contentView addSubview:self.cellImageView];
        [self.contentView addSubview:self.cellTextLable];
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*-(void)layoutSubviews {
    
   //.. [super layoutSubviews];
    
   /* self.cellImageView.frame= CGRectMake(self.contentView.frame.origin.x+10, 5, 40, 40);
     self.cellTextLable.frame = CGRectMake(CGRectGetMaxX(self.cellImageView.frame)+5, CGRectGetMinY(self.cellImageView.frame), 300, 50);
    
     self.cellTextLable.contentMode = UIViewContentModeScaleAspectFit;
    
    if(!isExpanded){
    self.cellImageView.center=CGPointMake(self.cellImageView.frame.origin.x+10, self.contentView.center.y);
     self.cellTextLable.frame = CGRectMake(CGRectGetMaxX(self.cellImageView.frame)+5, CGRectGetMidY(self.contentView.frame)-25, 300, 50);
    }
} */

@end
