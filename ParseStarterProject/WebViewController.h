//
//  WebViewController.h
//  Mansion
//
//  Created by Falguni Shah on 1/26/15.
//
//

#import <UIKit/UIKit.h>
#import "UIActionSheet+Blocks.h"
@interface WebViewController : UIViewController<UIWebViewDelegate>

@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,retain)IBOutlet UIView *topView;
@property(nonatomic,retain)NSString *urlString;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *spinner;
@property(nonatomic,retain)IBOutlet UIButton *safariButton,*backButton;
-(id)initWithURLString:(NSString*)urlStr;
-(IBAction)removeView:(UIButton*)sender;

@end
