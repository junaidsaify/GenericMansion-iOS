//
//  MansionDownloadManager.m
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//

#import "MansionDownloadManager.h"
#define USERNAME @"actioncharts\actioncharts"
#define PASSWORD @"Sarla2013"
@implementation MansionDownloadManager
@synthesize delegate,shouldJump;
-(id)init{
    self=[super init];
    if(self){
        
    }
    return self;
}


-(void)downloadFileForAddress:(NSString*)urlString forMansion:(NSString*)mansionName{
    self.mansionName=mansionName;
    NSURL *url = [NSURL URLWithString:urlString];
    
   request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
  /*  NSString *authStr = [NSString stringWithFormat:@"%@:%@", USERNAME, PASSWORD];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@",[authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"]; */
   //.. request=[NSMutableURLRequest requestWithURL:url];
    connection = [[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:YES];
    
}


# pragma NSURLConnection Delegate

// NSURLConnection Delegates
-(void) connection:(NSURLConnection*)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
 {
     NSLog(@"validate");
    if ([challenge previousFailureCount] == 0) {
        NSLog(@"received authentication challenge");
        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:USERNAME
                                                                    password:PASSWORD
                                                                 persistence:NSURLCredentialPersistenceForSession];
        NSLog(@"credential created");
        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
        NSLog(@"responded to authentication challenge");
    }
    else {
        NSLog(@"previous authentication failure");
    }
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Recieving Data");
    self.mdata = [[NSMutableData alloc]init];
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
   NSInteger statusCode = [httpResponse statusCode];
    if ((statusCode/100) == 2)
    {
        contentLength = [httpResponse expectedContentLength];
        NSLog(@"Expected Length: %ld",contentLength);
        if (contentLength == NSURLResponseUnknownLength)
            NSLog(@"unknown content length %ld", contentLength);
    }
    
    [self.delegate downloadManagerDidRecieveResponseWithContentLength:contentLength];

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    bytesSum += [data length];
    percent = ((float)bytesSum / (float)contentLength);
    NSLog(@"Completed percentage: %f",percent);
    [self.mdata appendData:data];
    
    [self.delegate downloadManagerDidRecieveResponseWithPercent:percent];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
     NSString *mansionPath =  [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip",self.mansionName]];
    [self.mdata writeToFile:mansionPath atomically:YES];
    
    // write unzipping logic
    
    // Unzipping
    NSString *destinationPath = [DocumentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",self.mansionName]];
    [SSZipArchive unzipFileAtPath:mansionPath toDestination:DocumentDirectory];
    
    // Delete the Zip after download
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:mansionPath]){
        [fileManager removeItemAtPath:mansionPath error:nil];
    }
    
    [self.delegate downloadManagerDidFinishLoading];
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
   NSLog(@"Failed Baba %@", error);
    [self.delegate downloadManagerDidFailWithError:error];
}


-(void)stopTheDownload{
    [connection cancel];
    connection=nil;
}

@end
