//
//  Mansion.m
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//

#import "Mansion.h"

@implementation Mansion
@synthesize mansionName,mansionImagesArray,aboutMansion;
@synthesize floorsArray;
@synthesize nameImage;
@synthesize isGPSPackage;

-(id)init {
    self= [super init];
    if(self){
          //.. self.floorsArray=[[NSMutableArray alloc]initWithCapacity:1];
    }
    
    return self;
}

-(Mansion*)fetchDataForMansionName:(NSString*)mansion {
    
    NSFileManager *manager= [NSFileManager defaultManager];
    NSString *mansionPath =  [DocumentDirectory stringByAppendingPathComponent:mansion];
   
    self.floorDict=[[NSMutableDictionary alloc] initWithCapacity:1];
    
    if ([manager fileExistsAtPath:mansionPath])
    {
        NSArray *floorsPresent= [manager contentsOfDirectoryAtPath:mansionPath error:nil];
        for(NSString *floorNames in floorsPresent){
            if([floorNames isEqualToString:@"MarkerImages"]){
                self.isGPSPackage=YES;
            }
            
            if(![floorNames isEqualToString:@".DS_Store"] && ![floorNames isEqualToString:[NSString stringWithFormat:@"%@.sqlite",mansion]]&& ![floorNames isEqualToString:@"MarkerImages"]){
          // create the data dictionary here based upon the floor name
               
                NSMutableDictionary *roomDict=[[NSMutableDictionary alloc] initWithCapacity:1];
                NSArray *roomPresent=[manager contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",mansionPath,floorNames] error:nil]; // Elms/Basement
                for(NSString *roomNames in roomPresent){ // 401,402,403.....
                    if(![roomNames isEqualToString:@".DS_Store"]){
                     NSMutableDictionary *lowerDict=[[NSMutableDictionary alloc] initWithCapacity:1];
                    NSArray *roomDetails=[manager contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@/%@",mansionPath,floorNames,roomNames] error:nil];
                    // Elms/Basement/429
                    
                    for(NSString *metadata in roomDetails){
                        if(![metadata isEqualToString:@".DS_Store"]){
                        NSMutableArray *fileNames=[manager contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@/%@/%@",mansionPath,floorNames,roomNames,metadata] error:nil];//Elms/Basement/429/Audio
                            [fileNames removeObject:@".DS_Store"];
                            
                            
                        [lowerDict setObject:fileNames forKey:metadata];// 401.mp3 for Audio .....
                        
                        }
                    }
                    
                    [roomDict setObject:lowerDict forKey:roomNames]; // query for string value of 401..
                    
                }
                }
               [self.floorDict setObject:roomDict forKey:floorNames]; // query floor number.
                
                
            }
        }
        
        
        return self;
    }

    else
        return nil;
}






@end
