//
//  FMDBManager.m
//  GenericMansion
//
//  Created by Junaid Saify on 07/11/15.
//
//

#import "FMDBManager.h"


@implementation FMDBManager
@synthesize dataArray,database;

-(id)init {
    
    if(!self){
        self=[super init];
    
    }
    NSString *path = [DocumentDirectory stringByAppendingPathComponent:@"Metadata.sqlite"];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    if([manager fileExistsAtPath:path]){
    
    database = [FMDatabase databaseWithPath:path];
    [database open];
    }
    return self;
}


-(NSMutableArray*)returnValueForQuery:(NSString*)queryString forTable:(NSString*)tableName{
    NSMutableArray *resultArray;
    
    
        if([tableName isEqualToString:@"SideMenu"]){
        resultArray = [[NSMutableArray alloc]initWithCapacity:1];
        FMResultSet *results = [database executeQuery:queryString];
        while([results next]) {
        //    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:1];
            SideMenu *sideMenu = [[SideMenu alloc]init];
            
            
            NSString *name = [results stringForColumn:@"Name"];
            NSString *imagePath = [results stringForColumn:@"ImagePath"];
            NSString *onclick = [results stringForColumn:@"OnClick"];
            NSString *link = [results stringForColumn:@"Link"];
            NSString *browser = [results stringForColumn:@"Safari"];
            NSString *extra = [results stringForColumn:@"Extra"];
            NSInteger sequence  = [results intForColumn:@"Sequence"];
            
            sideMenu.menuName=name;
            sideMenu.menuImagePath=imagePath;
            sideMenu.menuOnClick=onclick;
            sideMenu.menuLink=link;
            sideMenu.menuBrowser=browser;
            sideMenu.menuExtra=extra;
            sideMenu.menuSequence=sequence;
            
      /*      [dict setObject:(name!=nil) ? name:[NSNull null] forKey:@"Name"];
            [dict setObject:(imagePath!=nil) ? imagePath:[NSNull null] forKey:@"ImagePath"];
            [dict setObject:(onclick!=nil) ? onclick:[NSNull null] forKey:@"OnClick"];
            [dict setObject:(link!=nil) ? link:[NSNull null] forKey:@"Link"];
            [dict setObject:(browser!=nil) ? browser:[NSNull null] forKey:@"Safari"];
            [dict setObject:(extra!=nil) ? extra:[NSNull null] forKey:@"Extra"];
            [dict setObject:[NSNumber numberWithInteger:sequence] forKey:@"Sequence"]; */
            
            [resultArray addObject:sideMenu];
        }
        
        
        
    }
    
        else if ([tableName isEqualToString:@"BottomMenu"]) {
            resultArray = [[NSMutableArray alloc]initWithCapacity:1];
            FMResultSet *results = [database executeQuery:queryString];
            while([results next]) {
                //    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:1];
                BottomMenu *bottomMenu = [[BottomMenu alloc]init];
                
                
                NSString *name = [results stringForColumn:@"Name"];
                NSString *imagePath = [results stringForColumn:@"ImagePath"];
                NSString *onclick = [results stringForColumn:@"OnClick"];
                NSString *link = [results stringForColumn:@"Link"];
                NSString *browser = [results stringForColumn:@"Safari"];
                NSString *extra = [results stringForColumn:@"Extra"];
                NSInteger sequence  = [results intForColumn:@"Sequence"];
                
                bottomMenu.menuName=name;
                bottomMenu.menuImagePath=imagePath;
                bottomMenu.menuOnClick=onclick;
                bottomMenu.menuLink=link;
                bottomMenu.menuBrowser=browser;
                bottomMenu.menuExtra=extra;
                bottomMenu.menuSequence=sequence;
                
                /*      [dict setObject:(name!=nil) ? name:[NSNull null] forKey:@"Name"];
                 [dict setObject:(imagePath!=nil) ? imagePath:[NSNull null] forKey:@"ImagePath"];
                 [dict setObject:(onclick!=nil) ? onclick:[NSNull null] forKey:@"OnClick"];
                 [dict setObject:(link!=nil) ? link:[NSNull null] forKey:@"Link"];
                 [dict setObject:(browser!=nil) ? browser:[NSNull null] forKey:@"Safari"];
                 [dict setObject:(extra!=nil) ? extra:[NSNull null] forKey:@"Extra"];
                 [dict setObject:[NSNumber numberWithInteger:sequence] forKey:@"Sequence"]; */
                
                [resultArray addObject:bottomMenu];
            
        }
    
        }
    
    
    else if ([tableName isEqualToString:@"TopMenu"]){
        resultArray = [[NSMutableArray alloc]initWithCapacity:1];
        FMResultSet *results = [database executeQuery:queryString];
        while([results next]) {
            TopMenu *topMenu = [[TopMenu alloc]init];
            
            NSString *name = [results stringForColumn:@"Name"];
            NSString *imagePath = [results stringForColumn:@"ImagePath"];
            NSString *onclick = [results stringForColumn:@"OnClick"];
            NSString *link = [results stringForColumn:@"Link"];
            NSString *browser = [results stringForColumn:@"Safari"];
            NSString *extra = [results stringForColumn:@"Extra"];
            NSInteger sequence  = [results intForColumn:@"Sequence"];
            
            topMenu.topMenuName = name;
            topMenu.topMenuImagePath = imagePath;
            topMenu.topMenuOnClick=onclick;
            topMenu.topMenuLink=link;
            topMenu.topMenuBrowser=browser;
            topMenu.topMenuExtra=extra;
            topMenu.topMenuSequence=sequence;
     
            [resultArray addObject:topMenu];
            
        }
        
    }
    
    else if([tableName isEqualToString:@"MansionDetails"]){
        resultArray = [[NSMutableArray alloc]initWithCapacity:1];
        FMResultSet *results = [database executeQuery:queryString];
        while([results next]) {
            MansionModel *mansion = [[MansionModel alloc]init];
            mansion.name=[results stringForColumn:@"Name"];
            mansion.shortName=[results stringForColumn:@"ShortName"];
            mansion.imagePath=[results stringForColumn:@"ImagePath"];
            mansion.exteriorImagePath=[results stringForColumn:@"ExteriorImagePath"];
            mansion.interiorImagePath=[results stringForColumn:@"InteriorImagePath"];
            mansion.tourPath=[results stringForColumn:@"TourPath"];
            mansion.aboutMansion=[results stringForColumn:@"AboutMansion"];
            mansion.address=[results stringForColumn:@"Address"];
            mansion.sequence=[results intForColumn:@"Sequence"];
            mansion.extra=[results stringForColumn:@"Extra"];
            mansion.fileSizeInMB=[results stringForColumn:@"FileSizeInMB"];
            mansion.latitude=[results doubleForColumn:@"Latitude"];
            mansion.longitude=[results doubleForColumn:@"Longitude"];
            mansion.cultureCode=[results stringForColumn:@"CultureCode"];
            mansion.GUID=[results stringForColumn:@"GUID"];
            
            [resultArray addObject:mansion];
            
        }
        
    }
    
    else if ([tableName isEqualToString:@"Customer"]) {
        resultArray = [[NSMutableArray alloc]initWithCapacity:1];
        FMResultSet *results = [database executeQuery:queryString];
         while([results next]) {
             CustomerDetails *customer = [[CustomerDetails alloc]init];
             customer.customerName=[results stringForColumn:@"Name"];
             customer.ipadGridImagePath=[results stringForColumn:@"GridImageIpad"];
             customer.iphoneGridImagePath=[results stringForColumn:@"GridImageIphone"];
             customer.sideMenuImagePath=[results stringForColumn:@"SideMenuImage"];
             customer.copywriteText=[results stringForColumn:@"Copywrite"];
             customer.extra=[results stringForColumn:@"Extra"];
             customer.colorCode=[results stringForColumn:@"ColorCode"];
             customer.website=[results stringForColumn:@"Website"];
             customer.tagline=[results stringForColumn:@"Tagline"];
             customer.sideMenuColor=[results stringForColumn:@"SideMenuColor"];
             customer.topMenuColor=[results stringForColumn:@"TopMenuColor"];
             customer.bottomMenuColor=[results stringForColumn:@"BottomMenuColor"];
             customer.tourNextArrowPath=[results stringForColumn:@"TourNextArrowPath"];
             customer.tourPreviousArrowPath=[results stringForColumn:@"TourPreviousArrowPath"];
             customer.maximizeImagePath=[results stringForColumn:@"MaximizeImagePath"];
             customer.tourPlayButtonPath=[results stringForColumn:@"TourPlayButtonPath"];
             customer.autoPlayButtonPath=[results stringForColumn:@"AutoPlayButtonPath"];
             customer.isAutoPlay=[results stringForColumn:@"isAutoPlay"];
             customer.tourPauseButtonPath=[results stringForColumn:@"TourPauseButtonPath"];
             customer.popoverButtonPath=[results stringForColumn:@"PopoverButtonPath"];
             customer.substopText=[results stringForColumn:@"SubstopText"];
             customer.substopShowImagePath=[results stringForColumn:@"SubstopShowImagePath"];
             customer.substopRemoveImagePath=[results stringForColumn:@"SubstopRemoveImagePath"];
             customer.closeButtonImagePath=[results stringForColumn:@"CloseButtonImagePath"];
             customer.deleteButtonImagePath=[results stringForColumn:@"DeleteButtonImagePath"];
             customer.actionShowImagePath=[results stringForColumn:@"ActionshowImagePath"];
             customer.listViewDefaultImagePath=[results stringForColumn:@"ListViewDefaultImagePath"];
             customer.listViewSelectedImagePath=[results stringForColumn:@"ListViewSelectedImagePath"];
             customer.shareEnabled=[results stringForColumn:@"ShareEnabled"];
             customer.mansionLeftArrowPath=[results stringForColumn:@"MansionLeftArrowPath"];
              customer.mansionRightArrowPath=[results stringForColumn:@"MansionRightArrowPath"];
             customer.donateLink=[results stringForColumn:@"DonateLink"];
               customer.substopBackgroundColor=[results stringForColumn:@"SubstopBackgroundColor"];
               customer.segmentViewName=[results stringForColumn:@"SegmentViewName"];
             [resultArray addObject:customer];
             
         }
        
    }
    
    else if ([tableName isEqualToString:@"FeedbackDetails"]){
        resultArray = [[NSMutableArray alloc]initWithCapacity:1];
        FMResultSet *results = [database executeQuery:queryString];
        while([results next]) {
            FeedbackMailDetails *feedbackMailDetails= [[FeedbackMailDetails alloc]init];
            feedbackMailDetails.recipient = [results stringForColumn:@"Recipient"];
            feedbackMailDetails.subject = [results stringForColumn:@"Subject"];
            feedbackMailDetails.body = [results stringForColumn:@"Body"];
            feedbackMailDetails.extra= [results stringForColumn:@"Extra"];
            [resultArray addObject:feedbackMailDetails];
            
        }
    }
    
    else if ([tableName isEqualToString:@"SegmentView"]) {
        resultArray = [[NSMutableArray alloc]initWithCapacity:1];
        FMResultSet *results = [database executeQuery:queryString];
        while([results next]) {
            SegmentViewDetails *segmentViewDetails= [[SegmentViewDetails alloc]init];
            segmentViewDetails.name = [results stringForColumn:@"Name"];
            segmentViewDetails.onClick = [results stringForColumn:@"OnClick"];
            segmentViewDetails.colorCode = [results stringForColumn:@"ColorCode"];
             segmentViewDetails.browser = [results stringForColumn:@"Safari"];
            segmentViewDetails.link= [results stringForColumn:@"Link"];
            [resultArray addObject:segmentViewDetails];
            
        }

    }
    
    
    return resultArray;
    
}




@end
