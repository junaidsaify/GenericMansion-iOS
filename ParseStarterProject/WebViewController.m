//
//  WebViewController.m
//  Mansion
//
//  Created by Falguni Shah on 1/26/15.
//
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController
@synthesize urlString,backButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   [self.webView loadHTMLString:@"" baseURL:nil];
   //.. urlString= DonateLink;
    //Create a URL object.
    
    NSURL *url = [NSURL URLWithString: [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.spinner startAnimating];
    [self.view bringSubviewToFront:self.spinner];
    //Load the request in the UIWebView.
     [self.webView loadRequest:requestObj];
    //...[self.webView performSelector:@selector(loadRequest:) withObject:requestObj afterDelay:2.0];
    self.safariButton.hidden=NO;
    self.topView.backgroundColor=[UIColor colorWithString:@"#00563F"];
    
    if([self.urlString isEqualToString:DonateLink]){
     //   self.safariButton.hidden=NO;

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* -(id)initWithURLString:(NSString*)urlStr{
    self=[super init];
    if(self){
      self.urlString=urlStr;
    }
    
    return self;
} */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)showWebActionSheet:(id)sender
{
    [UIActionSheet showFromRect:self.safariButton.frame inView:self.view animated:YES withTitle:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@[@"Open in Safari"]  tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 0:{
                
                
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
            }
                break;
            default:
                break;
        }
        
    }];
}

-(IBAction)openInSafari:(UIButton*)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
}

-(IBAction)removeView:(UIButton*)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma WebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
}


@end
