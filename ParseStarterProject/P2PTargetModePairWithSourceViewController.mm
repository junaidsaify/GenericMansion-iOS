//
//  P2PTargetModePairWithSourceViewController.m
//  SyncDriveCore
//
//  Created by Alan Carney on 04/09/2013.
//  Copyright (c) 2013 Newbay Software Ltd. All rights reserved.
//

#import "QRCodeReader.h"
#import "P2PTargetModePairWithSourceViewController.h"
#import "P2PTargetModeConnectWithAccessCodeViewController.h"
#import "P2PSessionManager.h"
#import "SDSecondaryButton.h"
#import "STP2PLib.h"
#import "STP2PClientServerInteractionManager.h"
#import "P2PStrings.h"
#import "P2PUtils.h"
#import "STP2PUtilities.h"
#import "NotificationManager.h"
#import "P2PTargetModeManualPairingViewController.h"
#import "P2PTargetModeQRCodeScannerViewController.h"
#import "ConfigurationManager.h"
#import "P2PConstants.h"
#import "SDAnalyticsWrapper.h"
#import "GenericStrings.h"
#import "SDBarButtonItem.h"
#import "AnalysisStrings.h"

#define kScanTimeout 10.0
#define kSampleDataSize (1 * 1024 * 1024)

@interface P2PTargetModePairWithSourceViewController () {
    BOOL        isListening;
    NSTimer     *scanTimer;
    NSDate      *scanStartTime;
    NSInteger   selectedIndex;
    BOOL        isFirstPass;
    
    STP2PDiscoverManager *discoveryManager;
    NSMutableArray *discoveredServers;
    
    NSString *ipAddress;
    NSString *port;
    
    UIRefreshControl *_refreshControl;
    UIView           *_headerView;
    UIView           *_footerView;
    
    UIView           *_scanningProgressView;
    UILabel          *_scanLabel;
    UILabel          *_scanTimeLabel;
    UIActivityIndicatorView *_scanActivityIndicator;
    
    CFAbsoluteTime speedTestStartTime;
    NSInteger bandwidthBPS;
    long long sampleTotalBytesRead;
    int        elipsisCounter;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@end

@implementation P2PTargetModePairWithSourceViewController

@synthesize tableView = _tableView;
@synthesize isDisplayedAsOption;
@synthesize initialSSID;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:P2P_TARGET_PAIR_TITLE];
    
    [self configureRightbarButtonWithTitle:P2P_PAIR_SCAN_BUTTON setEnabled:YES];
    
    if ([self checkNumberOfOptions] > 0) {
        SDSecondaryButton* optionButton = [[[SDSecondaryButton alloc] initWithFrame:CGRectMake(0.0,0.0,65.0,30.0)] autorelease];
        [optionButton setTitle:P2P_MORE_OPTION_BUTTON forState:UIControlStateNormal];
        [optionButton setEnabled:YES];
        [optionButton addTarget:self action:@selector(optionsButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *optionBarButton = [[[UIBarButtonItem alloc] initWithCustomView:optionButton] autorelease];
        self.navigationItem.leftBarButtonItem = optionBarButton;
    }
    [self discoverServer];
    
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [self configureHeaderView];
    [self configureFooterView];
    [self configureRefreshControl];
    [self configureProgressView];
}

-(void)discoverServer {
    selectedIndex = -1;
    scanStartTime = nil;
    isListening = NO;
    isFirstPass = YES;
    discoveredServers = [[NSMutableArray alloc] init];
    ipAddress = nil;
    port = nil;
    
    speedTestStartTime = 0.0;
    bandwidthBPS = 0;
    sampleTotalBytesRead = 0;
    
    discoveryManager = [STP2PDiscoverManager defaultManager];
    [discoveryManager setTimeOut:kScanTimeout];
    [discoveryManager discoverServers:@"P2P" callback:^(NSArray *servers, NSError *error) {
        if (error == nil && [servers count] > 0) {
            [discoveredServers removeAllObjects];
            [discoveredServers addObjectsFromArray:servers];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
        } else {
            DLog(@"No P2P Servers were found.");
        }
    }];
    
    self.initialSSID = [NSString stringWithString:[STP2PUtilities getWifiName]];
}

-(void)configureRightbarButtonWithTitle:(NSString*)buttonTitle setEnabled:(BOOL)buttonState {
    SDSecondaryButton* optionButton = [[[SDSecondaryButton alloc] initWithFrame:CGRectMake(0.0,0.0,65.0,30.0)] autorelease];
    [optionButton setTitle:buttonTitle forState:UIControlStateNormal];
    [optionButton addTarget:self action:@selector(retryButtonWasTouched:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *optionBarButton = [[[UIBarButtonItem alloc] initWithCustomView:optionButton] autorelease];
    self.navigationItem.rightBarButtonItem = optionBarButton;
    [self.navigationItem.rightBarButtonItem setEnabled:buttonState];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self willAnimateRotationToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation] duration:0];
    
    //[[self navigationItem] setHidesBackButton:YES];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    [discoveredServers removeAllObjects];
    [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [discoveryManager stopListening];
    [[SDAnalyticsWrapper analyticApi] viewWillDisappear:MCT_PAGE_SCAN_TARGET];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[SDAnalyticsWrapper analyticApi] viewDidAppear:MCT_PAGE_SCAN_TARGET];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    
    if (isListening == NO) {
        [self retryButtonWasTouched:nil];
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

#pragma mark - Reachability Callbacks
- (void)networkStateChanged:(SDNetworkStatus *)networkStatus {
    if ([networkStatus isNotReachable]) {
        DLog(@"networkStateChanged - isNotReachable");
        [self.tableView setUserInteractionEnabled:NO];
        [self displayWifiAlertWithTitle:P2P_ALERT_NO_WIFI andMessage:P2P_ALERT_NO_WIFI_MSG];
        [[SDAnalyticsWrapper analyticApi] customProperty:@"1" forKey:MCT_SET_DISCONNECTS];
        
    } else {
        if([networkStatus isReachableViaWifi]) {
            [[SDAnalyticsWrapper analyticApi] customProperty:[STP2PUtilities getWifiName] forKey:MCT_SET_NETWORKTYPERECOVERED];
            NSString* message = [NSString stringWithFormat:P2P_ALERT_WIFI_OK_MSG,[STP2PUtilities getWifiName]];
            [self displayWifiAlertWithTitle:P2P_ALERT_WIFI_OK andMessage:message];
            [self.tableView setUserInteractionEnabled:YES];
        }
    }
}

-(void)displayWifiAlertWithTitle:(NSString*)alertTilte andMessage:(NSString*)alertMsg {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[[UIAlertView alloc] initWithTitle:alertTilte
                                     message:alertMsg
                                    delegate:self
                           cancelButtonTitle:GENERIC_BUTTON_OK
                           otherButtonTitles:nil, nil] autorelease] show];
    });
    
}

#pragma mark otpions management
-(int)checkNumberOfOptions {
    int nbOptions = 0;
    
    if (self.isDisplayedAsOption == YES) {
        //the user tapped the option button to display this ViewController
        //so don't dislay again the option button
        return 0;
    }
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionQRCodeIsOption] == YES)
        nbOptions++;
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionManualPairingIsOption] == YES)
        nbOptions++;
    
    return nbOptions;
}

-(void)optionsButtonTouched {
    
    if ([self checkNumberOfOptions] > 1) {
        [self configureOptionsActionSheet];
        
    } else if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionQRCodeIsOption] == YES) {
        
        [self displayQRCodeVC];
        
    } else if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionManualPairingIsOption] == YES) {
        
        [self displayManualPairingVC];
    }
}

-(void)configureOptionsActionSheet {
    UIActionSheet *actionSheet;
    
    if ([[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionQRCodeIsOption] == YES
        &&[[ConfigurationManager sharedInstance] isP2POptionActive:kP2POptionManualPairingIsOption] == YES) {
        
        actionSheet = [[[UIActionSheet alloc] initWithTitle:P2P_SELECT_PAIRING_METHOD
                                                   delegate:self
                                          cancelButtonTitle:GENERIC_BUTTON_CANCEL
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:P2P_SELECT_PAIRING_METHOD_QRCODE_TEXT, P2P_SELECT_PAIRING_METHOD_SELECT_SOURCE_TEXT, nil] autorelease];
        
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        if ([window.subviews containsObject:self.view]) {
            [actionSheet showInView:self.view];
        } else {
            [actionSheet showInView:window];
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    DLog(@"Action Sheet button clicked at Index= <%ld>",(long)buttonIndex);
    
    switch (buttonIndex) {
        case 0:
            [self displayQRCodeVC];
            break;
            
        case 1:
            [self displayManualPairingVC];
            break;
            
        default:
            break;
    }
}

-(void)displayQRCodeVC {
    P2PTargetModeQRCodeScannerViewController *generateQRCodeVC = [[UIStoryboard storyboardWithName:@"P2PTargetModeIPhone" bundle:[P2PUtils uiBundle]] instantiateViewControllerWithIdentifier:@"P2PTargetModeQRCodeScannerViewControllerID"];
    generateQRCodeVC.isDisplayedAsOption = YES;
    [self.navigationController pushViewController:generateQRCodeVC animated:YES];
}

-(void)displayManualPairingVC {
    P2PTargetModeManualPairingViewController *pairWithSourceVC = [[UIStoryboard storyboardWithName:@"P2PTargetModeIPhone" bundle:[P2PUtils uiBundle]] instantiateViewControllerWithIdentifier:@"P2PTargetModeManualPairingViewControllerID"];
    pairWithSourceVC.isDisplayedAsOption = YES;
    [self.navigationController pushViewController:pairWithSourceVC animated:YES];
}


#pragma mark TableView Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 90.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return isListening ? 66.0 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger nbRow = 0;
    
    if ([discoveredServers count] == 0) {
        nbRow = 8;
    } else {
        nbRow = [discoveredServers count] + 1;
    }
    
    return nbRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    static NSString *cellIdentifier = @"P2PServerNameCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    STP2ServerItem *serverItem = nil;
    
    if ([indexPath row] == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",P2P_DISPLAY_WIFI_LABEL,[STP2PUtilities getWifiName]];
    } else if ([indexPath row] > 0) {
        
        if ([discoveredServers count] > 0 /*&& indexPath.row < [discoveredServers count]*/) {
            serverItem = [discoveredServers objectAtIndex:indexPath.row - 1];
            cell.textLabel.text = [NSString stringWithFormat:@"%@",serverItem.displayName];
        } else {
            cell.textLabel.text = @"";
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([discoveredServers count] > 0) {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        selectedIndex = indexPath.row;
        isListening = NO;
        [discoveryManager stopListening];
        [self toggleRefreshControl:NO];
        
        STP2ServerItem *serverItem = [discoveredServers objectAtIndex:indexPath.row];
        [[STP2PClientServerInteractionManager sharedInstance] setPeer:serverItem];
        
        if ([[P2PSessionManager sharedInstance] transferInProgress] == NO) {
            [[STP2PClientServerInteractionManager sharedInstance] getGeneratePin:^(NSError *error) {
                
                if (error != nil) {
                    DLog(@"P2P Error: Could not generate PIN. %@", error);
                    [self showPINGenerateFailureError];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self performSegueWithIdentifier:@"P2PTargetModeConnectWithAccessCodeSeque" sender:_tableView];
                    });
                }
            }];
        } else {
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return _headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *blankView = nil;
    
    if (isListening) {
        return _footerView;
    } else {
        blankView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 1.0)] autorelease];
        [blankView setBackgroundColor:[UIColor whiteColor]];
        [blankView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    }

    return blankView;
}

- (void)showPINGenerateFailureError {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was a problem generating an Access Code." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Try Again", nil];
        [alertView setTag:kAlertViewFailedGeneratePinTag];
        [alertView show];
        [alertView release];
    });
}


#pragma mark Seque handler

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    STP2ServerItem *serverItem = nil;
    P2PTargetModeConnectWithAccessCodeViewController *targetController = nil;
    SDBarButtonItem *backButton = nil;
    
    if ([[segue identifier] isEqualToString:@"P2PTargetModeConnectWithAccessCodeSeque"]) {
        targetController = [segue destinationViewController];
        
        if (selectedIndex >= 0 && selectedIndex < [discoveredServers count]) {
            serverItem = [discoveredServers objectAtIndex:selectedIndex];
            [targetController setServerItem:serverItem];
        }
        
        backButton = [[[SDBarButtonItem alloc] initBackButtonWithTarget:nil action:nil] autorelease];
        [[self navigationItem] setBackBarButtonItem:backButton];
    } else if ([[segue identifier] isEqualToString:@"P2PTargetModePairViaQRCodeCompleteSegue"]) {
        [[P2PSessionManager sharedInstance] setSourceServerAddress:ipAddress andPort:port];
        [[P2PSessionManager sharedInstance] setEstimatedBandwidth:bandwidthBPS];
    }
}

#pragma mark pair/retry Button Actions
- (void)retryButtonWasTouched:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_tableView reloadData];
    });

    [self startScan];
}

#pragma mark Rotation Methods

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self updateUI];
}

#pragma mark Timer

- (void)timerDidFire:(NSTimer *)timer {
    NSDate *now = nil;
    NSTimeInterval elapsedTime = 0;
    NSTimeInterval remainingTime = 0;
    NSInteger numSeconds  = 0;
    NSString *labelText = nil;
    
    now = [NSDate date];
    elapsedTime = [now timeIntervalSinceDate:scanStartTime];
    remainingTime = [discoveryManager timeOut] - elapsedTime;
    numSeconds = (int)ceilf(remainingTime);
    
    elipsisCounter++;
    
    NSString *labelString = P2P_TARGET_SCAN_HEADER_SCANNING;
    
    if (elipsisCounter == 1) {
        labelString = [P2P_TARGET_SCAN_HEADER_SCANNING stringByAppendingString:@"."];
    }else if (elipsisCounter == 2) {
        labelString = [P2P_TARGET_SCAN_HEADER_SCANNING stringByAppendingString:@".."];
    }else if (elipsisCounter == 3) {
        labelString = P2P_TARGET_SCAN_HEADER_SCANNING;
        elipsisCounter = 0;
    }
    
    [_scanLabel setText:labelString];
    
    if (numSeconds <= 0) {
        numSeconds = 0;
        [discoveryManager stopListening];
        [scanTimer invalidate];
        [scanTimer release], scanTimer = nil;
        [_scanActivityIndicator stopAnimating];
        
        isListening = NO;
        
        [self toggleRefreshControl:NO];
        [self configureRightbarButtonWithTitle:P2P_PAIR_SCAN_BUTTON setEnabled:YES];
        
        NSString *labelString = P2P_TARGET_SCAN_HEADER;
        [_scanLabel setTextAlignment:NSTextAlignmentCenter];
        [_scanLabel setText:labelString];
        
        [_tableView reloadData];
    }
    
    labelText = [NSString stringWithFormat:@"%ld %@", (long)numSeconds, P2P_TARGET_PAIR_SCAN_TIME_LABEL];
    [_scanTimeLabel setText:labelText];
    [self updateUI];
}

#pragma mark Private Methods

- (void)configureHeaderView {
    CGFloat viewHeight = 0.0;
    UIView  *borderView = nil;
    
    viewHeight = [self tableView:_tableView heightForHeaderInSection:0];
    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, viewHeight)];
    [_headerView setBackgroundColor:[UIColor colorWithRed:245.0 / 255.0 green:245.0 / 255.0 blue:245.0 / 255.0 alpha:1.0]];
    [_headerView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    
    borderView = [[UIView alloc] initWithFrame:CGRectMake(0, viewHeight - 2.0, _tableView.frame.size.width, 1.0)];
    [borderView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    [borderView setBackgroundColor:[UIColor lightGrayColor]];
    
    [_headerView addSubview:borderView];
    [borderView release];
    
    _scanLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 5.0, _tableView.frame.size.width - 30.0, viewHeight - 10.0)];
    [_scanLabel setBackgroundColor:[UIColor clearColor]];
    [_scanLabel setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    
    NSString *labelString = P2P_TARGET_SCAN_HEADER;
    if ([P2PUtils SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO:@"6.0"]) {
        [_scanLabel setAttributedText:[self richTextForString:labelString withTargetText:@"pair"]];
    } else {
        [_scanLabel setText:labelString];
    }
    [_scanLabel setNumberOfLines:0];
    [_scanLabel setTextAlignment:NSTextAlignmentLeft];
    [_scanLabel setAdjustsFontSizeToFitWidth:YES];
    
    [_headerView addSubview:_scanLabel];
    [_scanLabel release];
    
}

-(NSMutableAttributedString*)richTextForString:(NSString*)aString  withTargetText:(NSString*)targetText {
    NSMutableAttributedString *attrString    = nil;
    NSRange targetRange;
    
    targetRange = [aString rangeOfString:targetText];
    attrString = [[[NSMutableAttributedString alloc] initWithString:aString] autorelease];
    [attrString setAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:17.0f]} range:targetRange];
    
    return attrString;
}

- (void)configureFooterView {
    CGFloat viewHeight = 0.0;
    
    //viewHeight = [self tableView:_tableView heightForFooterInSection:0];
    viewHeight = 66.0;//[self tableView:_tableView heightForFooterInSection:0];
    _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, viewHeight)];
    [_footerView setBackgroundColor:[UIColor colorWithRed:245.0 / 255.0 green:245.0 / 255.0 blue:245.0 / 255.0 alpha:1.0]];
    [_footerView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];

}

- (void)configureProgressView {
    NSString *labelText = nil;
    UIView *borderView = nil;
    
    _scanningProgressView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _footerView.frame.size.width, _footerView.frame.size.height)];
    [_scanningProgressView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    [_scanningProgressView setBackgroundColor:[UIColor whiteColor]];
    [_scanningProgressView setHidden:YES];
    
    borderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _footerView.frame.size.width, 1.0)];
    [borderView setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    [borderView setBackgroundColor:[UIColor lightGrayColor]];
    
    [_scanningProgressView addSubview:borderView];
    [borderView release];

    _scanActivityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0, (_footerView.frame.size.height / 2.0) - 16.0, 32.0, 32.0)];
    [_scanActivityIndicator setColor:[UIColor blackColor]];
    [_scanningProgressView addSubview:_scanActivityIndicator];
    [_scanActivityIndicator release];
    
    labelText = [NSString stringWithFormat:@"%d secs remaining, please wait...", (int)kScanTimeout];
    _scanTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(35.0, 0.0, 320.0 - 70.0, _footerView.frame.size.height)];
    [_scanTimeLabel setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
    [_scanTimeLabel setBackgroundColor:[UIColor clearColor]];
    [_scanTimeLabel setText:labelText];
    [_scanningProgressView addSubview:_scanTimeLabel];
    [_scanTimeLabel release];
    
    [_footerView addSubview:_scanningProgressView];
    [_scanningProgressView release];
}

- (void)configureRefreshControl {

}

- (void)startScan {
    isListening = YES;
    
    [_tableView reloadData];
    [self toggleRefreshControl:YES];
    
    NSString *labelString = P2P_TARGET_SCAN_HEADER_SCANNING;
    [_scanLabel setTextAlignment:NSTextAlignmentLeft];
    [_scanLabel setText:labelString];
    elipsisCounter = 0;
    [scanStartTime release], scanStartTime = nil;
    
    [_scanTimeLabel setText:[NSString stringWithFormat:@"%d %@", (int)[discoveryManager timeOut],P2P_TARGET_PAIR_SCAN_TIME_LABEL]];
    scanTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerDidFire:) userInfo:nil repeats:YES] retain];
    scanStartTime = [[NSDate date] retain];
    [_scanActivityIndicator startAnimating];
    
    [discoveryManager startListening];
    [self updateUI];
}

- (void)toggleRefreshControl:(BOOL)startRefreshing {
    if (_refreshControl) {
        if (startRefreshing) {
            [_refreshControl beginRefreshing];
            
            if (_tableView.contentOffset.y == 0) {
                [UIView animateWithDuration:0.15 delay:0 options:0 animations:^(void) {
                    [_tableView setContentOffset:CGPointMake(0, -_refreshControl.frame.size.height)];
                    
                } completion:^(BOOL finished){
                }];
            }
        } else {
            [_refreshControl endRefreshing];
            [UIView animateWithDuration:0.15 delay:0 options:0 animations:^(void) {
                [_tableView setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
            } completion:^(BOOL finished) {
                
            }];
        }
    }
}

- (void)updateUI {
    if (isListening) {
        [_scanningProgressView setHidden:NO];
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        [self.navigationItem.rightBarButtonItem.customView setAlpha:0.0];
    } else {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
        [self.navigationItem.rightBarButtonItem.customView setAlpha:1.0];
        [_scanningProgressView setHidden:YES];
    }
}

- (void)dealloc {
    [_tableView release], _tableView = nil;
    [_headerView release], _headerView = nil;
    [_footerView release], _footerView = nil;
    
    [super dealloc];
}

#pragma mark Network stats

- (void)beginNetworkSpeedTest {
    __block float currentProgress = 0.0;
    
    speedTestStartTime = CFAbsoluteTimeGetCurrent();
    if ([[P2PSessionManager sharedInstance] transferInProgress] == NO) {
        
        if ([STP2PClientServerInteractionManager sharedInstance].peer == nil) {
            STP2ServerItem *serverItem = [[[STP2ServerItem alloc] init] autorelease];
            serverItem.iPAddress = ipAddress;
            
            [STP2PClientServerInteractionManager sharedInstance].peer = serverItem;
        }
        [[STP2PClientServerInteractionManager sharedInstance] getDataOfLength:kSampleDataSize success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self calculateBandwidthForBytes:kSampleDataSize];
            
            /*dispatch_async(dispatch_get_main_queue(), ^{
             [[NotificationManager sharedInstance] removeNotification:notification];
             [_nextButton setEnabled:YES];
             });*/
        } progress:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            currentProgress = (float)totalBytesRead / (float)totalBytesExpectedToRead;
            sampleTotalBytesRead = totalBytesRead;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NotificationManager sharedInstance] updateUI];
            });
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"P2P Error calculating transfer bandwidth: %@", error);
            [self calculateBandwidthForBytes:sampleTotalBytesRead];
            
            /*dispatch_async(dispatch_get_main_queue(), ^{
             [[NotificationManager sharedInstance] removeNotification:notification];
             [_nextButton setEnabled:YES];
             });*/
        }];
    }
}

- (void)calculateBandwidthForBytes:(long long)bytesRead {
    CFAbsoluteTime speedTestEndTime = 0.0;
    NSTimeInterval delta = 0.0;
    
    speedTestEndTime = CFAbsoluteTimeGetCurrent();
    delta = speedTestEndTime - speedTestStartTime;
    bandwidthBPS = (NSInteger) ((float)bytesRead / (float)delta);
}


- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
