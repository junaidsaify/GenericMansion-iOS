//
//  GPSViewController.m
//  GenericMansion
//
//  Created by Junaid Saify on 23/04/16.
//
//

#import "GPSViewController.h"

@interface GPSViewController ()

@end

@implementation GPSViewController
@synthesize gpsDataArray,mansionPath;
- (void)viewDidLoad {
    [super viewDidLoad];
    isUserControlled=NO;
    // Do any additional setup after loading the view from its nib.
    [self setupMapAndLocations];
    [self plotTheLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadGPSMap:)
                                                 name:@"LoadGPSMap"
                                               object:nil];
}

-(void)layoutSubviews {
    

    NSLog(@"Layout subviews called");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupMapAndLocations{
    self.locationManager=[[CLLocationManager alloc]init];
    self.locationManager.delegate=self;
    
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    [self.locationManager startUpdatingHeading];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.myLocationEnabled = YES;
    });
    CLLocation *myLocation = [self.mapView myLocation];
    GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:myLocation.coordinate.latitude longitude:myLocation.coordinate.longitude zoom:16.0];
    
    self.mapView.camera=camera;
    self.mapView.settings.myLocationButton = YES;
    [self.mapView animateToBearing:0];
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.settings.consumesGesturesInView=YES;
    self.mapView.delegate=self;
    // Listen to the myLocation property of GMSMapView.
    [self.mapView addObserver:self
                   forKeyPath:@"myLocation"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
    
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!firstLocationUpdate_) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        firstLocationUpdate_ = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        self.mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                             zoom:16];
    }
}




-(void)plotTheLocation{
    
    /*NSArray* cachePathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = [cachePathArray lastObject];
    NSString *Path = [cachePath stringByAppendingPathComponent:@"Travel.json"];
    NSData *data = [NSData dataWithContentsOfFile:Path];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *dicArray = [json objectForKey:@"Travel"];*/
    GMSMutablePath *path = [GMSMutablePath path];
    for(NSDictionary *det in self.gpsDataArray){
        
        CLLocationCoordinate2D myCoordinate;
        myCoordinate.latitude=[[det valueForKey:@"Lat"] floatValue];
        myCoordinate.longitude=[[det valueForKey:@"Lon"] floatValue];        
        
        CLCircularRegion *region= [[CLCircularRegion alloc] initWithCenter:myCoordinate   radius:[[det valueForKey:@"Radius"] floatValue] identifier:[det objectForKey:@"RoomNumber"]];
        
        region.notifyOnEntry=YES;
        
        
        
        
        [self.locationManager startMonitoringForRegion:region];
        //.. [self.locationManager stopMonitoringSignificantLocationChanges];
        [self.locationManager requestStateForRegion:region];
        
        
        
        GMSMarker *annotation=[GMSMarker markerWithPosition:myCoordinate];
        annotation.title = [det valueForKey:@"RoomNumber"];
        annotation.appearAnimation = kGMSMarkerAnimationPop;
        //..annotation.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
        NSString *markerPath= [NSString stringWithFormat:@"%@/%@",mansionPath,[det valueForKey:@"MarkerImgPath"]];
       
       //.. annotation.icon=[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",mansionPath,[det valueForKey:@"MarkerImgPath"]]];
      //  UIImage *markerIcon = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",mansionPath,[det valueForKey:@"MarkerImgPath"]]];
      //  markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
      //  annotation.icon=markerIcon;
       
        NSData *imageData = [NSData dataWithContentsOfFile:markerPath];
        annotation.icon = [UIImage imageWithData:imageData scale:3.0];
        //[GMSMarker markerImageWithColor:[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",mansionPath,[det valueForKey:@"MarkerImgPath"]]]]];
        
                        
       //.. [self.mapView setSelectedMarker:annotation];
        

        annotation.opacity = 1.0;
        annotation.snippet= [[det valueForKey:@"Sequence"] stringValue];
        annotation.map=self.mapView;
        
        
        GMSCircle *circ = [GMSCircle circleWithPosition:myCoordinate
                                                 radius:[[det valueForKey:@"Radius"] floatValue]];
        
        circ.fillColor = [UIColor greenColor];
        circ.strokeColor = [UIColor blackColor];
        circ.strokeWidth = 5;
        circ.map = self.mapView;
        
        
        
        [path addCoordinate:myCoordinate];
        
        
        
       /* GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
        rectangle.strokeWidth = 2.f;
        rectangle.map = self.mapView;*/
        
    }
    
  
}


#pragma CLLocation Manager Delegates

-(void)locationManager:(CLLocationManager *)manager
        didEnterRegion:(CLRegion *)region{
    NSString* message = [NSString stringWithFormat:@"Entered region %@",region.identifier];
    
    NSLog(@"%@",message);
   //.. [[[UIAlertView alloc]initWithTitle:message message:[NSString stringWithFormat:@"%@",region.identifier] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    [self.delegate didEnterRegionWithName:region.identifier];
}
-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    //..  NSLog(@"Bye bye");
}

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    
    NSLog(@"State Change");
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"Region monitoring failed with error: %@", [error localizedDescription]);
    
}


- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region{
    //..  NSLog(@"Monitoring for region %@ ",region.identifier);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    if(!isUserControlled){
        CLLocation *myLocation =[self.mapView myLocation];
        [self.mapView animateToLocation:myLocation.coordinate];
    }
    //..  NSLog(@"%@",locations);
    NSSet * monitoredRegions = self.locationManager.monitoredRegions;
   
    if (self.gpsDataArray) {
        
    [self.gpsDataArray enumerateObjectsWithOptions:nil usingBlock:^(NSDictionary *region, NSUInteger idx, BOOL *stop)
        
        {
             NSString *identifer = [region objectForKey:@"RoomNumber"];
            CLLocationCoordinate2D centerCoords =CLLocationCoordinate2DMake([[region objectForKey:@"Lat"] floatValue], [[region objectForKey:@"Lon"] floatValue]);
             CLLocation *newLocation = (CLLocation*)[locations lastObject];
             CLLocation *oldLocation=nil;
             if(locations.count>1)
                 oldLocation =[locations objectAtIndex:locations.count-1];
             CLLocationCoordinate2D currentCoords= CLLocationCoordinate2DMake(newLocation.coordinate.latitude,newLocation.coordinate.longitude);
             CLLocationDistance radius = [[region objectForKey:@"Radius"] floatValue];
             
              NSNumber * currentLocationDistance =[self calculateDistanceInMetersBetweenCoord:currentCoords coord:centerCoords];
              if([currentLocationDistance doubleValue] < radius)
             
            // if([region containsCoordinate:currentCoords])
             {
                 if(![currentRegion isEqualToString:[region objectForKey:@"RoomNumber"]]){
                     NSLog(@"Mila %@",[region objectForKey:@"RoomNumber"]);
                     currentRegion = [region objectForKey:@"RoomNumber"];
                    // currentMonitoredRegion=region;
                     //..    NSLog(@"Invoking didEnterRegion Manually for region: %@",identifer);
                     [self.delegate didEnterRegionWithName:[[region valueForKey:@"Sequence"] stringValue]];
                     *stop=YES;
                     //stop Monitoring Region temporarily
                   /*  [self.locationManager stopMonitoringForRegion:region];
                     
                     [self locationManager:self.locationManager didEnterRegion:region];
                     //start Monitoing Region
                     [self.locationManager startMonitoringForRegion:region];*/
                     
                     
                 }
                 
                 
                 
                 else{
                     
                 }
                 
             }
              else{
                  //..currentRegion=@"";
              }
             
             
         }];
    }
    
    
}

- (void)locationManager:(CLLocationManager *)manager  didUpdateHeading:(CLHeading *)newHeading
{
    double heading = newHeading.trueHeading;
    double headingDegrees = (heading*M_PI/180);
    CLLocationDirection trueNorth = [newHeading trueHeading];
    
    [self.mapView animateToBearing:trueNorth];
    
    /* other solution
     float heading = newHeading.magneticHeading; //in degrees
     float headingDegrees = (heading*M_PI/180); //assuming needle points to top of iphone. convert to radians
     self.bearingView.transform = CGAffineTransformMakeRotation(headingDegrees);

     */
    
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
   
    if(gesture){
        isUserControlled=YES;
    [self.locationManager stopUpdatingHeading];
    }
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
   //..[self.locationManager startUpdatingHeading];
}
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    //[self.locationManager stopUpdatingHeading];
    //[self.locationManager performSelector:@selector(startUpdatingHeading) withObject:nil afterDelay:3.0];
    
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    //..  NSLog(@"Location error: %@", [error localizedDescription]);
    
}


#pragma Google Map Delegates
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker*)marker{
    
    currentRegion=marker.title;
    
    [self.delegate didEnterRegionWithName:marker.snippet];
    return NO;
}

- (BOOL) didTapMyLocationButtonForMapView:(GMSMapView *)mapView	{
    NSLog(@"My location clicked");
    [self reCentreMapToDefault];

    
    return NO;
}


#pragma Helper Functions


- (NSNumber*)calculateDistanceInMetersBetweenCoord:(CLLocationCoordinate2D)coord1 coord:(CLLocationCoordinate2D)coord2 {
    NSInteger nRadius = 6371; // Earth's radius in Kilometers
    double latDiff = (coord2.latitude - coord1.latitude) * (M_PI/180);
    double lonDiff = (coord2.longitude - coord1.longitude) * (M_PI/180);
    double lat1InRadians = coord1.latitude * (M_PI/180);
    double lat2InRadians = coord2.latitude * (M_PI/180);
    double nA = pow ( sin(latDiff/2), 2 ) + cos(lat1InRadians) * cos(lat2InRadians) * pow ( sin(lonDiff/2), 2 );
    double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
    double nD = nRadius * nC;
    // convert to meters
    return @(nD*1000);
}

-(void)reCentreMapToDefault{
    isUserControlled=NO;
   CLLocation *myLocation =[self.mapView myLocation];
    [self.mapView animateToLocation:myLocation.coordinate];
    [self.mapView animateToZoom:16.0f];
    [self.locationManager startUpdatingHeading];
    
}


#pragma NSNotification Method

-(void) loadGPSMap:(NSNotification*)notification
{
  NSDictionary* userInfo = notification.userInfo;
    NSString *identifier = [userInfo objectForKey:@"PresentRoom"];
    if([identifier isEqualToString:currentRegion]){
        [self.delegate willShowGPSMap];
    }
}

-(IBAction)removeView:(id)sender{
    [self.delegate didEnterRegionWithName:nil];
}

-(void)dealloc{
    [self.mapView removeObserver:self forKeyPath:@"myLocation"];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
