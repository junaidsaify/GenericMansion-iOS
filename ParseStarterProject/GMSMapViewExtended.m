//
//  GMSMapViewExtended.m
//  GenericMansion
//
//  Created by Junaid Saify on 29/04/16.
//
//

#import "GMSMapViewExtended.h"

@implementation GMSMapViewExtended

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    NSLog(@"DEBUG: Touches began" );
    [[[UIAlertView alloc]initWithTitle:nil message:@"Touch Began" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    UITouch *touch = [[event allTouches] anyObject];
    
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSLog(@"DEBUG: Touches cancelled");
    
    // Will be called if something happens - like the phone rings
    
    UITouch *touch = [[event allTouches] anyObject];
    
    [super touchesCancelled:touches withEvent:event];
    
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSLog(@"DEBUG: Touches moved" );
    
    UITouch *touch = [[event allTouches] anyObject];
    
    [super touchesMoved:touches withEvent:event];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"DEBUG: Touches ending" );
    [[[UIAlertView alloc]initWithTitle:nil message:@"Touch Ended" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    //Get all the touches.
    NSSet *allTouches = [event allTouches];
    
    //Number of touches on the screen
    switch ([allTouches count])
    {
        case 1:
        {
            //Get the first touch.
            UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
            
            switch([touch tapCount])
            {
                case 1://Single tap
                    
                    break;
                case 2://Double tap.
                    
                    break;
            }
        }
            break;
    }
    [super touchesEnded:touches withEvent:event];
}

@end
