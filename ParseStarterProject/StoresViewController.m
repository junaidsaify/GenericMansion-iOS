//
//  StoresViewController.m
//  Mansion
//
//  Created by Snehal Shah on 5/24/15.
//
//

#import "StoresViewController.h"

@interface StoresViewController ()

@end

@implementation StoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self resetDatasourceForStores];
    if ([self.storesTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.storesTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.storesTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    [self formatTheTextLable];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName=@"StoresScreen";
}

-(void)resetDatasourceForStores{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"StoresProperty" ofType: @"plist"];
    storesDictionary = [NSDictionary dictionaryWithContentsOfFile: path];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:nil ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sort];
    
    
    
    NSArray *arr= [[storesDictionary allKeys] sortedArrayUsingDescriptors:sortDescriptors];
    storesKeys=[NSMutableArray arrayWithArray:arr];
    [storesKeys removeAllObjects];
    
    [storesKeys insertObject:@"Bannister's Wharf" atIndex:0];
    [storesKeys insertObject:@"The Breakers" atIndex:1];
    [storesKeys insertObject:@"The Elms" atIndex:2];
    [storesKeys insertObject:@"Marble House" atIndex:3];
    [storesKeys insertObject:@"Rosecliff" atIndex:4];
    [storesKeys insertObject:@"NewportStyle" atIndex:5];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma tableview datasource and delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(isIphone){
        return  160;
    }
    return 230;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[storesDictionary allKeys] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StoresCell";
    
    StoresTableViewCell *cell = (StoresTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        //cell = [[SubStopsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        NSArray *nib;
        
        if(isIphone){
            nib = [[NSBundle mainBundle] loadNibNamed:@"StoresTableViewCell-iPhone" owner:self options:nil];
        }
        else{
            nib   = [[NSBundle mainBundle] loadNibNamed:@"StoresTableViewCell-iPad" owner:self options:nil];
        }
        
        
        cell = [nib objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.delegate=self;
        
        if(isIphone){
            cell.greenImage.frame=CGRectMake(10, 23, 109, 111);
            cell.mansionLable.frame = CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+10, 20, CGRectGetWidth(cell.mansionLable.frame), CGRectGetHeight(cell.mansionLable.frame));
            cell.addressButton.frame=CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+10, CGRectGetMaxY(cell.mansionLable.frame)+10, CGRectGetWidth(cell.contentView.frame)-CGRectGetWidth(cell.greenImage.frame)-50, CGRectGetHeight(cell.addressButton.frame));
            cell.phoneNumberButton.frame=CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+10, CGRectGetMaxY(cell.addressButton.frame)+10, CGRectGetWidth(cell.addressButton.frame)/2+20, CGRectGetHeight(cell.phoneNumberButton.frame));
            cell.extNumber.frame=CGRectMake(CGRectGetMaxX(cell.phoneNumberButton.frame)+5, CGRectGetMaxY(cell.addressButton.frame)+10, CGRectGetWidth(cell.extNumber.frame), CGRectGetHeight(cell.extNumber.frame));
        }
        
        else{
            cell.greenImage.frame=CGRectMake(10, 13, 350, 204);
        }
        
        
        
        //..cell.bagImage.frame=CGRectMake(CGRectGetMaxX(cell.greenImage.frame)+50, 30, 86, 83);
        
    }
    
    cell.addressButton.tag=indexPath.row;
    cell.greenImage.layer.borderColor=[UIColor colorWithString:@"#00563F"].CGColor;
    cell.greenImage.layer.borderWidth=2.0f;
    
    cell.extNumber.hidden=YES;
    if(indexPath.row==[storesKeys count]-1)
        cell.extNumber.hidden=NO;
    
    cell.greenImage.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:2]]];
    cell.mansionLable.text=[storesKeys objectAtIndex:indexPath.row];
    NSString *address = [[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:0];
    
    
    
    /*  [cell.addressButton setTitle:address forState:UIControlStateNormal];
     [cell.phoneNumberButton setTitle:[[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:1] forState:UIControlStateNormal];
     NSLog(@"Phone Number : %@", [[storesDictionary objectForKey:[storesKeys objectAtIndex:indexPath.row]] objectAtIndex:1]); */
    
    [cell.addressButton setAttributedTitle:[attributedAddressArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    [cell.phoneNumberButton setAttributedTitle:[attributedPhoneNumberArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    
    
    return cell;
}


-(void)formatTheTextLable {
    
    attributedAddressArray=[[NSMutableArray alloc]initWithCapacity:1];
    attributedPhoneNumberArray=[[NSMutableArray alloc]initWithCapacity:1];
    for(int i=0 ; i<storesKeys.count; i++){
        NSMutableAttributedString* addString = [[NSMutableAttributedString alloc] initWithString:[[storesDictionary objectForKey:[storesKeys objectAtIndex:i]] objectAtIndex:0]];
        NSMutableAttributedString* phoneString=[[NSMutableAttributedString alloc] initWithString:[[storesDictionary objectForKey:[storesKeys objectAtIndex:i]] objectAtIndex:1]];
        
        [addString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:(NSRange){0,1}];
        [addString addAttribute:NSUnderlineColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 1)];
        [addString addAttribute:NSUnderlineStyleAttributeName
                          value:@(NSUnderlineStyleSingle)
                          range:(NSRange){0,[addString length]}];
        
        if(isIphone){
        [phoneString addAttribute:NSUnderlineStyleAttributeName
                            value:@(NSUnderlineStyleSingle)
                            range:(NSRange){0,1}];
        [phoneString addAttribute:NSUnderlineColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 1)];
     
        [phoneString addAttribute:NSUnderlineStyleAttributeName
                            value:@(NSUnderlineStyleSingle)
                            range:(NSRange){0,[phoneString length]}];
        }
        
        [attributedAddressArray addObject:addString];
        [attributedPhoneNumberArray addObject:phoneString];
        
        
        
        
    }
    
    
}

#pragma Custom Cell Delegate

-(void)didClickOnAddressButtonForCell:(StoresTableViewCell*)cell withButton:(UIButton*)button{
    NSArray * allKeys=storesKeys;
    int  index=button.tag;
    
    NSString *reformAddress = [NSString stringWithFormat:@"%@,%@",[allKeys objectAtIndex:index],[button currentAttributedTitle].string];
    if([[allKeys objectAtIndex:index] isEqualToString:@"Bannister's Wharf"]){
        reformAddress =@"Bannister's Wharf Newport , Rhode Island 02840"; //..[button currentAttributedTitle].string;
    }

    
    NSString *formatted = [reformAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *addressString = [NSString stringWithFormat:@"%@%@",
                               @"http://maps.apple.com/?q=", formatted];
    if([formatted isEqualToString:[cell.addressButton currentTitle]])
        addressString=[cell.addressButton currentTitle];
    
    
    if(button.tag==[storesKeys count]-1){
        addressString = [button currentAttributedTitle].string;
    }
    
    if(isIphone){
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController" bundle:nil];
    }
    else{
        webViewCtrl=[[WebViewController alloc]initWithNibName:@"WebViewController-iPad" bundle:nil];
    }
    
    webViewCtrl.urlString=addressString;
    [self.view.window.rootViewController presentViewController:webViewCtrl animated:YES completion:nil];
    
    
}



-(void)didClickOnPhoneNumberButtonForCell:(StoresTableViewCell*)cell withButton:(UIButton*)button{
    NSString *phoneURLString = [NSString stringWithFormat:@"tel://%@", [button currentAttributedTitle].string];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        
    {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Newport Mansions" message:@"\nCall Not Supported" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
