//
//  NewDirectionViewController.h
//  Mansion
//
//  Created by Snehal Shah on 5/27/15.
//
//

#import <UIKit/UIKit.h>

@interface NewDirectionViewController : GAITrackedViewController

@property(nonatomic,retain)NSString *urlString;

@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *spinner;
-(id)initWithURLString:(NSString*)urlStr;
@end
