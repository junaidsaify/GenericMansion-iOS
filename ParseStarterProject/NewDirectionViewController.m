//
//  NewDirectionViewController.m
//  Mansion
//
//  Created by Snehal Shah on 5/27/15.
//
//

#import "NewDirectionViewController.h"

@interface NewDirectionViewController ()

@end

@implementation NewDirectionViewController
@synthesize urlString;

-(void) viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.webView loadHTMLString:@"" baseURL:nil];
  
    
    NSURL *url = [NSURL URLWithString: [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.spinner startAnimating];
    [self.view bringSubviewToFront:self.spinner];
    //Load the request in the UIWebView.
    [self.webView loadRequest:requestObj];
    //...[self.webView performSelector:@selector(loadRequest:) withObject:requestObj afterDelay:2.0];
       if([self.urlString isEqualToString:DonateLink]){
        //   self.safariButton.hidden=NO;
        
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.screenName=@"DirectionScreen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openInSafari:(UIButton*)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
}
#pragma WebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
