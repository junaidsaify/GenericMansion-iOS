//
//  AudioPlayer.m
//  Mansion
//
//  Created by Falguni Shah on 11/21/14.
//
//

#import "AudioPlayer.h"

@implementation AudioPlayer
//@synthesize player;
-(id)init{
    self=[super init];
    if(self){
        
    }
    
    return self;
}

-(void)createAudioPlayerWithPath:(NSString*)path{
    if(_player==nil){
        
      //..  path=[[NSBundle mainBundle] pathForResource:@"eng_417" ofType:@"mp3"];
        NSURL *fileURL =[[NSURL alloc] initFileURLWithPath: path];
        NSError *error = nil;
_player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
        
        if(!error){
        _player.delegate=self;
        _player.numberOfLoops=0;
        [_player setVolume:1.0];
        [_player prepareToPlay];
        [_player play];
        }
        
        else{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Oops" message:@"No Audio File Present" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else{
        [self togglePlayer:_player];
    }
    
}

-(void)togglePlayer:(AVAudioPlayer*)player{
    
    if([_player isPlaying]){
        [_player pause];
    }
    else{
        [_player play];
    }
}

-(void)stopAndDistroyPlayer{
    [_player stop];
    _player=nil;
}

-(void)pauseAudioPlayer{
    [_player pause];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    NSLog(@"Finkgj");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AudioPlayerFinishedPlaying" object:nil];
}

/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    NSLog(@"bfghjk");
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
     NSLog(@"hghkk");
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
     NSLog(@"jjj");
}

@end
