//
//  DBManager.m
//  Mansion
//
//  Created by Falguni Shah on 1/18/15.
//
//

#import "DBManager.h"

@implementation DBManager
@synthesize dbPath;
-(id)initWithSQLPath:(NSString*)sqlDbFilePath{
    self=[super init];
    if (self) {
        //intialize the path
        self.dbPath=sqlDbFilePath;
        
    }
    
    NSLog(@"the dbpath is %@",self.dbPath);
    return self;
}


-(sqlite3*)openDataBase{
    //sqlite3 *dataBase;
    if(dbPtr){
        return dbPtr;
    }
    
    
   else if(sqlite3_open([self.dbPath UTF8String], &dbPtr)==SQLITE_OK){
        return dbPtr;
    }
    return nil;
}

-(void)closeDataBase{
    sqlite3_close(dbPtr);
}

-(NSString*) getTranscriptForRoom:(NSString*)room andTable:(NSString*)tableName {
    NSString *transcipt = @"";
    NSString *query;
    if([tableName isEqualToString:@"Main"]){
        query= [NSString stringWithFormat:@"SELECT transcript FROM %@ WHERE roomnumber=\"%@\"",tableName,room];
    }
    else{
        query= [NSString stringWithFormat:@"SELECT transcript FROM %@ WHERE stopnumber=\"%@\"",tableName,room];
    }
    
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            transcipt=(NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
        }
        
    }
    sqlite3_finalize(compiledStatement);
    return transcipt;
    
}

-(NSMutableArray*)getAllRooms{
    
    NSMutableArray *roomArray=[NSMutableArray arrayWithCapacity:1];
    NSString *query=[NSString stringWithFormat:@"SELECT roomnumber FROM Main order by sequence ASC"];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            NSString *roomnumber=[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
            [roomArray addObject:roomnumber];
        }
    }
    
    return roomArray;
    
}


-(NSMutableArray*)getRoomsForFloor:(NSString*)floorName{
    NSMutableArray *roomArray=[NSMutableArray arrayWithCapacity:1];
    NSString *query=[NSString stringWithFormat:@"SELECT roomnumber FROM Main WHERE floorname=\"%@\" order by sequence ASC",floorName];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            NSString *roomnumber=[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
            [roomArray addObject:roomnumber];
        }
    }
    
    return roomArray;
 
}




-(NSArray*)getMansionDetailForMansion:(NSString*)mansionName{
/*    NSMutableArray *categoryArray=[NSMutableArray arrayWithCapacity:1];
    NSString *query=[NSString stringWithFormat:@"SELECT * FROM Category WHERE UserID=\"%@\"",userID];
    
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(database, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        // Loop through the results and add them to the feeds array
        
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
            NSLog(@"the category id is %d ",sqlite3_column_int(compiledStatement, 0));
            NSInteger categoryID=sqlite3_column_int(compiledStatement, 0);
            NSString *categoryName=[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 1)];
            NSInteger serviceID=sqlite3_column_int(compiledStatement, 4);
            if(serviceID==0){//For Default categories
                serviceID=categoryID;
            }
            NSDictionary *dict=@{@"id": @(serviceID),@"name":categoryName};
            [categoryArray addObject:dict];
            
        }
    }
    sqlite3_finalize(compiledStatement);
    
    
    return categoryArray; */
    return nil;
}

-(NSString*)getNameForRoom:(NSString*)roomNumber{
    NSString *roomName=@"Pata Nai Room";
     NSString *query=[NSString stringWithFormat:@"SELECT stopname FROM Main WHERE roomnumber=\"%@\"",roomNumber];
    sqlite3_stmt *compiledStatement;
      if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
          while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
         roomName=(NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
          }

      }
    sqlite3_finalize(compiledStatement);
    return roomName;
}

-(NSUInteger)getFloorNumberForName:(NSString*)floorName{
    NSString *query=[NSString stringWithFormat:@"SELECT floornumber FROM Main WHERE floorname=\"%@\"",floorName];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
         while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
        NSString *floorNumStr=(NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
        return [floorNumStr intValue];
         }
    }
    
    return nil;
}

-(NSString*)getFloorNameForRoom:(NSString*)roomNumber{
    NSString *floorName=nil;
    NSString *query=[NSString stringWithFormat:@"SELECT floorname FROM Main WHERE roomnumber=\"%@\"",roomNumber];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            floorName=(NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
        }
        
    }
    sqlite3_finalize(compiledStatement);
    return floorName;
}


-(NSString*)getSubStopNameForRoom:(NSString*)stopNumber{
    NSString *stopName=@"";
    NSString *query=[NSString stringWithFormat:@"SELECT stopname FROM Substops WHERE stopnumber=\"%@\"",stopNumber];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            stopName=(NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
            stopName=[NSString stringWithFormat:@"%@- %@",stopNumber,stopName];
        }
        
    }
    sqlite3_finalize(compiledStatement);
    return stopName;
}


-(NSString*)getOrderOfSubStopForRoom:(NSString*)roomNumber{
    NSString *subStopName=nil;
    NSString *query=[NSString stringWithFormat:@"SELECT substops FROM Main WHERE roomnumber=\"%@\"",roomNumber];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            subStopName=(NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
        }
        
    }
    sqlite3_finalize(compiledStatement);
    return subStopName;
}

-(NSMutableArray*)getDataForGPS{
    
    NSString *query= @"SELECT roomnumber,floornumber,lat,lon,markerImgPath,radius,sequence FROM Main order by sequence ASC";
    NSMutableArray *GPSArray = [[NSMutableArray alloc]initWithCapacity:1];
    sqlite3_stmt *compiledStatement;
    if(sqlite3_prepare_v2(dbPtr, [query UTF8String], -1, &compiledStatement, nil) == SQLITE_OK) {
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            //..NSDictionary *gpsDict=[[NSDictionary alloc]init];
            NSString *roomnumber = (NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 0)];
            
            NSString *floornumber = (NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 1)];
            NSString *lat = (NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 2)];
            NSString *lon = (NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 3)];
            NSString *markerImgPath = (NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 4)];
            NSString *radius = (NSString*)[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, 5)];
            NSNumber *sequence = [NSNumber numberWithInt:(int)sqlite3_column_int(compiledStatement, 6)];
          
            
            NSDictionary *gpsDict=@{@"RoomNumber":roomnumber , @"FloorName":floornumber ,@"Lat":lat ,@"Lon":lon ,@"MarkerImgPath":markerImgPath ,@"Radius":radius,@"Sequence":sequence};
            [GPSArray addObject:gpsDict];
            
        }
        
    }
    sqlite3_finalize(compiledStatement);
    
    return GPSArray;
}




@end
