//
//  GettingHereViewController.h
//  Mansion
//
//  Created by Falguni Shah on 12/13/14.
//
//

#import <UIKit/UIKit.h>
#import "Mansion.h"
#import "DirectionsViewController.h"
#import "StoresTableViewCell.h"
#import "WebViewController.h"
#import "NewDirectionViewController.h"
@interface GettingHereViewController : GAITrackedViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    DirectionsViewController *directionVC;
    NSDictionary *storesDictionary;
    NSMutableArray *storesKeys;
    WebViewController *webViewCtrl;
    NewDirectionViewController *newDirVC;
          NSMutableArray *attributedPhoneNumberArray,*attributedAddressArray;
    IBOutlet UIScrollView *mapScrollView;
}
@property(nonatomic,retain)IBOutlet UIButton *mapButton,*directionButton,*parkingButton,*storesButton;
@property(nonatomic,strong)IBOutlet UIView *mapView,*topView,*bottomView,*storesView;
@property(nonatomic,retain)IBOutlet UILabel *mansionNameLable;
@property(nonatomic,retain)IBOutlet UIImageView *mapImageView,*legendImageView;
@property(nonatomic,retain)Mansion *mansion;
@property(nonatomic,retain)NSString *currentMansion,*currentMansionAddress;
;
@property(nonatomic,retain)IBOutlet UITableView *tableView;
@property(nonatomic,retain)UIButton *safariButton;
-(IBAction)removeView:(UIButton*)sender;
-(IBAction)loadDirectionScreen:(UIButton*)sender;
@end
