//
//  ParseStarterProjectAppDelegate.h
//  ParseStarterProject
//
//  Copyright 2014 Parse, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
//..#import <FacebookSDK/FacebookSDK.h>
//#import "RegistrationViewController.h"
//@class ParseStarterProjectViewController;
//..
@class MasterViewController;
#import "MasterViewController.h"
@interface ParseStarterProjectAppDelegate : UIResponder <UIApplicationDelegate>
{
      UIAlertView *alert;
}
@property (nonatomic, strong) IBOutlet UIWindow *window;
@property(nonatomic,retain) CLLocationManager *locationManager;
//..@property (nonatomic, strong) IBOutlet ParseStarterProjectViewController *viewController;
@property (nonatomic, strong) IBOutlet MasterViewController *viewController;
@property(nonatomic, strong) id<GAITracker> tracker;
//@property (nonatomic, strong) IBOutlet RegistrationViewController *viewController;
@end
