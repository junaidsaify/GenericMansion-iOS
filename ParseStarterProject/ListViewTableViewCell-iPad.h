//
//  ListViewTableViewCell-iPad.h
//  Mansion
//
//  Created by Snehal Shah on 4/12/15.
//
//

#import <UIKit/UIKit.h>

@interface ListViewTableViewCell_iPad : UITableViewCell

@property(nonatomic,retain)IBOutlet UILabel *roomTextLable,*floorTextLable;
@property(nonatomic,retain)IBOutlet UIImageView *roomImage;
@end
