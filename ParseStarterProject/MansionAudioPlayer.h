//
//  MansionAudioPlayer.h
//  Mansion
//
//  Created by Falguni Shah on 11/22/14.
//
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
@interface MansionAudioPlayer : UIViewController <AVAudioPlayerDelegate,AVAudioSessionDelegate>

@property(nonatomic,retain)AVAudioPlayer *player;
@property(nonatomic,retain)NSString *audioPath;
-(void)createAudioPlayerWithPath:(NSString*)path;
-(void)togglePlayer:(AVAudioPlayer*)player;
-(void)stopAndDistroyPlayer;

@end
