//
//  DBManager.h
//  Mansion
//
//  Created by Falguni Shah on 1/18/15.
//
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface DBManager : NSObject{
    
    sqlite3 *dbPtr;
}

@property(nonatomic,retain)NSString *dbPath;

-(id)initWithSQLPath:(NSString*)sqlDbFilePath;
-(sqlite3*)openDataBase;
-(void)closeDataBase;

-(NSString*)getNameForRoom:(NSString*)roomNumber;
-(NSUInteger)getFloorNumberForName:(NSString*)floorName;
-(NSString*)getFloorNameForRoom:(NSString*)roomNumber;
-(NSString*)getSubStopNameForRoom:(NSString*)stopNumber;
-(NSString*)getOrderOfSubStopForRoom:(NSString*)roomNumber;

-(NSMutableArray*)getRoomsForFloor:(NSString*)floorName;
-(NSMutableArray*)getAllRooms;
-(NSString*) getTranscriptForRoom:(NSString*)room andTable:(NSString*)tableName;
-(NSMutableArray*)getDataForGPS;
@end
